<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'App\Http\Controllers\HomeController@index');

Route::get('home', 'App\Http\Controllers\HomeController@index');
Route::get('career', 'App\Http\Controllers\HomeController@career');
Route::get('whatwedo', 'App\Http\Controllers\HomeController@whatwedo');
Route::get('doctor', 'App\Http\Controllers\HomeController@doctor');
Route::get('study1', 'App\Http\Controllers\HomeController@study1');
Route::get('study2', 'App\Http\Controllers\HomeController@study2');
Route::get('study3', 'App\Http\Controllers\HomeController@study3');
Route::get('study4', 'App\Http\Controllers\HomeController@study4');
Route::get('partnerships', 'App\Http\Controllers\HomeController@partnerships');
Route::get('aboutus', 'App\Http\Controllers\HomeController@aboutus');
Route::get('technology', 'App\Http\Controllers\HomeController@technology');
Route::get('book_appointment', 'App\Http\Controllers\HomeController@book_appointment');
Route::get('fevertest', 'App\Http\Controllers\HomeController@fevertest');
Route::get('patents', 'App\Http\Controllers\HomeController@patents');
Route::get('contact', 'App\Http\Controllers\HomeController@contact');
Route::get('terms_use_privacy_policy', 'App\Http\Controllers\HomeController@terms_use_privacy_policy');
Route::get('gdpr_privacy_notes', 'App\Http\Controllers\HomeController@gdpr_privacy_notes');
Route::get('refund_and_cancellation', 'App\Http\Controllers\HomeController@refund_and_cancellation');

Route::get('women', 'App\Http\Controllers\HomeController@women');
Route::get('doctor', 'App\Http\Controllers\HomeController@doctor');


// Admin Start
Route::get('a/', 'App\Http\Controllers\Admin\AdminController@index');
Route::get('admin/', 'App\Http\Controllers\Admin\AdminController@index');
Route::get('admin/login', 'App\Http\Controllers\Admin\AdminController@login');
Route::post('admin/check_login', 'App\Http\Controllers\Admin\AdminController@check_login')->name('admin.check_login');
Route::get('/logout', '\App\Http\Controllers\Auth\LoginController@logout');

Auth::routes();

//Dashboard
Route::get('admin/dashboard', 'App\Http\Controllers\Admin\AdminController@dashboard');




//Banner
Route::resource('admin/banner', 'App\Http\Controllers\Admin\BannerController');
Route::get('admin/banner/status/{id}/{status}', 'App\Http\Controllers\Admin\BannerController@status');


//Advisory
Route::resource('admin/advisory', 'App\Http\Controllers\Admin\AdvisoryController');
Route::get('admin/advisory/status/{id}/{status}', 'App\Http\Controllers\Admin\AdvisoryController@status');

//Awards
Route::resource('admin/awards', 'App\Http\Controllers\Admin\AwardsController');
Route::get('admin/awards/status/{id}/{status}', 'App\Http\Controllers\Admin\AwardsController@status');
 
//Product
Route::resource('admin/product', 'App\Http\Controllers\Admin\ProductController');
Route::get('admin/product/status/{id}/{status}', 'App\Http\Controllers\Admin\ProductController@status');


//Partners
Route::resource('admin/partners', 'App\Http\Controllers\Admin\PartnersController');
Route::get('admin/partners/status/{id}/{status}', 'App\Http\Controllers\Admin\PartnersController@status');
//Careers
Route::resource('admin/career', 'App\Http\Controllers\Admin\CareerController');
Route::get('admin/career/status/{id}/{status}', 'App\Http\Controllers\Admin\CareerController@status');

//Technology
Route::resource('admin/technology', 'App\Http\Controllers\Admin\TechnologyController');
Route::get('admin/technology/status/{id}/{status}', 'App\Http\Controllers\Admin\TechnologyController@status');

//Partnerships
Route::resource('admin/partnerships', 'App\Http\Controllers\Admin\PartnershipsController');
Route::get('admin/partnerships/status/{id}/{status}', 'App\Http\Controllers\Admin\PartnershipsController@status');

//Book_appointment
Route::resource('admin/book_appointment', 'App\Http\Controllers\Admin\Book_AppointmentController');
Route::get('admin/book_appointment/status/{id}/{status}', 'App\Http\Controllers\Admin\Book_AppointmentController@status');
//Profile
Route::match(array('GET','POST'),'admin/profile', 'App\Http\Controllers\Admin\ProfileController@index');
Route::match(array('GET','POST'),'admin/change_password', 'App\Http\Controllers\Admin\AdminController@change_password');

//Setting
Route::resource('admin/setting', 'App\Http\Controllers\Admin\SettingController');





Route::get('/cache', function() {
    Artisan::call('route:cache');
    Artisan::call('route:clear');
    Artisan::call('optimize');
    Artisan::call('config:cache');
    
    
    Artisan::call('view:clear');
    Artisan::call('cache:clear');
   
    return '<h1>Clear Config cleared</h1>';
});