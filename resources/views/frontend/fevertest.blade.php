@extends('layouts.app')

@section('content')
<style>
p{
    text-align: justify;
}
#customers {
        font-family: Arial, Helvetica, sans-serif;
        border-collapse: collapse;
        width: 100%;
    }

    #customers td,
    #customers th {
        border: 1px solid #ddd;
        text-align: center;
        padding: 8px;
    }

    #customers tr:nth-child(even) {
        background-color: #f2f2f2;
    }

    #customers tr:hover {
        background-color: #ddd;
    }

    #customers th {
        padding-top: 12px;
        padding-bottom: 12px;
        text-align: center;
        background-color: #d7358c;
        color: white;
    }
    h4{
        color: #d7358c;
    }
</style>

<section>
    <div class="women">
        <div class="container">
            <div class="vertical-space-70"></div>
            <h4 class="main-title mb-1">Fever Test</h4>
              </div>
              <div class="vertical-space-20"></div>
         <div class="container">
            <h4 class="mb-1 text-center text-dark">Automated COVID Screening using NIRAMAI FeverTest</h4>
             </div>

             <div class="vertical-space-40"></div>
         <div class="container">
            <h4 class="mb-1">Background</h4>
            <div class="row gx-0">
                <div class="col-lg-12">
                    <p class="main-text">
                        <i class="fa fa-circle" style="font-size:8px;color: #d7358c;"></i>
                        The recent outbreak of COVID-19 has shocked everyone. Community screening is the most important aspect to reduce this disease spread. Identifying likely infected people correctly helps to control the spread. The most common symptom seen in likely COVID positive patients is  elevated body temperature (fever). According to the Center for Disease Control (CDC), all visitors to  hospitals, public places or other institutions, should be passively screened for symptoms of fever and acute respiratory illness before entering such facilities.    </p>
                        <p class="main-text mb-5">
                        <i class="fa fa-circle" style="font-size:8px;color: #d7358c;"></i>
                        Currently, hand-held infra-red thermometers are used to measure the skin temperature of individuals  in airports, hospitals and other public places. This method of measuring the skin temperature is prone to manual errors, time consuming and risks infections due to close contact. Just measuring increased temperature of the individual is also not a sufficient signal of COVID19 infection.
                        </p>       </div>
            </div>
        </div>
        <div class="container">
            <div class="vertical-space-30"></div>
            <h4 class="main-title mb-1">Advantages of Niramai FeverTest
</h4>
              </div>
              <div class="vertical-space-20"></div>
     
        <div class="container">
            <h4 class="mb-1">How is this different from infrared thermometers?</h4>
            <p class="main-text">Thermal Screening using  infrared thermometers is not suitable for mass screening and has the following disadvantages over NIRAMAI solution:</p>

            <div class="row gx-0">
                <div class="col-lg-12">
                    <p class="main-text" style="text-align: justify;">
                        <i class="fa fa-circle" style="font-size:8px;color: #d7358c;"></i>
              Close contact measurement provokes fear across individuals  being screened and the required close proximity also increases the risk to the person conducting the screening if  social distancing is not maintained.           </p>
                    <p class="main-text" style="text-align: justify;">
                        <i class="fa fa-circle" style="font-size:8px;color: #d7358c;"></i>
                        The manual process is time consuming, labor intensive and could result in long queues when used in high traffic areas such as at airports, railway stations and other public areas </div>
                        <p class="main-text mb-5" style="text-align: justify;">
                        <i class="fa fa-circle" style="font-size:8px;color: #d7358c;"></i>
                        Ensuring that each individual is screened in  high footfall  areas may be difficult.
                    </div>
        </div>

        <div class="container">
            <h4 class="mb-1">How is this different from thermal cameras installed in some airports?</h4>
            <p class="main-text">FeverTest  makes the screening process non-contact and passive. The thermal camera  provides a live thermal video stream  which is analysed. FeverTest sounds an immediate alarm when the  threshold temperature is exceeded. 
<p class="main-text">The NIRAMAI FeverTest solution also includes an AI enabled software for automated analysis of the thermal stream. The advantages of NIRAMAI Automated FeverTest are:</p>
            <div class="row gx-0">
                <div class="col-lg-12">
                    <p class="main-text" style="text-align: justify;">
                        <i class="fa fa-circle" style="font-size:8px;color: #d7358c;"></i>
                        The solution based on thermal imaging measures skin temperature automatically  using high-quality  thermal  cameras. It can measure the temperature of  individuals from a distance of 1.5 feet to 10 feet .
                           </p>
                           <p class="main-text" style="text-align: justify;">
                        <i class="fa fa-circle" style="font-size:8px;color: #d7358c;"></i>
                        Thermal cameras can measure the temperature of multiple people at once. This is especially beneficial in busy public and work areas.
                           </p>
                           <p class="main-text" style="text-align: justify;">
                        <i class="fa fa-circle" style="font-size:8px;color: #d7358c;"></i>
                        Contactless measurement supports social distancing norms.     </p>
                        <p class="main-text" style="text-align: justify;">
                        <i class="fa fa-circle" style="font-size:8px;color: #d7358c;"></i>
                        Reduced psychological impact as it is a passive process and does not provoke fear and negative emotions.
                      </p>
                        <p class="main-text" style="text-align: justify;">
                        <i class="fa fa-circle" style="font-size:8px;color: #d7358c;"></i>
                        Using NIRAMAI AI solution, enables centralized data collection of the screened individuals, face recognition and automated alarms when individuals with elevated temperature are detected.
                        </p>
                        <p class="main-text" style="text-align: justify;">
                        <i class="fa fa-circle" style="font-size:8px;color: #d7358c;"></i>
                        In addition to a fixed temperature threshold, the solution detects relative temperature abnormality and learns the threshold automatically, enabling the screening to work irrespective of the environmental temperature.
                         </p>

                         <p class="main-text" style="text-align: justify;">
                        <i class="fa fa-circle" style="font-size:8px;color: #d7358c;"></i>
                        In addition to elevated body temperature, thermal distribution on different parts of the face are analysed to determine potential respiratory abnormalities as well.   </p>

                         <p class="main-text" style="text-align: justify;">
                        <i class="fa fa-circle" style="font-size:8px;color: #d7358c;"></i>
                        Daily statistics of the number of people screened and detected with elevated temperature can be made available in a seamless manner.
                          </p>
                          <div class="vertical-space-30"></div>
       
<p class="main-text text-center">
<iframe width="560" height="315" src="https://www.youtube.com/embed/CIDAl6MWlx0" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
</p>
<div class="vertical-space-30"></div>
       
<p class="main-text" style="text-align: justify;">
                        <i class="fa fa-circle" style="font-size:8px;color: #d7358c;"></i>
                        Currently the solution is available only for Indian market.
                       </p>
                      
                   
                       <a href="{{base_url('contact')}}" class="btn common-btn">Contact Form
</a>
   
                
                    </div>
            </div>
        </div>
    </div>
</section>
@endsection