@extends('layouts.app')
@php
    $setting = \App\Models\Setting::find(1);
 @endphp

 <script src="https://cdn.jsdelivr.net/npm/sweetalert2@10/dist/sweetalert2.all.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>

@section('content')
<style>
.questionwrapper {
  width: 100%;
}

.questionh1 {
  margin-bottom: 20px;
}

.questioncontainer {
  background-color: white;
  color: black;
  border-radius: 10px;
  box-shadow: 0 5px 10px 0 rgb(0,0,0,0.25);
  margin: 20px 0;
}

.question {
  font-size: 1rem;
  padding: 10px 10px 10px 10px;
  position: relative;
  display: flex;
  align-items: center;
  cursor: pointer;
  width: 90%;
  text-align: justify;
}

.question::after {
  content: "\002B";
  font-size: 2.2rem;
  position: absolute;
  right: -25px;
  transition: 0.2s;
}

.question.active::after {
  transform: rotate(45deg);
}

.answercont {
  max-height: 0;
  overflow: hidden;
  transition: 0.3s;
}

.answer {
  padding: 0 20px 20px;
  line-height: 1.5rem;
}


@media screen and (max-width: 790px){
  html {
    font-size: 14px;
  }
  .questionwrapper {
  width: 100%;
}
.question::after {
  content: "\002B";
  font-size: 2.2rem;
  position: absolute;
  right: -20px;
  transition: 0.2s;
}
.question {
  font-size: 1rem;
  padding: 10px 10px 10px 10px;
  position: relative;
  display: flex;
  align-items: center;
  cursor: pointer;
  width: 85%;
  text-align: justify;
}
}   
    
  input[type=text],input[type=email]{
    margin-top:7%;
    padding: 2%;
    margin-bottom: 7%;
  }
  .social-links-footer ul li a {
    color: #d7355c !important;
    font-size: 15px !important;
    position: relative !important;
    display: inline-block !important;
    /* padding:14px; */
    width: 40px !important;
    height: 40px !important;
    border: 1px solid #d7355c !important;
    background:transparent !important;
    text-align: center !important;
    line-height: 40px !important;
    border-radius: 50% !important;
    -webkit-transition: all .3s ease-in-out;
    -o-transition: all .3s ease-in-out;
    -ms-transition: all .3s ease-in-out;
    -webkit-transition: all .3s ease-in-out
}
.social-links-footer ul li a i {
position: relative;
top: 12px;
}
.social-links-footer ul li:hover a {
    background:#d7355c !important;
    color: #fff !important;
}
    h4{
        color: #d7358c;
    }
</style>

<section>
    <div class="women">
        <div class="container">
            <div class="vertical-space-70"></div>
            <h4 class="mb-1 main-text" style="color:#000;font-weight:600;font-size:24px">We would love to partner with you</h4>
            <div class="row gx-0">
                <div class="col-lg-12">
                    <p class="main-text mb-5 text-left">
                    If you are interested in collaborating with us or have any general enquiry/suggestion, please fill out the form below.
                    </p>
                 </div>
            </div>
        </div>
        <div class="container">
        
                <div class="row col-lg-12">
                <div class="col-lg-5">  
                <?php $interested=isset($_GET['name']) ? $_GET['name'] : "";
               // print_r($interested=="For Sponsors");
                ?>
              <form method="get" action=""> 
                       
                            <input type="text" placeholder="Name*" name="firstname" required class="form-control"/>
  <input type="email" placeholder="Email ID*" name="email" required class="form-control"/>
  <label class="main-text">I am a</label>
  <select style="width: 100%;  border-radius: 5px; padding:2%" name="interest">
  <option value="Woman interested in getting screened" selected disabled>Woman interested in getting screened</option>
  <option value="Hospital" <?php echo($interested=="For Hospitals" ? 'selected' : ''); ?>>Hospital</option>
                    <option value="Doctor" <?= ($interested=="For Doctors") ? 'selected' : '' ?>>Doctor</option>
                    <option value="Distributor" <?= ($interested=="For Distributors") ? 'selected' : '' ?>>Distributor</option>
                    <option value="Screening Partner" <?= ($interested=="For Screening Partners") ? 'selected' : '' ?>>Screening Partner</option>
                    <option value="Sponsor" <?php echo($interested=="For Sponsors" ? 'selected' : ''); ?>>Sponsor</option>
                    </select>
  <input type="text" placeholder="Phone Number*" name="mobile" required class="form-control"/>
 <textarea placeholder="Message*" rows="8" name="message" required class="form-control"></textarea>
<input type="checkbox" class="mt-3" checked><span class="main-text mt-3">&nbsp;&nbsp;Subscribe to Niramai Newsletter</span>
<p class="mt-3"><button name="sendmail" class="btn common-btn" type="submit">SUBMIT</button></p>
</form>
      </div>  
      
      <div class="col-lg-1">  
              </div>
               
              <div class="col-lg-6">  
              
              <h4 class="mb-1 main-text" style="color:#000;font-weight:500;font-size:24px">
                FAQ</h4>
                
<div class="questionwrapper">
  
  <div class="questioncontainer">
    <div class="question">
    Does Niramai Screening detect cancer? 
    </div>
    <div class="answercont">
      <div class="answer main-text">
      Niramai Breast Health Test is irradiation free. non-touch, painless, privacy aware, works for women of all ages and completely safe. Niramai’s test is approved by multiple hospitals and is found to identify breast cancer at a much earlier stage than traditional methods or self-examination.
  </div>
  </div>
    </div>

    <div class="questioncontainer">
    <div class="question">
    What will happen/ how will the test happen?
    </div>
    <div class="answercont">
      <div class="answer main-text">
      The person is made to relax in an air conditioned room or a room with a cooler for about 10-15 minutes. Only your upper body clothes need to be removed with hands clasped behind the head &  hair tied above. 
 </div>
  </div>
    </div>

    <div class="questioncontainer">
    <div class="question">
    What is a Thermal Image?
    </div>
    <div class="answercont">
      <div class="answer main-text">
      Thermal images are a heat map showing the temperature variations of the skin. They represent the temperature distribution of 400,000 points on your breasts.
 </div>
  </div>
    </div>

    <div class="questioncontainer">
    <div class="question">
    What if I have a wound dressing over the breast region? 
   </div>
    <div class="answercont">
      <div class="answer main-text">
      The wound dressing will have to be removed for accurate measurements of the heat patterns.
</div>
  </div>
    </div>

    <div class="questioncontainer">
    <div class="question">
    Will the technician be male or female? </div>
    <div class="answercont">
      <div class="answer main-text">
      Only female technicians will be attending the woman or men who opt for the Niramai Breast Health Test 
</div>
  </div>
    </div>


    <div class="questioncontainer">
    <div class="question">
    What does the image you capture look like? Are you capturing a photograph?
  </div>
    <div class="answercont">
      <div class="answer main-text">
      The images we capture will be thermal images, not photographs.
</div>
  </div>
    </div>

    <div class="questioncontainer">
    <div class="question">
    What radiation does Niramai use? 
</div>
    <div class="answercont">
      <div class="answer main-text">
      The Niramai Breast Health test does not emit any radiation. It captures the heat patterns emitted from your body and analyses it.
    <br/>  Five thermal images of the person are captured for (a) frontal (b) left-lateral (c) left-oblique (d) Right-lateral and (e) Right -oblique within 2 minutes.  
<br/>
Our Niramai solution analyzes the thermal images and generates reports for review by our radiologist. Radiologist reviews the images and reports and gives a final guidance. Niramai test helps in detecting heat patterns that look abnormal. If  detected, then we will recommend further follow up with detailed diagnostic tests.

</div>
  </div>
    </div>

    <div class="questioncontainer">
    <div class="question">
    Are there any side effects/is our test safe/is your test radiation free/any long term consequences of niramai test/does this test increase the chances of getting breast cancer? 
</div>
    <div class="answercont">
      <div class="answer main-text">
      Niramai Breast Health Test is a safe, no-contact, non-invasive test. It is radiation-free and does not use any harmful radiation like x-rays
</div>
  </div>
    </div>

    <div class="questioncontainer">
    <div class="question">
    What precautions should I take before the screening?
</div>
    <div class="answercont">
      <div class="answer main-text">
      Patients suffering from colds, influenza, fever and similar issues should not be scheduled for a screening on the day. Alcoholic beverages  should not be consumed for 12 hours prior to the test. Hot or cold beverages should be avoided for at least one full hour before examination. Aspirins, pain medications, vasodilators, constrictors should be avoided for 24 hours prior to test. Women who are currently in their menstrual cycle can take the test after completing the cycle.
</div>
  </div>
    </div>

    <div class="questioncontainer">
    <div class="question">
    Are you checking for breast cancer? 
</div>
    <div class="answercont">
      <div class="answer main-text">
      Yes, Niramai Breast Health Test is radiation free. non-touch, painless, privacy aware, works for women of all ages and completely safe. Niramai’s test is approved by multiple hospitals and is found to identify breast cancer at a much earlier stage than traditional methods or self-examination.
</div>
  </div>
    </div>

    <div class="questioncontainer">
    <div class="question">
    How is the Niramai Breast Health Test different from Ultrasound and Mammography? 
</div>
    <div class="answercont">
      <div class="answer main-text">
      Niramai Breast Health Test is irradiation free. non-touch, painless, privacy aware, works for women of all ages and completely safe. Niramai’s test is approved by multiple hospitals and is found to identify breast cancer at a much earlier stage than traditional methods or self-examination.</div>
  </div>
    </div>


</div>
                <hr/> <h4 class="mb-1 main-text" style="color:#000;font-weight:500;font-size:24px">
                {{$setting->company_name}}</h4>
                <p class="main-text mb-4" style="text-align: justify;">

                
                Ground Floor, Innova Pearl, No. 17, 5th<br/>
                 Block, Koramangala Industrial Layout, <br/>
                 Bangalore, 560095<br/>
Phone: +91 8660922978<br/>
Email: contact@niramai.com
                    </p>
                    
                    <hr class="mt-3">
                 
                    <h4 class="mb-1 main-text" style="color:#000;font-weight:500;font-size:24px">
                Follow Us:</h4>
                
                                    <div class="social-links-footer">
                            <ul>
                                <li>
                                    <a href="{{$setting->facbook}}" style="border-radius: 0px !important;"><i class="fab fa-facebook-f"></i></a>
                                </li>
                                <li>
                                    <a href="{{$setting->twitter}}" style="border-radius: 0px !important;"><i class="fab fa-twitter"></i></a>
                                </li>
                                <li>
                                    <a href="{{$setting->whatsapp}}" style="border-radius: 0px !important;"><i class="fab fa-whatsapp"></i></a>
                                </li>
                                <li>
                                    <a href="{{$setting->instrgram}}" style="border-radius: 0px !important;"><i class="fab fa-instagram"></i></a>
                                </li>
                                <li>
                                    <a href="{{$setting->youtube}}" style="border-radius: 0px !important;"><i class="fab fa-youtube"></i></a>
                                </li>
                         </ul>
                                       </div>
                               
                </div>
                </div>
        </div>
    </div>
</section>

@endsection

@push('scripts')
  <script type="text/javascript">
  
  let question = document.querySelectorAll(".question");

question.forEach(question => {
  question.addEventListener("click", event => {
    const active = document.querySelector(".question.active");
    if(active && active !== question ) {
      active.classList.toggle("active");
      active.nextElementSibling.style.maxHeight = 0;
    }
    question.classList.toggle("active");
    const answer = question.nextElementSibling;
    if(question.classList.contains("active")){
      answer.style.maxHeight = answer.scrollHeight + "px";
    } else {
      answer.style.maxHeight = 0;
    }
  })
})

  </script>
  @endpush
<?php
  if(isset($_GET['sendmail'])){
    $subject = "Niramai";
    $firstname=$_GET['firstname'];
    $email="contact@niramai.com";
    $mobile=$_GET['mobile'];
    $interest=$_GET['interest'];
    $message=$_GET['message'];
    
  $header = "MIME-Version: 1.0\r\n";
     $header .= "Content-type: text/html\r\n";
   
     $emailcontent = '<!DOCTYPE>
     <html>
     <head></head>
     <body>
     <table width=100% style="border:1px solid #cccccc;">';
    $emailcontent .= '<tr align="center"><td> <h6>Name : </h6></td><td align="left"> '.$firstname.' </td></tr>';
    $emailcontent .= '<tr align="center"><td> <h6>Email : </h6></td><td align="left"> '.$email.' </td></tr>';
    $emailcontent .= '<tr align="center"><td> <h6>Woman interested in getting screened : </h6></td><td align="left"> '.$interest.' </td></tr>';
    $emailcontent .= '<tr align="center"><td> <h6>Phone Number : </h6></td><td align="left"> '.$mobile.' </td></tr>';
    $emailcontent .= '<tr align="center"><td"> <h6>Message : </h6></td><td align="left"> '.$message.' </td></tr></table></body></html>';
    $retval = mail ($email,$subject,$emailcontent,$header);
  
    if ($retval == true) {
      echo "<script>";
      echo "Swal.fire('Message Sent Successfully!', 'Thank you for contacting us. We will get back to you shortly.', 'success');";
      echo "</script>";
    } else {
      echo "<script>";
    echo "Swal.fire('Message Sent Failure!', 'Try Again..', 'error');";
      echo "</script>";
    }
  }
  ?>   
