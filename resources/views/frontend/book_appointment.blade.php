@extends('layouts.app')

@section('content')
<style>
  /* a {
	color: #fff;
	text-decoration: none;
} */


@media only screen and (max-width: 600px) {
.tabs>input:checked+span
 {
    color: #000 !important;
    font-size: 9.5px !important;
    font-weight: 400 !important;
}
.tabs>input, .tabs>span
{
  color: #000 !important;
    font-size: 9.5px !important;
    font-weight: 400 !important;
}
}
  a:hover {
    color: #d7358c !important;
  }

  header {
    color: #fff;
    text-align: center;
    min-height: 140px;
    margin-bottom: 60px;
  }

  header h1 {
    margin-top: 100px;
    font-size: 50px;
    margin-bottom: 20px;
    font-weight: 100;
  }

  header a {
    font-size: 18px;
    margin-left: 20px;
  }

  .copyright {
    font-size: 25px;
    font-weight: 100;
    color: #fff;
    text-align: center;
    margin-top: 100px;
  }

  /* Tabs Start */

  .ease {
    -webkit-transition: all .5s;
    -moz-transition: all .5s;
    -o-transition: all .5s;
    transition: all .5s;
  }

  .container {
    width: 100%;
    max-width: 1000px;
    margin: 0 auto;
  }

  .tabs {
    background: #fff;
    position: relative;
    margin-bottom: 50px;
    border: 1px solid #dcdee4;
  }

  .tabs>input,
  .tabs>span {
    width: 35%;
    height: 60px;
    line-height: 60px;
    position: absolute;
    top: -5px;
  }

  .tabs>input {
    cursor: pointer;
    filter: alpha(opacity=0);
    opacity: 0;
    position: absolute;
    z-index: 99;
  }

  .tabs>span {
    background: #f0f0f0;
    text-align: center;
    overflow: hidden;
  }

  .tabs>span i,
  .tabs>span {
    -webkit-transition: all .5s;
    -moz-transition: all .5s;
    -o-transition: all .5s;
    transition: all .5s;
  }

  .tabs>input:hover+span {
    background: rgba(255, 255, 255, .1);
  }

  .tabs>input:checked+span {
    background: #fff;
  }

  .tabs>input:checked+span,
  .tabs>input:hover+span {
    color: #000;
  }

  #tab-1,
  #tab-1+span {
    left: 0;
  }

  #tab-2,
  #tab-2+span {
    left: 33%;
  }

  #tab-3,
  #tab-3+span {
    left: 65%;
  }

  #tab-4,
  #tab-4+span {
    left: 60%;
  }

  #tab-5,
  #tab-5+span {
    left: 80%;
  }

  .tab-content {
    padding: 80px 20px 20px;
    width: 100%;
    min-height: 340px;
  }

  .tab-content section {
    width: 100%;
    display: none;
  }

  .tab-content section h1 {
    margin-top: 15px;
    font-size: 100px;
    font-weight: 100;
    text-align: center;
  }

  #tab-1:checked~.tab-content #tab-item-1 {
    display: block;
  }

  #tab-2:checked~.tab-content #tab-item-2 {
    display: block;
  }

  #tab-3:checked~.tab-content #tab-item-3 {
    display: block;
  }

  #tab-4:checked~.tab-content #tab-item-4 {
    display: block;
  }

  #tab-5:checked~.tab-content #tab-item-5 {
    display: block;
  }

  /* effect-3 */

  .effect-3 .line {
    background: #d7358c;
    width: 30%;
    height: 4px;
    position: absolute;
    top: 60px;
  }

  #tab-1:checked~.line {
    left: 1.5%;
  }

  #tab-2:checked~.line {
    left: 34%;
  }

  #tab-3:checked~.line {
    left: 67%;
  }

  #tab-4:checked~.line {
    left: 60%;
  }

  #tab-5:checked~.line {
    left: 80%;
  }


  input[type=text],input[type=email]{
    margin-top:7%;
    padding: 3%;
    margin-bottom: 7%;
  }

  .list{
    padding-left: 28px;
    background: url('{{base_url()}}public/images/li.png') no-repeat 0px 3px;
  }
</style>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">

<section>
  <div class="about-us">
    <div class="container">

      <div class="vertical-space-70"></div>
      <h3 class="main-text" style="text-align: center;font-size: 20px;font-weight: 900;">
        Early detection helps save lives test.
        <br /><br />Get yourself screened with NIRAMAI’s award winning Thermalytix solution.
      </h3>

      <div class="container">
        <div class="vertical-space-70"></div>
        <div class="tabs effect-3">
          <!-- tab-title -->
          <input type="radio" id="tab-1" name="tab-effect-3" checked="checked">
          <span class="main-text">Home Screening</span>

          <input type="radio" id="tab-2" name="tab-effect-3">
          <span class="main-text">Hospital/Clinic Screening</span>

          <input type="radio" id="tab-3" name="tab-effect-3">
          <span class="main-text">Organize Screening Camp</span>


          <div class="line ease"></div>

          <!-- tab-content -->
          <div class="tab-content">
            <section id="tab-item-1">
              <div class="vertical-space-10"></div>
              <div class="col-lg-12 row">
                <div class="col-lg-6">
                  <p class="main-text" style="font-size: 26px;font-weight: 900;">How home screening works?</h1>
                  <div class="vertical-space-10"></div>
                  <p class="main-text" style="font-size: 14px;font-weight: 900;color:#d7358c">1. Appointment booking and confirmation</p>
                  <p class="main-text" style="text-align: justify;">Once you raise a home screening request, Niramai executive will reach out to you to confirm the time of appointment and provide any information that you need. Niramai test is available for home screening in Bangalore, Hyderabad, Mumbai, Delhi NCR, Chennai and Pune.</p>
                  </p>

                  <div class="vertical-space-10"></div>
                  <p class="main-text" style="font-size: 14px;font-weight: 900;color:#d7358c">2. At-home screening</p>
                  <p class="main-text" style="text-align: justify;">
                    We will be assigning a well-trained female technician to conduct the checkup in the comfort of your home. The technician will carry all equipment required for the setup and reach the location at designated time. The overall process will take 40 minutes including time for setting up/wrap up and screening.
                  </p>


                  <div class="vertical-space-10"></div>
                  <p class="main-text" style="font-size: 14px;font-weight: 900;color:#d7358c">3. Reporting</p>
                  <p class="main-text" style="text-align: justify;">
                    Once the screening process is completed, you will receive your report within 1 working day. All the reports are certified by our panel of radiologist.
                  </p>
                  <div class="vertical-space-10"></div>

                  <p class="main-text">For any queries, please write to <a class="text-dark" href="mailto:homescreening@niramai.com"><b>homescreening@niramai.com.</b></a></p>
                </div>
                <div class="col-lg-1"></div>
                <div class="col-lg-5 text-center">
                <div class="vertical-space-30"></div>

                <select style="width: 80%;  border-radius: 5px; padding:2%" id="city">
                    <option value="0">Bengaluru</option>
                    <option value="1">Bhopal</option>
                    <option value="2">Chennai</option>
                    <option value="3">Delhi NCR</option>
                    <option value="4">Mumbai</option>
                    <option value="5">Pune</option>
                    <option value="6">Others</option>
                  </select>
                  <p class="mt-3"><button class="btn common-btn" type="submit" onclick="goToProperPage()">BOOK NEW APPOINTMENT</button></p>

           
                </div>
              </div>
            </section>
            <section id="tab-item-2">
              <div class="vertical-space-10"></div>
              <div class="col-lg-12 row">
                <div class="col-lg-6">
                  <div class="vertical-space-10"></div>
                  <p class="main-text" style="font-size: 14px;font-weight: 900;color:#d7358c">1. Book an appointment</p>
                  <p class="main-text" style="text-align: justify;">
                  Please share your details in the form to book an appointment. Niramai representative will contact you, provide more information about the test and will suggest a centre near to you. Niramai test is available in Bangalore, Hyderabad, Mumbai, Delhi, Chennai, Pune, Vadodara, Dehradun, Mysore, Bhopal, Gurgaon and Bhubaneswar.
                </p>

                  <div class="vertical-space-10"></div>
                  <p class="main-text" style="font-size: 14px;font-weight: 900;color:#d7358c">2. Get screened</p>
                  <p class="main-text" style="text-align: justify;">
                  Visit the diagnostic centre and get screened with absolute privacy. Payment for the tests need to be made to the centre directly.
                </p>


                  <div class="vertical-space-10"></div>
                  <p class="main-text" style="font-size: 14px;font-weight: 900;color:#d7358c">3. Get your reports and advice on further course of action</p>
                  <p class="main-text" style="text-align: justify;">
                  Receive your radiologist-certified breast health report over email or print copy.


                </p>
                  <div class="vertical-space-20"></div>

                  <p class="main-text">
                  Leave your details and we will get in touch with you to make the arrangements
                  </p>
                
                </div>
                <div class="col-lg-1"></div>
                <div class="col-lg-5 text-center">
                  <div class="vertical-space-5"></div>


                  <form>
  <input type="text" placeholder="Name*" class="form-control"/>
  <input type="text" placeholder="Mobile*" class="form-control"/>
  <input type="email" placeholder="Email*" class="form-control"/>
  <input type="text" placeholder="City*" class="form-control"/>
  <input type="text" placeholder="Pincode*(For location near you)" class="form-control"/>
<textarea placeholder="Message*" rows="8" class="form-control"></textarea>
<input type="checkbox"><span class="main-text">&nbsp;&nbsp;I authorise Niramai & its representatives to contact me to explain the features of Niramai solution.</span>
</form>
                <p class="mt-3"><button class="btn common-btn" type="submit" onclick="goToProperPage()">BOOK NEW APPOINTMENT</button></p>


                </div>
              </div>
            </section>
            <section id="tab-item-3">
            <div class="vertical-space-10"></div>
            <p class="main-text" style="font-size: 26px;font-weight: 900;">Want To Help?</h1>
     
              <div class="col-lg-12 row">
                <div class="col-lg-6">
                  <div class="vertical-space-10"></div>
                  <p class="main-text" style="text-align: justify;">
                  Niramai is conducting screening camps for women with no access to healthcare facilities. We welcome sponsorships from donors interested in supporting breast cancer screening for the under-privileged women.  </p>

                  <div class="vertical-space-10"></div>
                  <p class="main-text" style="font-size: 14px;font-weight: 900;color:#d7358c">For the Donor</p>
                 <li style="list-style-type: none;" class="main-text list"> Visible communication to the beneficiaries about the sponsorship during the camp.
                 </li>  
                 <li style="list-style-type: none;" class="main-text list"> 
                 Field report about the impact created after the camp is concluded.    </li>  
                 <li style="list-style-type: none;" class="main-text list"> 
                 80G benefits.
                   </li>  
                 <li style="list-style-type: none;" class="main-text list"> 
                 Feel good factor.
                  </li>  

                  <div class="vertical-space-10"></div>
                  <p class="main-text" style="font-size: 14px;font-weight: 900;color:#d7358c">For The Women</p>
                 <li style="list-style-type: none;" class="main-text list"> 
                 Cancer detection at their doorsteps.
                </li>  
                 <li style="list-style-type: none;" class="main-text list"> 
                 On the spot diagnosis and treatment guidance.
                 </li>
                  <li style="list-style-type: none;" class="main-text list"> 
                  Privacy aware, non-invasive and safe test.
                    </li>  
                 <li style="list-style-type: none;" class="main-text list"> 
                 Free of cost.
                   </li>  
                  <div class="vertical-space-20"></div>

                  <p class="main-text">
                  Leave your details and we will get in touch with you to make the arrangements
                </p>
                
                </div>
                <div class="col-lg-1"></div>
                <div class="col-lg-5 text-center">
                  <div class="vertical-space-5"></div>


                  <form>
  <input type="text" placeholder="Full Name*" class="form-control"/>
  <input type="email" placeholder="Email*" class="form-control"/>
  <input type="text" placeholder="Phone Number*" class="form-control"/>
<textarea placeholder="I am interested to contribute to screening camps" rows="8" class="form-control"></textarea>
<input type="checkbox" checked><span class="main-text">&nbsp;&nbsp;Subscribe to Niramai Newsletter</span>
</form>
                <p class="mt-3"><button class="btn common-btn" type="submit" onclick="goToProperPage()">BOOK NEW APPOINTMENT</button></p>


                </div>
              </div>
            </section>
          </div>
        </div>

      </div>
    </div>
  </div>
</section>

@if($book_appointment)
<!-- <section>
        <div class="container">
        		@foreach($book_appointment as $row)
            <div class="vertical-space-50"></div>
           <div class="card mb-3 booking-card">
              <div class="row g-4">
                <div class="col-md-4">
                   <img src="{{url('/')}}/public/images/book_appointment/{{$row->book_appointment_image}}"class="booking-card-img img-fluid" alt="...">
                </div>
                <div class="col-md-8">
                  <div class="card-body">
                    <h5 class="card-title">{{$row->book_appointment_name}}</h5>
                    <p class="card-text">{{$row->book_appointment_description}}</p>
                    <div class="vertical-space-10"></div>
                    <div class="row">
                        <div class="col-lg-4">
                            <ul class="booking-list">
                                <li>Benefit-1</li>
                                <li>Benefit-2</li>
                                <li>Benefit-3</li> 
                            </ul>
                        </div>
                        <div class="col-lg-4"></div>
                        <div class="col-lg-4">
                            <div class="text-right">
                                <h5 class="amount-text"><strike>$600</strike><span>$250</span></h5>
                                <p class="float-right mt-3"> <a href="#" class="btn common-btn">Book Appoinment <i class="fas fa-long-arrow-alt-right ms-2"></i></a></p>
                            </div>
                        </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
             @endforeach
        </div> -->
</section>
@endif
@endsection
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>

@push('scripts')
<script type="text/javascript">
  function goToProperPage() {
    var url;
    var location = document.getElementById("city").selectedIndex;
    if (location == 0) {
      window.open('https://hs.thermalytix.com/newbooking', '_blank');
    } else if (location == 1) {
      window.open('https://hs.thermalytix.com/newbooking', '_blank');
    } else if (location == 2) {
      window.open('https://hs.thermalytix.com/newbooking', '_blank');
    } else if (location == 3) {
      window.open('https://hs.thermalytix.com/newbooking', '_blank');
    } else if (location == 4) {
      window.open('https://hs.thermalytix.com/newbooking', '_blank');
    } else if (location == 5) {
      window.open('https://hs.thermalytix.com/newbooking', '_blank');
    } else if (location == 6) {
      window.open('https://hs.thermalytix.com/newbooking', '_blank');
    }
    // window.location.href=url;
  }
</script>
@endpush