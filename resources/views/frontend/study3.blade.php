@extends('layouts.app')

@section('content')
<style>
    #customers {
        font-family: Arial, Helvetica, sans-serif;
        border-collapse: collapse;
        width: 100%;
    }

    #customers td,
    #customers th {
        border: 1px solid #ddd;
        text-align: center;
        padding: 8px;
    }

    #customers tr:nth-child(even) {
        background-color: #f2f2f2;
    }

    #customers tr:hover {
        background-color: #ddd;
    }

    #customers th {
        padding-top: 12px;
        padding-bottom: 12px;
        text-align: center;
        background-color: #d7358c;
        color: white;
    }
    h4{
        color: #d7358c;
    }
</style>

<section>
    <div class="women">
        <div class="container">
            <div class="vertical-space-70"></div>
            <h4 class="main-title mb-1">Prospective Single Centre&nbsp;</h4>
            <div class="row gx-0">
                <div class="col-lg-12">
                    <p class="main-text mb-5 text-center">
                        Prospective single centre study to evaluate the clinical efficacy of Thermalytix™ over Mammography in women with dense and non-dense breasts
                </div>
            </div>
        </div>
        <div class="container">
            <h4 class="mb-1">Objective</h4>
            <div class="row gx-0">
                <div class="col-lg-12">
                    <p class="main-text mb-5" style="text-align: justify;">
                        <i class="fa fa-circle" style="font-size:8px;color: #d7358c;"></i>
                        To evaluate the diagnostic effectiveness of Thermalytix 
                        for breast cancer detection in both dense and non-dense breasts.
                    </p>
                </div>
            </div>
        </div>

        <div class="container">
            <h4 class="mb-1">Study Site And Duration</h4>
            <div class="row gx-0">
                <div class="col-lg-12">
                    <p class="main-text mb-5" style="text-align: justify;">
                        <i class="fa fa-circle" style="font-size:8px;color: #d7358c;"></i>
                        Study at Max Super-Specialty Hospital, Saket, Delhi, India
                    </p>
                </div>
            </div>
        </div>

        <div class="container">
            <h4 class="mb-1">Data and Method</h4>
            <div class="row gx-0">
                <div class="col-lg-12">
                    <p class="main-text mb-5" style="text-align: justify;">
                        <i class="fa fa-circle" style="font-size:8px;color: #d7358c;"></i>
                        A prospective, single-site blinded clinical study was conducted as per GCP guidelines on 687 women who walked-in for a mammography test from 15th December 2018 to 6th January 2020. All women between 29 to 78 years, who were not pregnant, lactating or diagnosed with cancer were included in the study. Among the 687 women recruited for the study, 459 women who complied with the study protocol were included in the analysis. All women underwent Thermalytix and mammography, and if there was a discrepancy between the Thermalytix and mammography results, a breast USG was recommended for a conclusive outcome. When both Thermalytix and mammography results were test-negative, the mammography report was considered the ground truth. The ground truth for positive cases of malignancies was confirmed by histopathological diagnosis. 168 women of the 459 women were classified to have dense breasts (ACR category C or D) and the rest were classified to have non-dense breasts (ACR category A or B). Out of the 459 eligible women, 21 breast cancer cases were detected in this study and there were no bilateral cancers.
                    </p>

                </div>
            </div>
        </div>

        <div class="container">
            <h4 class="mb-1">Results</h4>
            <div class="row gx-0">
                <div class="col-lg-12">
                    <p class="main-text" style="text-align: justify;">
                        <i class="fa fa-circle" style="font-size:8px;color: #d7358c;"></i>
                        Thermalytix™ demonstrated an overall sensitivity of 95.24% 
                        (95% CI, 76.18 to 99.88), specificity of 88.58% (95% CI, 85.23 to 91.41), PPV of 28.57% (95% CI, 18.4 to 40.62),
                         and NPV of 99.74% (95% CI, 98.58 to 99.99).
                    </p>
                    <p class="main-text" style="text-align: justify;">
                        In women with dense breasts with class C and D as per ACR category
                         (n = 168), Thermalytix™ showed a sensitivity of 100% (95% CI, 69.15 to 100.00) and a specificity of
                          81.64% (95% CI, 74.72 to 87.35). In comparison, out of these 168 women who had a breast density of 
                          ACR category C or D, 37 cases were reported as inconclusive (BI-RADS 0) by mammography. If all the 
                          37 cases reported as inconclusive were considered as positive for follow-up diagnostic work-up (n = 168),
                           the sensitivity of mammography would be 100% while the specificity would be a low 77.22% (95% CI, 69.88 to 83.50).
                            If all the 37 cases reported as inconclusive were considered as negative (n = 168), the sensitivity of mammography
                             would decrease to 70.00% (95% CI, 34.75 to 93.33) while the specificity would increase to 98.73%
                              (95% CI, 95.50 to 99.85). Furthermore, Thermalytix correctly identified all the malignant cases
                               in the cohort of people tagged as BIRAD 0 by mammography.</p>
                        <img src="{{url('/')}}/public/images/study3/graph.png" class="mt-5 rounded mx-auto d-block partner-img img-fluid" alt="...">
                    <p class="main-text  mt-5 mb-5" style="text-align: center;">
                        <span style="font-size:14px;color: #d7358c;">

                            Figure 6: </span>
                        Performance of Thermalytix and mammography in women with dense breast tissue</span>
                    </p>

                    <div class="row col-lg-12">
                        <div class="col-lg-4">
                            <img src="{{url('/')}}/public/images/study3/fig7a.png" class="mt-5 rounded mx-auto d-block partner-img img-fluid" alt="...">
                            <p class="main-text mt-5 mb-5" style="text-align: justify;">
                                <span style="font-size:14px;color: #d7358c;">

                                    Figure 7 :</span>
                                a) Bilateral mammographic views of a woman with dense breast tissue, which was reported as BIRADS 0 and was later diagnosed as benign lesion on USG.
                        </div>
                        <div class="col-lg-4">
                            <img src="{{url('/')}}/public/images/study3/fig7b.png" class="mt-5 rounded mx-auto d-block partner-img img-fluid" alt="...">
                            <p class="main-text mt-5 mb-5" style="text-align: justify;">
                                <span style="font-size:14px;color: #d7358c;">

                                    Figure 7 : </span>
                                b) Breast thermal images of the same woman generated by Thermalytix with annotations of a benign lesion shown with a green border. (Thermo-biological score: 0·14, Areolar score: 0·3, Vascular score: 0·34, Ensemble score: 0·1; Conclusion: Benign).
                        </div>
                        <div class="col-lg-4">
                            <img src="{{url('/')}}/public/images/study3/fig7c.png" class="mt-5 rounded mx-auto d-block partner-img img-fluid" alt="...">
                            <p class="main-text mt-5 mb-5" style="text-align: center;">
                        </div>
                    </div>


                    <div class="row col-lg-12">
                        <div class="col-lg-4">
                            <img src="{{url('/')}}/public/images/study3/fig8a.png" class="mt-5 rounded mx-auto d-block partner-img img-fluid" alt="...">
                            <p class="main-text mt-5 mb-5" style="text-align: justify;">
                                <span style="font-size:14px;color: #d7358c;">

                                    Figure 8 :</span>
                                a) Bilateral mammographic views of a woman with lesion reported as BI-RADS 0. MRI of the breast revealed a large area of clumped non-mass enhancement in the upper outer quadrant of the right breast which was further confirmed as intra-ductal carcinoma (IDC) on biopsy.
                        </div>
                        <div class="col-lg-4">
                            <img src="{{url('/')}}/public/images/study3/fig8b.png" class="mt-5 rounded mx-auto d-block partner-img img-fluid" alt="...">
                            <p class="main-text mt-5 mb-5" style="text-align: justify;">
                                <span style="font-size:14px;color: #d7358c;">

                                    Figure 8 : </span>
                                b) Breast thermal images generated by Thermalytix with annotations in the upper outer quadrant of the right
                                breast shown with a blue border. (Thermo-biological score: 0·94, Areolar score: 0, Vascular score: 0·74,
                                Ensemble score: 0·99; Conclusion: Malignant)
                            </p>
                        </div>
                        <div class="col-lg-4">
                            <img src="{{url('/')}}/public/images/study3/fig8c.png" class="mt-5 rounded mx-auto d-block partner-img img-fluid" alt="...">
                        </div>
                    </div>
                        <img src="{{url('/')}}/public/images/study3/graph2.png" class="mt-3 rounded mx-auto d-block partner-img img-fluid" alt="...">
                        <p class="main-text  mt-5 mb-5" style="text-align: center;">
                            <span style="font-size:14px;color: #d7358c;">

                                Figure 5: </span>
                            Performance of Thermalytix and mammography in women with dense breast tissue</span>
                        </p>
                    </div>
            </div>
        </div>

                    <div class="container">
                        <h4 class="mt-2">Conclusion</h4>
                        <div class="row gx-0">
                            <div class="col-lg-12">
                                <p class="main-text" style="text-align: justify;">
                                    <i class="fa fa-circle" style="font-size:8px;color: #d7358c;"></i>
                                    In this prospective, cross-sectional study, Thermalytix, an AI-based thermal imaging test that uses machine learning on breast thermal images, demonstrated high sensitivity and specificity in the cohort consisting of symptomatic and asymptomatic women and identified all malignancies in women younger than 50 years and pre-menopausal women. In women with ACR categories ‘c’ and ‘d’ dense breasts, while 22% of women had incomplete mammographic assessments, Thermalytix had 100% sensitivity.
                                </p>
                                <p class="main-text mb-5" style="text-align: justify;">
                                    <i class="fa fa-circle" style="font-size:8px;color: #d7358c;"></i>
                                    This study introduces Thermalytix, a promising radiation-free, automated, and privacy-aware test, for breast cancer screening with good clinical performance on women across breast densities. The relevance of this new test in clinical practice could be primarily for women under 50 years of age and for women with high breast densities, where there is an evident gap in the current screening modalities. Being a portable and affordable modality, Thermalytix can be used for community-based screenings and preventive care. Thus, Thermalytix can influence the screening programs in LMICs and enable early detection of breast cancer.
                                </p>

                            </div>
                        </div>
                    </div>

                    <div class="container">
                        <h4 class="mb-1">Publication</h4>
                        <div class="row gx-0">
                            <div class="col-lg-12">
                                <p class="main-text mb-5" style="text-align: justify;">
                                    <i class="fa fa-circle" style="font-size:8px;color: #d7358c;"></i>
                                    Bansal R, Aggarwal B, and Krishnan L. A prospective study of an AI-based breast cancer screening solution for resource-constrained settings. Journal of Clinical Oncology 2021 39:15_suppl, e13586-e13586. Abstract published with ASCO. <a href="https://ascopubs.org/doi/abs/10.1200/JCO.2021.39.15_suppl.e13586.">ASCOPUBS</a> Full paper under review with The Lancet group.
                                </p>

                            </div>
                        </div>
                    </div>


                </div>
</section>
@endsection