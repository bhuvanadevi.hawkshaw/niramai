@extends('layouts.app')

@section('content')
<style>
.main-title::after {
    content: "";
    display: block;
    background-color: #d7355c;
    width: 70px !important;
    height: 4px;
    position: absolute;
    border-radius: 25px;
    content: "";
    position: absolute;
    margin: 0 35%;
    top: -20px;
}

@media screen and (max-width: 680px){
    .partner-img{
       position: relative !important;
    top: -10px !important;
    }
  }

</style>

@if($partnerships)
        <section>
        <div class="partnership">
            <div class="container">
                @foreach($partnerships as $row)
                <div class="vertical-space-70"></div>
                
                 <h4 class="main-title mb-5">{{$row->partnerships_name}}</h4>
                <div class="row col-lg-12">
                    <div class="col-lg-6">
                        <img src="{{url('/')}}/public/images/partnerships/{{$row->partnerships_image}}" style="width:50%;height:100%" class="rounded mx-auto d-block partner-img img-fluid" alt="...">
                    </div>
                    <div class="col-lg-6">
                        <div class="vertical-space-30"></div>
                        <p class="main-text" style="text-align: justify;"><span class="common me-2">.</span>{{$row->partnerships_description}}</p>
                        <p><a href='{{base_url("contact")}}?name={{$row->partnerships_name}}' class="read-link ">Get In Touch</a></p>
                        <!-- <p class="mt-3"><a href="{{base_url('book_appointment')}}" class="btn common-btn">Book Appoinment <i class="fas fa-long-arrow-alt-right ms-2"></i></a></p> -->
                    </div>
            <div class="vertical-space-30"></div>
            </div>
             @endforeach
           </div>
         
        </div>
        <div class="container">
                <div class="vertical-space-70"></div>
                
                 <h4 class="main-title">Benefits To the Donor</h4>
        
                        <p class="main-text">
                        <span class="common me-2">.</span>     Visible communication to the beneficiaries about the sponsorship during the camp.
                        </p>

                        <p class="main-text">
                        <span class="common me-2">.</span> Field report about the impact created after the
                            camp is concluded.
                        </p>
                        <p class="main-text">
                        <span class="common me-2">.</span>       80G benefits.
                        </p>
                        <p class="main-text">
                        <span class="common me-2">.</span>     Feel good factor.
                        </p>
        </div>
        </section>

@endif
    
  
    @endsection
