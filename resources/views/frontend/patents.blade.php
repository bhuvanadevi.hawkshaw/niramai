@extends('layouts.app')

@section('content')
<style>
p{
    text-align: justify;
}
#customers {
        font-family: Arial, Helvetica, sans-serif;
        border-collapse: collapse;
        width: 100%;
    }

    #customers td,
    #customers th {
        border: 1px solid #ddd;
        text-align: center;
        padding: 8px;
    }

    #customers tr:nth-child(even) {
        background-color: #f2f2f2;
    }

    #customers tr:hover {
        background-color: #ddd;
    }

    #customers th {
        padding-top: 12px;
        padding-bottom: 12px;
        text-align: center;
        background-color: #d7358c;
        color: white;
    }
    h4{
        color: #d7358c;
    }
    .flexbox {
  display: flex;
  flex-direction: row;
  justify-content: space-around;
  width: 90%;
  padding-left:150px;
  flex-wrap: wrap;
}

@media only screen and (max-width: 600px) {
.flexbox {
  display: flex !important;
  flex-direction: row !important;
  justify-content: space-around !important;
  width: 100% !important;
  padding-left:0px !important;
  flex-wrap: wrap !important;
}
}

.flexcard {
  display: flex;
  flex-direction: column;
  justify-content: flex-start;
  width: 16%;
  align-items: baseline;
  height: 325px;
  border-radius: 20px;
  text-align: center;
}

.flexcardNumber {
  width: 70%;
  display: flex;
  justify-content: center;
  align-items: center;
  height: 50px;
  margin: 10px 0px;
  border-radius: 0px 50px 50px 0px;
  font-family: 'Jost', sans-serif;
  color: white;
  text-shadow: 0px 3px 5px black;
  font-weight: 500;
  font-size: 20px;
  position: relative;
}

.flexcardTitle {
  font-family: 'Jost', sans-serif;
  text-transform: uppercase;
  font-size: 26px;
  font-weight: 400;
}

.main-text {
  font-family: 'Jost', sans-serif;
  text-align: center;
  font-weight: 300;
}

.flexcardNumber:before {
  content: '';
  width: 34px;
  height: 90px;
  position: absolute;
  left: -33px;
  top: 0px;
  border-radius: 50px 0px 0px 50px;
  z-index: -1;
/* background: #95B7A2; *//* background: -webkit-linear-gradient(bottom, #95B7A2, #AED2BC); */
  background: -moz-linear-gradient(bottom, #95B7A2, #AED2BC);
/* background: linear-gradient(to top, #95B7A2, #AED2BC); */;
}

.flexcardNumber:after {
  content: '';
  width: 25px;
  height: 40px;
  position: absolute;
  left: -25px;
  bottom: -40px;
  border-radius: 50px 0px 0px 50px;
  z-index: -1;
}

.flex {
  display: flex;
  justify-content: center;
  width: 100%;
  margin: 10px auto;
}

/* PİNK CARD */

.flexcardPink {
  background: #fffafd;
  position: relative;
    box-shadow: 2px 2px 1px #d7358c ;
    border-radius: 20px 20px 0px 20px;
}
.flexcardPink:before {
    content: '';
    width: 0px;
    height: 50px;
    /* background: red; */
    position: absolute;
    bottom: 0;
    right: 0;
    border-left: 100px solid transparent;
    border-bottom: 60px solid #F34EAE;
}
.flexcardNumberPink {
  background: #FDFFFE;
  background: -webkit-linear-gradient(right, #FDFFFE, #F34EAE);
  background: -moz-linear-gradient(right, #FDFFFE, #F34EAE);
  background: linear-gradient(to left, #FDFFFE, #F34EAE);
  box-shadow: 0px 2px 2px #F34EAE;
}

.flexcardNumberPink:before {
  background: #F34EAE;
}

.flexcardNumberPink:after {
  background: #d24095;
}

/* Blue CARD */

.flexcardBlue {
  background: #fffafd;
  position: relative;
    box-shadow: 2px 2px 1px #92E8EB ;
    border-radius: 20px 20px 0px 20px;
}
.flexcardBlue:before {
    content: '';
    width: 0px;
    height: 50px;
    /* background: red; */
    position: absolute;
    bottom: 0;
    right: 0;
    border-left: 100px solid transparent;
    border-bottom: 60px solid #92E8EB;
}
.flexcardNumberBlue {
  background: #FDFFFE;
  background: -webkit-linear-gradient(right, #FDFFFE, #92E8EB);
  background: -moz-linear-gradient(right, #FDFFFE, #92E8EB);
  background: linear-gradient(to left, #FDFFFE, #92E8EB);
  box-shadow: 0px 2px 2px #92E8EB;
}

.flexcardNumberBlue:before {
  background: #92E8EB;
}

.flexcardNumberBlue:after {
  background: #92E8EB;
}

/* Green CARD */

.flexcardGreen {
  background: #fffafd;
  position: relative;
    box-shadow: 2px 2px 1px #AED2BC ;
    border-radius: 20px 20px 0px 20px;
}
.flexcardGreen:before {
    content: '';
    width: 0px;
    height: 50px;
    /* background: red; */
    position: absolute;
    bottom: 0;
    right: 0;
    border-left: 100px solid transparent;
    border-bottom: 60px solid #AED2BC;
}
.flexcardNumberGreen {
  background: #FDFFFE;
  background: -webkit-linear-gradient(right, #FDFFFE, #AED2BC);
  background: -moz-linear-gradient(right, #FDFFFE, #AED2BC);
  background: linear-gradient(to left, #FDFFFE, #AED2BC);
  box-shadow: 0px 2px 2px #AED2BC;
}

.flexcardNumberGreen:before {
  background: #AED2BC;
}

.flexcardNumberGreen:after {
  background: #AED2BC;
}


/* Orange CARD */

.flexcardOrange {
  background: #fffafd;
  position: relative;
    box-shadow: 2px 2px 1px #EB984E ;
    border-radius: 20px 20px 0px 20px;
}
.flexcardOrange:before {
    content: '';
    width: 0px;
    height: 50px;
    /* background: red; */
    position: absolute;
    bottom: 0;
    right: 0;
    border-left: 100px solid transparent;
    border-bottom: 60px solid #EB984E;
}
.flexcardNumberOrange {
  background: #FDFFFE;
  background: -webkit-linear-gradient(right, #FDFFFE, #EB984E);
  background: -moz-linear-gradient(right, #FDFFFE, #EB984E);
  background: linear-gradient(to left, #FDFFFE, #EB984E);
  box-shadow: 0px 2px 2px #EB984E;
}

.flexcardNumberOrange:before {
  background: #EB984E;
}

.flexcardNumberOrange:after {
  background: #EB984E;
}


/* Gray CARD */

.flexcardGray {
  background: #fffafd;
  position: relative;
    box-shadow: 2px 2px 1px #ABB2B9;
    border-radius: 20px 20px 0px 20px;
}
.flexcardGray:before {
    content: '';
    width: 0px;
    height: 50px;
    /* background: red; */
    position: absolute;
    bottom: 0;
    right: 0;
    border-left: 100px solid transparent;
    border-bottom: 60px solid #ABB2B9;
}
.flexcardNumberGray {
  background: #FDFFFE;
  background: -webkit-linear-gradient(right, #FDFFFE, #ABB2B9);
  background: -moz-linear-gradient(right, #FDFFFE, #ABB2B9);
  background: linear-gradient(to left, #FDFFFE, #ABB2B9);
  box-shadow: 0px 2px 2px #ABB2B9;
}

.flexcardNumberGray:before {
  background: #ABB2B9;
}

.flexcardNumberGray:after {
  background: #ABB2B9;
}


/* Purple CARD */

.flexcardPurple {
  background: #fffafd;
  position: relative;
    box-shadow: 2px 2px 1px #A569BD;
    border-radius: 20px 20px 0px 20px;
}
.flexcardPurple:before {
    content: '';
    width: 0px;
    height: 50px;
    /* background: red; */
    position: absolute;
    bottom: 0;
    right: 0;
    border-left: 100px solid transparent;
    border-bottom: 60px solid #A569BD;
}
.flexcardNumberPurple {
  background: #FDFFFE;
  background: -webkit-linear-gradient(right, #FDFFFE, #A569BD);
  background: -moz-linear-gradient(right, #FDFFFE, #A569BD);
  background: linear-gradient(to left, #FDFFFE, #A569BD);
  box-shadow: 0px 2px 2px #A569BD;
}

.flexcardNumberPurple:before {
  background: #A569BD;
}

.flexcardNumberPurple:after {
  background: #A569BD;
}


/* Yellow CARD */

.flexcardYellow {
  background: #fffafd;
  position: relative;
    box-shadow: 2px 2px 1px #F4D03F;
    border-radius: 20px 20px 0px 20px;
}
.flexcardYellow:before {
    content: '';
    width: 0px;
    height: 50px;
    /* background: red; */
    position: absolute;
    bottom: 0;
    right: 0;
    border-left: 100px solid transparent;
    border-bottom: 60px solid #F4D03F;
}
.flexcardNumberYellow {
  background: #FDFFFE;
  background: -webkit-linear-gradient(right, #FDFFFE, #F4D03F);
  background: -moz-linear-gradient(right, #FDFFFE, #F4D03F);
  background: linear-gradient(to left, #FDFFFE, #F4D03F);
  box-shadow: 0px 2px 2px #F4D03F;
}

.flexcardNumberYellow:before {
  background: #F4D03F;
}

.flexcardNumberYellow:after {
  background: #F4D03F;
}
/* Sky CARD */

.flexcardSky {
  background: #fffafd;
  position: relative;
    box-shadow: 2px 2px 1px #5DADE2;
    border-radius: 20px 20px 0px 20px;
}
.flexcardSky:before {
    content: '';
    width: 0px;
    height: 50px;
    /* background: red; */
    position: absolute;
    bottom: 0;
    right: 0;
    border-left: 100px solid transparent;
    border-bottom: 60px solid #5DADE2;
}
.flexcardNumberSky {
  background: #FDFFFE;
  background: -webkit-linear-gradient(right, #FDFFFE, #5DADE2);
  background: -moz-linear-gradient(right, #FDFFFE, #5DADE2);
  background: linear-gradient(to left, #FDFFFE, #5DADE2);
  box-shadow: 0px 2px 2px #5DADE2;
}

.flexcardNumberSky:before {
  background: #5DADE2;
}

.flexcardNumberSky:after {
  background: #5DADE2;
}

/* Red CARD */

.flexcardRed {
  background: #fffafd;
  position: relative;
    box-shadow: 2px 2px 1px #EC7063;
    border-radius: 20px 20px 0px 20px;
}
.flexcardRed:before {
    content: '';
    width: 0px;
    height: 50px;
    /* background: red; */
    position: absolute;
    bottom: 0;
    right: 0;
    border-left: 100px solid transparent;
    border-bottom: 60px solid #EC7063;
}
.flexcardNumberRed {
  background: #FDFFFE;
  background: -webkit-linear-gradient(right, #FDFFFE, #EC7063);
  background: -moz-linear-gradient(right, #FDFFFE, #EC7063);
  background: linear-gradient(to left, #FDFFFE, #EC7063);
  box-shadow: 0px 2px 2px #EC7063;
}

.flexcardNumberRed:before {
  background: #EC7063;
}

.flexcardNumberRed:after {
  background: #EC7063;
}

/* Thick CARD */

.flexcardThick {
  background: #fffafd;
  position: relative;
    box-shadow: 2px 2px 1px #1A5276;
    border-radius: 20px 20px 0px 20px;
}
.flexcardThick:before {
    content: '';
    width: 0px;
    height: 50px;
    /* background: red; */
    position: absolute;
    bottom: 0;
    right: 0;
    border-left: 100px solid transparent;
    border-bottom: 60px solid #1A5276;
}
.flexcardNumberThick {
  background: #FDFFFE;
  background: -webkit-linear-gradient(right, #FDFFFE, #1A5276);
  background: -moz-linear-gradient(right, #FDFFFE, #1A5276);
  background: linear-gradient(to left, #FDFFFE, #1A5276);
  box-shadow: 0px 2px 2px #1A5276;
}

.flexcardNumberThick:before {
  background: #1A5276;
}

.flexcardNumberThick:after {
  background: #1A5276;
}
/* RESPONSİVE */

@media only screen and (max-width: 800px) {

  .flexcard {
    width: 35%;
    margin-top: 20px;
  }

  img.flexcardimgItem {
    width: 30%;
  }

}

@media only screen and (max-width: 500px) {

  .flexcard {
    width: 70%;
  }


}
footer {
    display: flex;
    width: 100%;
    justify-content: center;
    margin: 50px;
    font-family: 'Jost', sans-serif;
    font-size: 15px;
}
</style>

<section>
    <div class="women">
        <!-- <div class="container">
            <div class="vertical-space-70"></div>
            <h4 class="main-title mb-1">Thermalytix</h4>
            <p class="main-text text-center">
                        The core of  NIRAMAI solution is Thermalytix, a computer aided diagnostic engine that is powered by Artificial Intelligence. The solution uses a high resolution thermal sensing device and a cloud hosted analytics solution for analysing the thermal images. Our SaaS solution has been developed using big data analytics, artificial intelligence and machine learning for reliable, early and accurate breast cancer screening.
   
                      </div> -->
              <div class="vertical-space-70"></div>
              <h4 class="text-center text-dark"  id="Patents">
              <b>    The innovative methods used in our solution have led to 10 Granted US patents.
</b></h4>      
           
        </div>

        <div class="vertical-space-60"></div>
         
        <div class="flexbox">
        <div class="flexcard flexcardPink">
            <div class="flexcardNumber flexcardNumberPink">01</div>
            <div class="flex flexcardTitle"></div>
            <div class="flex  main-text main-text">
            Privacy booth for breast cancer screening (Granted by USPTO recently)
      </div>
            <!-- <div class="flex flexcardImg">
              <img class="flexcardimgItem"
                    src="https://cdn.pixabay.com/photo/2017/01/10/23/01/seo-1970475_960_720.png" alt="">
                
                  </div> -->
      
                  </div>
        <div class="flexcard flexcardGreen">
            <div class="flexcardNumber flexcardNumberGreen">02</div>
            <div class="flex flexcardTitle"></div>
            <div class="flex  main-text">
            Classifying hormonal receptor status of malignant tumorous tissue from breast thermographic images, US 10,368,846</div>
 <!-- <div class="flex flexcardImg"><img class="flexcardimgItem"
                    src="https://cdn.pixabay.com/photo/2017/01/10/23/01/seo-1970475_960_720.png" alt=""></div>
        -->
                  </div>
        <div class="flexcard flexcardOrange">
            <div class="flexcardNumber flexcardNumberOrange">03</div>
            <div class="flex flexcardTitle"></div>
            <div class="flex  main-text">
            Thermography based breast cancer screening using a measure of symmetry,  US 10,307,141
 </div>
            <!-- <div class="flex flexcardImg"><img class="flexcardimgItem"
                    src="https://cdn.pixabay.com/photo/2017/01/10/23/01/seo-1970475_960_720.png" alt=""></div>
       -->
                  </div>
        <div class="flexcard flexcardBlue">
            <div class="flexcardNumber flexcardNumberBlue">04</div>
            <div class="flex flexcardTitle"></div>
            <div class="flex  main-text">
            Blood vessel extraction in two-dimensional thermography, US 10,198,670
 </div>
            <!-- <div class="flex flexcardImg"><img class="flexcardimgItem"
                    src="https://cdn.pixabay.com/photo/2017/01/10/23/01/seo-1970475_960_720.png" alt=""></div>
       -->
                  </div>
        <div class="flexcard flexcardGray">
            <div class="flexcardNumber flexcardNumberGray">05</div>
            <div class="flex flexcardTitle"></div>
            <div class="flex  main-text">
            Automatic segmentation of breast tissue in a thermographic image, US 10,068,330
</div>
            <!-- <div class="flex flexcardImg"><img class="flexcardimgItem"
                    src="https://cdn.pixabay.com/photo/2017/01/10/23/01/seo-1970475_960_720.png" alt=""></div>
       -->
                  </div>

        <div class="vertical-space-30"></div>
        </div>
        <div class="flexbox">
          <div class="flexcard flexcardPurple">
            <div class="flexcardNumber flexcardNumberPurple">06</div>
            <div class="flex flexcardTitle"></div>
            <div class="flex  main-text">
Software tool for breast cancer screening, US 9,898,817
</div>
            <!-- <div class="flex flexcardImg"><img class="flexcardimgItem"
                    src="https://cdn.pixabay.com/photo/2017/01/10/23/01/seo-1970475_960_720.png" alt=""></div>
      -->
                  </div>
        <div class="flexcard flexcardYellow">
            <div class="flexcardNumber flexcardNumberYellow">07</div>
            <div class="flex flexcardTitle"></div>
            <div class="flex  main-text">
            Contour-based determination of malignant tissue in a thermal image, US 9,865,052
</div>
            <!-- <div class="flex flexcardImg"><img class="flexcardimgItem"
                    src="https://cdn.pixabay.com/photo/2017/01/10/23/01/seo-1970475_960_720.png" alt=""></div>
       -->
                  </div>


        <div class="flexcard flexcardSky">
            <div class="flexcardNumber flexcardNumberSky">08</div>
            <div class="flex flexcardTitle"></div>
            <div class="flex  main-text">
            Software interface tool for breast cancer screening, US  10,055,542
</div>
            <!-- <div class="flex flexcardImg"><img class="flexcardimgItem"
                    src="https://cdn.pixabay.com/photo/2017/01/10/23/01/seo-1970475_960_720.png" alt=""></div>
      -->
                  </div>

        <div class="flexcard flexcardRed">
            <div class="flexcardNumber flexcardNumberRed">09</div>
            <div class="flex flexcardTitle"></div>
            <div class="flex  main-text">
            System and method for detecting cancerous tissue from a thermal image, US 9,622,698
</div>
            <!-- <div class="flex flexcardImg"><img class="flexcardimgItem"
                    src="https://cdn.pixabay.com/photo/2017/01/10/23/01/seo-1970475_960_720.png" alt=""></div>
         -->
                  </div>
        <div class="flexcard flexcardThick">
            <div class="flexcardNumber flexcardNumberThick">10</div>
            <div class="flex flexcardTitle"></div>
            <div class="flex  main-text">
            Detecting tumorous breast tissue in a thermal image, US 9,486,146
</div>
            <!-- <div class="flex flexcardImg"><img class="flexcardimgItem"
                    src="https://cdn.pixabay.com/photo/2017/01/10/23/01/seo-1970475_960_720.png" alt=""></div>
        -->
                  </div>

        </div>
        <div class="vertical-space-40"></div>
      
        <div class="container">
            <p class="main-text" style="text-align: left;">
                        <i class="fa fa-circle" style="font-size:8px;color: #d7358c;"></i>
                        Our novel algorithms have also been peer-reviewed in international scientific conferences.

        </p>
        <p class="main-text"  style="text-align: left;">
                        <i class="fa fa-circle" style="font-size:8px;color: #d7358c;"></i>
                        List of Peer-Reviewed Publications in Medical and AI journals  (Access Restricted)

        </p>
        </div>
</section>

        @endsection
      