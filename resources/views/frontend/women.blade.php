@extends('layouts.app')

@section('content')

<style type="text/css">
    .sample::after {
        content: "";
        display: block;
        background-color: #d7355c;
        width: 100px !important;
        height: 4px;
        position: absolute;
        border-radius: 25px;
        content: "";
        position: absolute;
        margin: 0 43%;
        top: -20px;
    }

    .sample {
        font-family: "Roboto Slab", serif;
        /* font-weight: 200; */
        font-size: 35px;
        padding: 0 0 15px;
        text-align: center;
        width: auto;
        margin: 0 auto;
        text-align: center;
        color: #000;
        font-weight: 400;
        position: relative;
        margin-bottom: 30px;
        display: table;
    }

    .women {
        font-family: 'Poppins'
    }

    .wrapper {
        margin: 0 auto;
        width: 100%;
        /* padding: 50px; */
    }

    .wrapper1 {
        margin: 0 auto;
        width: 100%;
        /* padding: 50px; */
    }

    @import url("https://pro.fontawesome.com/releases/v6.0.0-beta1/css/all.css");

    .ol-cards,
    .ol-cards * {
        /* margin: 0; */
        /* padding: 0; */
        box-sizing: border-box;
    }

    .ol-cards {
        --flapWidth: 2rem;
        --flapHeigth: 1rem;
        --iconSize: 1rem;
        --numberSize: 3rem;
        --colGapSize: 2rem;
        width: min(100%, 40rem);
        margin-inline: auto;
        display: grid;
        gap: 2rem;
        padding-inline-start: var(--flapWidth);
        font-family: sans-serif;
        color: #222;
        counter-reset: ol-cards-count;
        list-style: none;
    }

    .ol-cards>li {
        display: grid;
        grid-template-areas:
            "icon title nr"
            "icon descr nr";
        gap: 0 var(--colGapSize);
        align-items: center;
        /* padding: var(--colGapSize) var(--flapWidth) var(--colGapSize) 0; */
        border-radius: 1rem 5rem 5rem 1rem;
        background-image: linear-gradient(to bottom right, #ffffff, #ffffff);
        counter-increment: ol-cards-count;
        filter: drop-shadow(10px 10px 10px rgba(0, 0, 0, 0.25));
        box-shadow: inset 2px 2px 2px white, inset -1px -1px 1px rgba(0, 0, 0, 0.25);
    }

    .ol-cards>li>.icon {
        grid-area: icon;
        background: var(--accent-color);
        color: white;
        font-size: var(--iconSize);
        width: calc(2 * var(--flapWidth) + var(--iconSize));
        padding-block: 1rem;
        border-radius: 0 5rem 5rem 0;
        margin-inline-start: calc(-1 * var(--flapWidth));
        box-shadow: 2px 2px 4px rgba(0, 0, 0, 0.25);
        position: relative;
        display: grid;
        place-items: center;
    }

    .ol-cards>li>.icon::before {
        content: "";
        position: absolute;
        width: var(--flapWidth);

        height: calc(100% + calc(var(--flapHeigth) * 2));
        left: 0;
        top: calc(var(--flapHeigth) * -1);
        clip-path: polygon(0 var(--flapHeigth),
                100% 0,
                100% 100%,
                0 calc(100% - var(--flapHeigth)));
        background-color: var(--accent-color);
        background-image: linear-gradient(90deg,
                rgba(0, 0, 0, 0.5),
                rgba(0, 0, 0, 0.2));
        z-index: -1;
    }

    .ol-cards>li>.title {
        grid-area: title;
        font-weight: 600;
        font-size: 1.25rem;
    }

    .ol-cards>li>.descr {
        grid-area: descr;
    }

    .ol-cards>li::after {
        /* grid-area: nr;
  content: counter(ol-cards-count, decimal-leading-zero);
  color: var(--accent-color);
  font-size: var(--numberSize);
  font-weight: 700; */
    }
    @media only screen and (max-width: 768px) {
   .wrapper
   {
      margin:70px !important;
   }
    }

    @media (max-width: 40rem) {
        .ol-cards {
            --flapWidth: 1rem;
            --flapHeigth: 0.5rem;
            --iconSize: 2rem;
            --numberSize: 2rem;
            --colGapSize: 1rem;
        }
    }

    /* for demo */

    h1 {
        text-transform: uppercase;
        font-family: sans-serif;
        text-align: center;
        color: #222;
    }


    .square {
        width: 190px;
        height: 280px;
        position: relative;
        float: left;
    }

    .square:nth-child(2n) {
        background-color: #df6baf;
        box-shadow: 5px 0px 5px rgba(0, 0, 0, 0.2) inset;
    }

    .square:nth-child(2n+1) {
        background-color: #d7358c;
        box-shadow: 5px 0px 5px rgba(0, 0, 0, 0.1) inset;
    }

    .square:first-child {
        box-shadow: none;
    }

    .square p.text {
        font: 400 14px/18px Verdana;
        padding: 0 20px;
        color: #FFFFFF;
        text-align: center;
        line-height: 1.4;
    }

    .square:nth-child(2n+1) p.text {
        color: #F2F2F2;
        position: absolute;
        top: 30px;
    }

    .triagle {
        width: 0;
        height: 0;
        border-style: solid;
        border-width: 20px 0 20px 20px;
        position: absolute;
        top: 80px;
        line-height: 0;
        border-color: transparent transparent transparent #df6baf;
        z-index: 999;
        left: 190px;
    }

    .square:nth-child(2n+1) .triagle {
        border-color: transparent transparent transparent #d7358c;
    }

    .circle {
        background-color: #eed;
        border: 15px solid #d7358c;
        border-radius: 100px;
        height: 100px;
        width: 100px;
        position: absolute;
        top: -55px;
        left: 45px;
    }

    .square:nth-child(2n) .circle {
        border: 15px solid #df6baf;
        top: 230px;
    }

    .circle p {
        /* bottom: -50px;
    font-size: 50px;*/
        position: absolute;
        display: block;
        border: 5px solid #FCFCFA;
        border-radius: 50px;
        height: 70px;
        width: 70px;
        padding: 5px;
        box-shadow: 5px 5px 5px rgba(0, 0, 0, 0.2) inset;
    }

    .circle p span {
        font: 700 35px/50px Verdana;
        color: #A6A6A6;
    }
</style>

<section>
    <div class="women">
        <div class="container">
            <div class="vertical-space-70"></div>
            <h4 class="main-title mb-5">&nbsp;&nbsp;&nbsp;&nbsp;For Women</h4>
            <p class="main-text text-center col-lg-12">1 in 28 women in India get diagnosed with breast cancer and 1 out of 2 women diagnosed succumbs to it. </p>
            <div class=" row col-lg-12">
                <p class="main-text text-center">

                    <img src="{{base_url()}}public/images/women/women-28.png" style="width:70%;">
                </p>
            </div>
            <p class="main-text text-center  row col-lg-12">More than 60% of women in India report having breast cancer at an advanced stage. If detected early, this cancer is completely curable. Hence, early detection is the key to survival.</p>

            <p class="main-text text-center">1 woman is diagnosed with breast cancer every 3 minutes in India and 1 woman dies because of breast cancer every 6 minutes.
           
            <div class=" row col-lg-12">
                <p class="main-text text-center">

                    <img src="{{base_url()}}public/images/women/india-women.png" style="width:70%;">
                </p>
            </div>
         
            <p class="main-text text-center">Women should undergo a self breast examination once every month on the 5th day of period. For menopausal women, a fixed date of every month can be decided to remember the routine better. Also, it is advisable to get a screening once a year for ensuring your breast health.
            <div class="vertical-space-10"></div>
            <p class="main-text text-center"><a href="{{base_url('book_appointment')}}" class="btn common-btn">Book Appointment Now <i class="fas fa-long-arrow-alt-right ms-2"></i></a></p>
            </p>
            <div class="vertical-space-10"></div>


            <div class="vertical-space-70"></div>
            <h4 class="main-title mb-5">Our premium Screening features&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</h4>
            <div class="row col-lg-12">
                <div class="col-lg-4">
                    <div class="text-left col-lg-12">
                        <img src="{{base_url('public/images/screening features/all ages.png')}}" style="width: 60px;height: 60px;" >
                        <span class="main-text"> Works for women of all ages </span>
                    </div>
                </div>
                <div class="col-lg-5">
                    <div class="text-left col-lg-12">
                        <img src="{{base_url('public/images/screening features/detect lump.png')}}" style="width: 60px;height: 60px;">
                        <span class="main-text"> Can detect cancer long before lump can be felt</span>
                    </div>
                </div>
                <div class="col-lg-3">
                    <div class="text-left col-lg-12">
                        <img src="{{base_url('public/images/screening features/contactless.png')}}"  style="width: 60px;height: 60px;"/>
                        <span class="main-text"> Contactless process </span>
                    </div>
                </div>

            </div>
            <div class="vertical-space-20"></div>

            <div class="row col-lg-12">
                <div class="col-lg-4">
                    <div class="text-left">
                        <img src="{{base_url('public/images/screening features/certified technician.png')}}"  style="width: 60px;height: 60px;">
                        <span class="main-text"> Certified female technicians </span>
                    </div>
                </div>
                <div class="col-lg-5">
                    <div class="text-left">
                        <img src="{{base_url('public/images/screening features/radiologist certified.png')}}"  style="width: 60px;height: 60px;">
                        <span class="main-text">Radiologist-certified reports within 24hrs

                            &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp;
                        </span>
                    </div>
                </div>
                <div class="col-lg-3">
                    <div class="text-left">
                        <img src="{{base_url('public/images/screening features/complete privacy.png')}}" style="width: 60px;height: 60px;">
                        <span class="main-text"> Complete privacy &nbsp; &nbsp;</span>
                    </div>
                </div>

            </div>

            <div class="vertical-space-80"></div>
            <h4 class="main-title mb-5">Safety measures used by our technicians&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</h4>
            <div class="row col-lg-12">
                <div class="col-lg-6">
                    <div class="text-left">
                        <img src="{{base_url('public/images/Safety measure/Usage of mask.png')}}" style="width: 60px;height:60px;">
                        <span class="main-text"> Usage of masks, face shields & gloves </span>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="text-left">
                        <img src="{{base_url('public/images/Safety measure/Sanitization.png')}}" style="width: 60px;height: 60px;">
                        <span class="main-text"> Sanitisation of equipments
                            &nbsp; &nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;

                        </span>
                    </div>
                </div>

            </div>

            <div class="vertical-space-50"></div>
            <div class="row col-lg-12 ">
                <div class="col-lg-6">
                    <div class="text-left">
                        <img src="{{base_url('public/images/Safety measure/Maintain 6ft.png')}}" style="width: 60px;height: 60px;">
                        <span class="main-text"> Technician will maintain 6 ft distance </span>
                        &nbsp;
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="text-left">
                        <img src="{{base_url('public/images/Safety measure/temp checks.png')}}" style="width: 60px;height: 60px;">
                        <span class="main-text"> Temperature checks of technicians</span>
                    </div>
                </div>

            </div>

            <p class="vertical-space-30"></p>

            <div class="p-3">
                <p class="vertical-space-30"></p>
                <h3 class="main-title">Things to know about Niramai test&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</h3>

                <p class="vertical-space-30"></p>
                <div class="wrapper1">

                    <div class="row col-lg-12">
                        <div class="col-lg-6">

                            <ol class="ol-cards">
                                <li style="--accent-color: #d7358c">
                                    <div class="icon">
                                        <img src="{{base_url('public/images/Niramai Brochure_38-03.png')}}" style="width: 50px;height: 50px;" class="health-card-logo">
                                    </div>
                                    <div class="main-text my-4" style="text-align: justify;">
                                        Painless, Radiation-Free And Safe For Women Of All Ages
                                    </div>
                                </li>
                            </ol>
                        </div>
                        <div class="col-lg-6">

                            <ol class="ol-cards">
                                <li style="--accent-color: #df6baf">
                                    <div class="icon">
                                        <img src="{{base_url('public/images/Niramai Brochure_38-03.png')}}" style="width: 50px;height: 50px;" class="health-card-logo">
                                    </div>
                                    <div class="main-text my-4" style="text-align: justify;">
                                        Completely privacy-aware i.e. no-see, no-touch method of screening </div>
                                </li>
                            </ol>
                        </div>
                    </div>
                    <div class="vertical-space-30"></div>

                    <div class="row col-lg-12">

                        <div class="col-lg-6">

                            <ol class="ol-cards">
                                <li style="--accent-color: #df6baf">
                                    <div class="icon">
                                        <img src="{{base_url('public/images/Niramai Brochure_38-03.png')}}" style="width: 50px;height: 50px;" class="health-card-logo">
                                    </div>
                                    <div class="main-text my-4" style="text-align: justify;">
                                        No Side Effects & The Screening Can Be Done Multiple Times
                                    </div>
                                </li>
                            </ol>
                        </div>

                        <div class="col-lg-6">

                            <ol class="ol-cards">
                                <li style="--accent-color: #d7358c">
                                    <div class="icon">
                                        <img src="{{base_url('public/images/Niramai Brochure_38-03.png')}}" style="width: 50px;height: 50px;" class="health-card-logo">
                                    </div>
                                    <div class="main-text my-4" style="text-align: justify;">
                                        A thermal device which captures a heat map of the area and does not capture the actual image </div>
                                </li>
                            </ol>

                        </div>
                    </div>
                    <div class="vertical-space-30"></div>

                    <div class="row col-lg-12">
                        <div class="col-lg-6">

                            <ol class="ol-cards">
                                <li style="--accent-color: #df6baf">
                                    <div class="icon">
                                        <img src="{{base_url('public/images/Niramai Brochure_38-03.png')}}" style="width: 50px;height: 50px;" class="health-card-logo">
                                    </div>
                                    <div class="main-text my-4" style="text-align: justify;">
                                        FDA has approved Thermography as an adjunct modality for breast cancer screening </div>
                                </li>
                            </ol>
                        </div>

                        <div class="col-lg-6">

                            <ol class="ol-cards">
                                <li style="--accent-color: #d7358c">
                                    <div class="icon">
                                        <img src="{{base_url('public/images/Niramai Brochure_38-03.png')}}" style="width: 50px;height: 50px;" class="health-card-logo">
                                    </div>
                                    <div class="main-text my-4" style="text-align: justify;">
                                        Thermal devices are CE marked, ISO & MDSAP certified for safety. </div>
                                </li>
                            </ol>
                        </div>


                    </div>
                </div>

                <div class="vertical-space-30"></div>

                <div class="row col-lg-12">
                    <div class="col-lg-2"></div>
                    <div class="col-lg-8">

                        <ol class="ol-cards">
                            <li style="--accent-color: #d7358c">
                                <div class="icon">
                                    <img src="{{base_url('public/images/Niramai Brochure_38-03.png')}}" style="width: 50px;height: 50px;" class="health-card-logo">
                                </div>
                                <div class="main-text my-4" style="text-align: justify;">
                                    Niramai screening and the diagnostic test has regulatory clearance from DCGI (Drug Controller General of India)
                                </div>
                            </li>
                        </ol>
                    </div>
                </div>
                <!-- <p class="main-text" ><span class="common me-2">.</span>
                 Painless, radiation-free and safe for women of all ages</p>
                 <p class="main-text"><span class="common me-2">.</span>
                   Completely privacy-aware i.e. no-see, no-touch method of screening</p>
                   <p class="main-text"><span class="common me-2">.</span>
                   No side effects & the screening can be done multiple times</p>
                   <p class="main-text">
                       <span class="common me-2">.</span>
                       A thermal device which captures a heat map of the area and does
                        not capture the actual image
                   </p>
                   <p class="main-text"><span class="common me-2">.</span>
                   Niramai screening and the diagnostic test has regulatory clearance from DCGI (Drug Controller General of India)</p>
                   <p class="main-text"><span class="common me-2">.</span>Thermal devices are CE marked, ISO & MDSAP certified for safety. </p>
                   <p class="main-text"><span class="common me-2">.</span>
                   FDA has approved Thermography as an adjunct modality for breast cancer screening
                   </p> -->
            </div>
            <div class="p-3" class="guide">
                <p class="vertical-space-30"></p>
                <h3 class="sample">Guidelines to be followed before your appointment&nbsp;</h3>
                <p class="vertical-space-70"></p>
                <div class="wrapper">
                    <div class="square">
                        <div class="circle">
                            <p>
                                <span>01</span>
                            </p>
                        </div>
                        <div class="triagle"></div>
                        <p class="main-text text" style="text-align: left;margin-top:22%">
                            Do Not Do Any Yoga Massage, Or Strenuous Exercise (Physical Therapy) For At Least 3 Hours Before The Examination
                        </p>
                    </div>
                    <div class="square">
                        <div class="circle">
                            <p><span>02</span></p>
                        </div>
                        <div class="triagle"></div>
                        <p class="main-text text" style="text-align: center;margin-top:30%">
                            Avoid Lotions, Creams, Deodorants, Powders, Or Makeup On The Breast Or Underarms On The Day Of The Exam
                        </p>
                    </div>
                    <div class="square">
                        <div class="circle">
                            <p><span>03</span></p>
                        </div>
                        <div class="triagle"></div>
                        <p class="main-text text" style="text-align: center;margin-top:10%">
                            Avoid Physical Stimulation, Mammography, Ultrasound Or Any Treatment Of The Breast, Chest, Neck, Or Back At Least 24 Hours Before The Screening
                        </p>
                    </div>
                    <div class="square">
                        <div class="circle">
                            <p><span>04</span></p>
                        </div>
                        <div class="triagle"></div>
                        <p class="main-text text" style="text-align: center;margin-top:50% !important">
                            Do Not Bathe At Least 1 Hour Prior To The Examination
                        </p>
                    </div>
                    <div class="square">
                        <div class="circle">
                            <p><span>05</span></p>
                        </div>
                        <div class="triagle"></div>
                        <p class="main-text text" style="text-align: center;margin-top:28%">
                            Do Not Consume Coffee Or Any Other Hot Drink Or Food 1 Hour Before The Screening
                        </p>
                    </div>
                    <div class="square">
                        <div class="circle">
                            <p><span>06</span></p>
                        </div>
                        <div class="triagle"></div>
                        <p class="main-text text" style="text-align: center;margin-top:9%">
                            All Prior Prescribed Medications Can Be Continued To Be Taken But A List Of Such Medications And Supplements Should Be Provided To The Technician At The Time Of Screening
                        </p>
                    </div>
                </div>
                <!-- 
                <p class="main-text"><span class="common me-2">.</span>
                  Do not do any yoga massage, or strenuous exercise (physical therapy) for at least 3 hours before the examination
                   </p>     
                   <p class="main-text"><span class="common me-2">.</span>
                   Avoid lotions, creams, deodorants, powders, or makeup on the breast or underarms on the day of the exam</p>
                   <p class="main-text"><span class="common me-2">.</span>
                   Avoid physical stimulation, mammography, ultrasound or any treatment of the breast, chest, neck, or back at least 
                   24 hours before the screening</p>
                   <p class="main-text"><span class="common me-2">.</span>
                   Do not bathe at least 1 hour prior to the examination</p>
                   <p class="main-text"><span class="common me-2">.</span>
                   Do not consume coffee or any other hot drink or food 1 hour before the screening</p>
                   <p class="main-text"><span class="common me-2">.</span>
                   All prior prescribed medications can be continued to be taken but a list of such medications and 
                   supplements should be provided to the technician at the time of screening</p> -->
            </div>

            <!--    
                <p>Book an appointment for ensuring your breast health <a href="{{base_url('book_appointment')}}" class="">Book Appointment</a>

                <p>Niramai Breast Health Screening is a no-touch, no-pain, radiation free, privacy aware solution which works for women of all ages. All women above the age of 18 are eligible for taking the Niramai Breast Health test including symptomatic, pregnant & post-treatment cases.</p> -->


            <!--  <p><a href="{{base_url('book_appointment')}}" class="btn common-btn">Book Appointment <i class="fas fa-long-arrow-alt-right ms-2"></i></a></p> -->
        </div>
    </div>
    <div class="vertical-space-30"></div>
</section>


<section>
    <div class="container">
        <div class="vertical-space-100"></div>
        <h4 class="main-title mb-5">&nbsp;&nbsp;Patient testimonials</h4>
        <div class="owl-carousel" id="advisory-carousel">
            <div class="item">
                <div class="advisory">
                    <div class="row">
                        <!-- <div class="col-lg-4">
                            <p class="text-center mb-0"><img src="{{url('/')}}/public/images/qr/qr.jpg}}" class="council-pic"></p>
                        </div> -->
                        <div class="col-lg-12">
                            <div class="council-desc-block">
                                <div class="council-name"></div>
                                <div class="council-post">
                                </div>
                                <p class="council-desc mt-2" style="text-align:center">
                                    I got this checkup since I have a family history & thought it best to get screened. I wanted to get a mammogram but kept postponing due to COVID. My friend suggested Niramai screening especially because I could take it at home. It was completely hassle-free. They brought the whole equipment themselves and set them up in the corner of my room. They clicked thermal pictures in 5 positions & the whole screening took 10-15 mins. I am happy that I took it up as it was very comfortable & the reports came in 24 hours. I would urge all women to go forward & get this screening done even if you don't have a family history.
                                    Unlike mammography, this process is no-see, no-touch & completely pain free. I happily recommend it to other women!
                                </p>
                            </div>
                        </div>
                    </div>

                </div>

            </div>
            <div class="item">
                <div class="advisory">
                    <div class="row">
                        <!-- <div class="col-lg-4">
                            <p class="text-center mb-0"><img src="{{url('/')}}/public/images/qr/qr.jpg}}" class="council-pic"></p>
                        </div> -->
                        <div class="col-lg-12">
                            <div class="council-desc-block">
                                <div class="council-name"></div>
                                <div class="council-post">
                                </div>
                                <p class="council-desc mt-2" style="text-align:center">
                                    As compared to the conventional techniques, this process was very easy where your body
                                    is cooled & thermal images. It is completely pain free & privacy aware unlike other methods of screening.
                                </p>
                            </div>
                        </div>
                    </div>

                </div>

            </div>
            <div class="item">
                <div class="advisory">
                    <div class="row">
                        <!-- <div class="col-lg-4">
                            <p class="text-center mb-0"><img src="{{url('/')}}/public/images/qr/qr.jpg}}" class="council-pic"></p>
                        </div> -->
                        <div class="col-lg-12">
                            <div class="council-desc-block">
                                <div class="council-name" style="text-align: center;"></div>
                                <div class="council-post">
                                </div>
                                <p class="council-desc mt-2" style="text-align: center;">It was a really nice experience, I would recommend it to all other women as well. </p>
                            </div>
                        </div>
                    </div>

                </div>

            </div>
            <div class="item">
                <div class="advisory">
                    <div class="row">
                        <!-- <div class="col-lg-4">
                            <p class="text-center mb-0"><img src="{{url('/')}}/public/images/qr/qr.jpg}}" class="council-pic"></p>
                        </div> -->
                        <div class="col-lg-12">
                            <div class="council-desc-block">
                                <div class="council-name" style="text-align: center;"></div>
                                <div class="council-post">
                                </div>
                                <p class="council-desc mt-2" style="text-align: center;">It was a good experience.</p>
                            </div>
                        </div>
                    </div>

                </div>

            </div>

        </div>
    </div>

</section>
@endsection

@push('scripts')
<script type="text/javascript">
    $(document).ready(function() {

        $("#advisory-carousel").owlCarousel({
            loop: true,
            margin: 10,
            nav: true,
            autoplay: true,
            autoplayTimeout: 5000,
            autoplayHoverPause: true,
            responsive: {
                0: {
                    items: 1,
                },
                600: {
                    items: 1,
                },
                1000: {
                    items: 1,
                },
            },
        });
    });
    $(".owl-prev").html('<i class="fa fa-chevron-left"></i>');
    $(".owl-next").html('<i class="fa fa-chevron-right"></i>');
</script>
@endpush