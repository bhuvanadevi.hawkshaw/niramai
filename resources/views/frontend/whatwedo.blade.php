@extends('layouts.app')

@section('content')
<style>
    p {
        text-align: justify;
    }
    .list-detail-title{
        color: #d7358c !important;
    }
</style>

<section>
    <div class="what_we_do" id="breasttesthealth">
        <div class="container">
            <div class="vertical-space-40"></div>
            <h4 class="main-title mb-3">&nbsp;&nbsp;Breast Health Test</h4>
            <div class="row">
                <div class="col-lg-1"></div>
                <div class="col-lg-10">
                    <p class="main-text" style="text-align: center;">
                        Niramai’s ‘No Touch’, ‘No Pain’, ‘No See’, ‘No Radiation’ and privacy aware method helps detect breast cancer at a much earlier stage than traditional methods. This screening test works for women of all ages & has no side effects.
                    </p>
                </div>
            </div>
            <div class="row gx-0 mt-3">
                <div class="col-lg-7">
                    <div class="what-we-img">
                        <img src="{{url('/')}}/public/images/breast-health-mob.png" class="do-img1">
                        <img src="{{url('/')}}/public/images/breast-health-d.png" class="do-img">
                    </div>
                </div>
                <div class="col-lg-5">
                    <div class="list-detail mb-5">
                        <h5 class="list-detail-title"> At Hospitals & Clinics</h5>
                        <p class="main-text">Niramai screening is available at your nearest hospital or clinic under the guidance of your doctors.
                            Ask about the Niramai Breast Health screening today!
                        
                        </p>
                        <p>    <a href="{{base_url('contact')}}?name=For Hospitals" class="btn common-btn">Hospital Screening</a>
                        </p>
                    </div>
                    <div class="list-detail mb-5">
                        <h5 class="list-detail-title">Screening Camps</h5>
                        <p class="main-text">

                            Niramai also conducts screening camps for women with little or no access to healthcare facilities. We welcome sponsorships from donors interested in supporting breast cancer screening for under-privileged women.
                        </p>
                       <p> <a href="{{base_url('contact')}}" class="btn common-btn">Get In Touch With Us</a></p>
                    </div>

                </div>
            </div>
           
            <!-- <div class="row col-lg-12" style="padding-top: 20%;">
<div class="col-lg-6">
<div class="list-detail mb-4">
                        <h5 class="list-detail-title">Benefits To the Donor</h5>
                        <p class="main-text">
                            <i class="fa fa-circle" style="font-size:3px;color: #d7358c;"></i>
                     Visible communication to the beneficiaries about the sponsorship during the camp.
                        </p>

                        <p class="main-text">
                            <i class="fa fa-circle" style="font-size:3px;color: #d7358c;"></i>
                        Field report about the impact created after the
                            camp is concluded.
                        </p>
                        <p class="main-text">
                            <i class="fa fa-circle" style="font-size:3px;color: #d7358c;"></i>
                            80G benefits.
                        </p>
                        <p class="main-text">
                            <i class="fa fa-circle" style="font-size:3px;color: #d7358c;"></i>
                            Feel good factor.
                        </p>
                    </div>
                 
</div>
<div class="col-lg-1"></div>
<div class="col-lg-5">
<div class="list-detail mb-2">

<h5 class="list-detail-title">For Women</h5>
<p class="main-text">
    <i class="fa fa-circle" style="font-size:3px;color: #d7358c;"></i>

    Cancer detection at their doorsteps.
</p>
<p class="main-text">
    <i class="fa fa-circle" style="font-size:3px;color: #d7358c;"></i>
    On-the-spot diagnosis and treatment guidance.
</p>
<p class="main-text">
    <i class="fa fa-circle" style="font-size:3px;color: #d7358c;"></i>
    Privacy-aware, non-invasive and safe test.
</p>
<p class="main-text">
    <i class="fa fa-circle" style="font-size:3px;color: #d7358c;"></i>
    Free of cost.
</p>
</div>

</div>
            </div> -->
            <div class="row col-lg-12" style="padding-top: 12%;">
            <div class="list-detail mb-2">
                        <h5 class="list-detail-title">Home Screening</h5>
                        <p class="main-text">

                            we do not compromise on being vigilant about our breast health. High risk women need to be tested for probable cancers routinely. However, since it was very difficult to get to a hospital for a Mammography test, NIRAMAI launched a breast health screening solution that could be conducted at home. Since the Thermalytix Breast Health Test is non-contact and radiation-free, the test is conducted following all COVID precautionary protocols ensuring safety to the woman & the technician during screening.
                        </p>
                    </div>

            </div>
        </div>
    </div>
    <div class="vertical-space-10"></div>

    <p class="text-center"><a href="{{base_url('book_appointment')}}" class="btn common-btn">Book Home Screening</a></p>

</section>

<section>
    <div class="what_we_do" id="fevertest">
        <div class="container">
            <div class="vertical-space-80"></div>
            <h4 class="main-title mb-5">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Covid-19</h4>
            <div class="row">
                <div class="col-lg-6">
                   <div class="list-detail">
                        <h5 class="list-detail-title">Fever Test</h5>
                        <p class="main-text">
                            Community screening is very important to reduce the spread of COVID-19 as new variants come up constantly. Identifying likely infected people in public places helps in controlling the spread. Some of the most common symptoms seen in likely COVID-19 positive patients are fever, dry throat and possible sneezing. NIRAMAI has developed a suite of products to seamlessly identify people with possible symptoms of COVID-19 without interfering with their activities minimizing human error & intervention.
                        <p><a target="_blank" href="{{base_url('fevertest')}}" class="btn common-btn">Read More</a></p>
                    </div>
                    <div class="list-detail">
                        <h5 class="list-detail-title">Xray Setu</h5>
                        <p class="main-text ">
                            NIRAMAI, in collaboration with IISc and clinicians, developed a new Whatsapp-based Chest XRay analysis service to address this issue. This service was pilot tested from June 2020 until April 2020. This FREE service helped over 300 rural doctors to provide better early intervention and treatment to more than 1000 COVID patients.
                        <p><a href="http://xraysetu.com/" class="btn common-btn">Read More</a></p>
                    </div>
                 
                </div>
                <div class="col-lg-6">
                    <p class="text-center"><img src="{{url('/')}}/public/images/covid.png" width="100%"></p>
                </div>
            </div>
        </div>
    </div>
</section>

<section>
    <div class="what_we_do" id="onlineriskassessment">
        <div class="container">
            <div class="vertical-space-40"></div>
            <h4 class="main-title mb-5">Online Risk Assessment</h4>
            <div class="row">
                <div class="col-lg-6">
                    <p class="text-center"><img src="{{url('/')}}/public/images/risk-manage.png" width="100%"></p>
                </div>
                <div class="col-lg-6">
                    <div class="vertical-space-10"></div>
                    <p class="main-text mb-4">
                        NiHA, our very own was launched by Niramai for easy access & support to the customers. Now, you can sign up for a breast health risk assessment for free by sending ‘Hi’ to our Whatsapp number +91-94834 34444
                        <a href="https://api.whatsapp.com/send/?phone=919483434444&text=Hi&app_absent=0">‘’Click here to Get your breast health score’’
                        </a>
                        Or scan the below barcode.
                    </p>
                    <p class="main-text text-center"><img src="{{url('/')}}/public/images/Qr/qr.jpg" style="width:250px" />
                        <br /><br /> Or,
                        <a href="https://hra.niramai.org/">click here</a>
                        to start your risk assessment!
                    </p>
                </div>
            </div>
        </div>
    </div>
</section>









@endsection