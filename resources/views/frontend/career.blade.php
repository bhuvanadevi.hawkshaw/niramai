@extends('layouts.app')

@section('content')
        

    
    <section class="career-page">
        <div class="vertical-space-70"></div>
        <div class="job-search-box">
            <div class="container">
               <div class="row">
                   <div class="col-lg-3">
                    <div class="text-center mb-2">
                        <a href="#" class="search-box">
                          Category
                        </a>
                    </div>    
                   </div>
                    <div class="col-lg-3">
                        <div class="text-center mb-2">
                             <a href="#" class="search-box">
                              Job Type
                            </a>
                        </div>
                   </div>
                    <div class="col-lg-3">
                        <div class="text-center mb-2">
                            <a href="#" class="search-box">
                              Location
                            </a>
                        </div>    
                   </div>
                    <div class="col-lg-3">
                        <div class="text-center mb-2">
                            <a href="#" class="search-btn">
                               <i class="fas fa-search"></i>
                            </a>
                        </div>    
                   </div>
               </div>
            </div>
        </div>
    </section>
   @if($career)
    <section class="career-page">
        <div class="vertical-space-70"></div>
        <div class="container">
             @foreach($career as $row)
            <div class="career-card">
                <div class="vertical-space-5"></div>

                <div class="row">
                   
                 
                    <div class="col-lg-12">
                       <p class="career-card-text">{{$row->career_name}}</p>
                    </div>
                    <div class="row">
                         <div class="col-lg-2">
                           <p class="career-text"><i class="fas fa-business-time"></i>{{$row->career_job_type}}</p>
                        </div>
                         <div class="col-lg-2">
                           <p class="career-text"><i class="fas fa-map-marker-alt"></i>{{$row->career_location}}</p>
                        </div>
                         <div class="col-lg-3">
                           <p class="career-text"><i class="far fa-calendar-minus"></i>{{$row->created_at}}</p>
                        </div>
                    </div>
                </div>
                <p class="main-text">{{$row->career_description}}</p>
                <p class="mt-3"><a href="{{$row->career_content}}" class="btn blog-btn">Read More<i class="fas fa-long-arrow-alt-right ms-2"></i></a></p>
            </div>
            @endforeach
        </div>
            @endif
            
   
     @endsection



