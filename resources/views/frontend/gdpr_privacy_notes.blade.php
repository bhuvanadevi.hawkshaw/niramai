@extends('layouts.app')

@section('content')
<style>
    #customers {
        font-family: Arial, Helvetica, sans-serif;
        border-collapse: collapse;
        width: 100%;
    }

    #customers td,
    #customers th {
        border: 1px solid #ddd;
        text-align: center;
        padding: 8px;
    }

    #customers tr:nth-child(even) {
        background-color: #f2f2f2;
    }

    #customers tr:hover {
        background-color: #ddd;
    }

    #customers th {
        padding-top: 12px;
        padding-bottom: 12px;
        text-align: center;
        background-color: #d7358c;
        color: white;
    }

    h4{
        color: #d7358c;
    }
</style>

<section>
    <div class="women">
        <div class="container">
            <div class="vertical-space-70"></div>
            <h4 class="main-title mb-1">Privacy Notice</h4>
               </div>
        <div class="container">
            <div class="row gx-0">
                <div class="col-lg-12">
                    <p class="main-text mb-5" style="text-align: justify;">
               <br/>     This Privacy Notice outlines Niramai Health Analytix Pvt Ltd’s approach to privacy to fulfil its obligations under applicable privacy laws. 
<br/><br/>
This Privacy Notice is published in compliance with the following privacy laws: European Union General Data Protection Regulation (GDPR), 2018.
<br/><br/>
This Privacy Notice applies to all your <b style="font-weight: 600;">Personal Data</b> processed by us, whether in physical or electronic mode. 
<br/><br/>
Please note that this Privacy Notice is applicable to all instances where we play the role of a <b style="font-weight: 600;">Data Controller</b> of your Personal Data, when we get your Personal Data from you on your visit to our website and when you provide it to us for registration. There may be instances where we play the role of a <b style="font-weight: 600;">Data Processor</b> too, when we process Personal Data on behalf of another organization. In that case, the Privacy Notice of that organization becomes applicable to your Personal Data. 
<br/><br/>
Niramai Health Analytix Pvt Ltd is committed to keeping your Personal Data private. We process any Personal Data we collect from you in accordance with the applicable laws and regulations mentioned and the provisions of this Privacy Notice. Please read the following carefully to understand our views and practices regarding your Personal Data and how we treat it. 
<br/><br/>
Throughout this document, the terms “we”, “us”, “our” & “ours” refer to Niramai Health Analytix Pvt Ltd. And the terms “you”, “your” & “yours” refer to YOU (as the Data Subject).
                    </p>               
</div>
            </div>
        </div>

        <div class="container">
            <h4 class="mb-1  text-dark text-left"style="font-size:18px"><b>WHAT PERSONAL DATA DO WE COLLECT & PROCESS?</b></h4>
            <div class="row gx-0">
                <div class="col-lg-12"><br/>
                <p class="main-text">Categories of Personal Data that we collect and process are as follows: – </p><br/>
<ul class="main-text mb-5" style="text-align: justify;">
<li>Demographic & Identity Data (for e.g., name, email address, contact number)</li>
<li>Communication Data (for e.g., message sent, details about queries)</li>
<li>Financial Data (for e.g., Payment method, payment details, transaction details)</li> 
<li>Online Identifiers and other Technical Data (for e.g., IP address, Cookie details)</li>
<li>Opinions, Preferences, Interests Data (for e.g., customer Feedback, survey responses)</li>
<li>Special Category of Data (for e.g., Health Information, Medical History)</li>

</ul>
                </div>
            </div>
        </div>

        <div class="container">
        <h4 class="mb-1 text-dark text-left"style="font-size:18px"><b>WHERE DO WE OBTAIN YOUR PERSONAL DATA FROM? </b></h4>
              <div class="row gx-0">
                <div class="col-lg-12">
                <br/>    <p class="main-text" style="text-align: justify;">
                    Most of the Personal Data we process is provided by you directly to us when you use our products and/or services. This also includes the Personal Data collected automatically and indirectly by us when you use our website and application(s). 
<br/>

We may also receive Personal Data about you from third parties and publicly available sources of information.
                    </p>
                    <br/>

                </div>
              </div>
              <h4 class="mb-1 text-dark text-left"style="font-size:18px"><b>HOW DO WE USE YOUR PERSONAL DATA?</b></h4>
              <div class="row gx-0">
                <div class="col-lg-12">
                <br/>    <p class="main-text" style="text-align: justify;">
                We use your Personal Data for the following purposes:
            <ol class="main-text">
                    <li> To verify your identity and to maintain the personal data record</li>

<li> To deliver our products and services </li>

<li> To communicate with you regarding existing products and services availed by you, including notifications of any alerts or updates </li>

<li> To evaluate, develop and improve our products and services </li>

<li> For market and product analysis and market research </li>

<li> To send you information about our other products or services which may be of interest to you</li> 

<li> To handle enquiries and complaints </li>

<li> To comply with legal or regulatory requirements </li>

<li> To investigate, prevent, or take action regarding illegal activities, suspected fraud and situations involving potential threats to the safety of any person
</li>      </ol>
              </p>
                    <br/>

                </div>
              </div>
              <div class="row gx-0">
                <div class="col-lg-12">
                <h4 class="mb-1 text-dark text-left"style="font-size:18px"><b>     LAWFUL BASES OF PROCESSING YOUR PERSONAL DATA
           </b></h4>
          
                <br/>    <p class="main-text" style="text-align: justify;">
                We process your Personal Data by relying on one or more of the following lawful bases: 
         
                <ol class="main-text">
<li>You have explicitly agreed to/consented to us processing your Personal Data for a specific reason </li>

<li>The processing is necessary for the performance of the contract we have with you or to take steps to enter into a contract with you </li>

<li>The processing is necessary for compliance with a legal obligation we have </li>

<li>The processing is necessary for the purposes of a legitimate interest pursued by us </li>

            </ol>
            </p>
         <p class="main-text">   Where the processing is based on your consent, you have the right to withdraw your consent at any point in time. Please note that should the withdrawal of consent result in us not being able to continue offering our products and services to you, we reserve the right to withdraw or cease our products and services to you upon your withdrawal. You may withdraw consent by contacting us with a written request to the contact details specified below in the ‘Contact Us’ section. Upon receipt of your request to withdraw your consent, the consequences of withdrawal will be communicated to you. Upon your agreement to the same, your request for withdrawal will be processed.
         </p>
            </div>
              </div>
              <br/><br/>
              <div class="row gx-0">
                <div class="col-lg-12">
                <h4 class="mb-1 text-dark text-left"style="font-size:18px"><b>WHEN DO WE SHARE YOUR PERSONAL DATA WITH THIRD PARTIES?
           </b></h4>
          
                <br/>    <p class="main-text" style="text-align: justify;">
                We may use third parties in the provision of our products and services to you. We may share your Personal Data with such third parties. We may share Anonymous Data with Third Parties for providing our Services. We have appropriate contracts in place with all such third parties. This means that they are not permitted to do anything with your Personal Data which is outside of the scope specified by us. They are committed to hold your Personal Data securely and retain it only for the period specified in our contracts with them.
         </p></div></div>
                
         <div class="row gx-0">
                <div class="col-lg-12">
                <h4 class="mb-1 text-dark text-left"style="font-size:18px"><b>                1) Reasons for sharing your Personal Data with third parties: 
           </b></h4>
          
                <br/> <br/>   <p class="main-text" style="text-align: justify;">


We may disclose your Personal Data to third parties only where it is lawful to do so. This includes instances where we or they: 

<ol class="main-text">
<li>need to provide you with products or services </li>
<li>have asked you for your consent to share it, and you have agreed</li> 
<li>have a legitimate business reason for doing so </li>
<li>have a legal obligation to do so. For e.g., to assist with detecting and preventing fraud </li>
<li>have a requirement in connection with regulatory reporting, litigation or asserting or defending legal rights and interests </li>

</ol>
</p>
<p class="main-text" style="text-align: justify;">

We may also disclose your Personal Data to appropriate authorities if we believe that it is reasonably necessary to comply with a law, regulation, legal process; protect the safety of any person; address fraud, security, or technical issues; or protect our rights or the rights of those who use our products & services. 
</p>
                </div>
         </div>
                </br> <br/>  
         <div class="row gx-0">
                <div class="col-lg-12">
                <h4 class="mb-1 text-dark text-left"style="font-size:18px"><b> 
                2) With whom your Personal Data may be shared:
           </b></h4>
          
                <br/> <p class="main-text" style="text-align: justify;">
                We may disclose your Personal Data to the following third parties:
<ol class="main-text">
<li>any sub-contractors, agents or service providers who work for us or provide services or products to us </li>
<li>law enforcement authorities, government authorities, courts, dispute resolution bodies, regulators, auditors and any party appointed or requested by applicable regulators to carry out investigations or audits of our activities </li>
<li>statutory and regulatory bodies, authorities (including the government) investigating agencies and entities or persons, to whom or before whom it is mandatory to disclose Personal Data as per the applicable law, courts, judicial and quasi-judicial authorities and tribunals, arbitrators and arbitration tribunals</li>


</ol>
</p>
<br/>
<h4 class="mb-1 text-dark text-left"style="font-size:18px"><b> 
CROSS-BORDER DATA TRANSFER 
    </b></h4>
    <br/>
 <p class="main-text">          
Personal Data we hold about you may be transferred to other countries outside your residential country for any of the purposes described in this Privacy Notice. 
<br/>
Please note  that these countries may have differing (and potentially less stringent) privacy laws and that Personal Data can become subject to the laws and disclosure requirements of such countries, including disclosure to governmental bodies, regulatory agencies and private persons, as a result of applicable governmental or regulatory inquiry, court order or other similar process.
<br/>
 </p>
 <br/>
<h4 class="mb-1 text-dark text-left"style="font-size:18px"><b> 
USE OF COOKIES AND OTHER TRACKING MECHANISMS
    </b></h4>
   <br/>
   <p class="main-text">
We may use cookies and other tracking mechanisms on our website and other digital properties to collect data about you.
<br/>
Cookies are small text files that are placed on your computer by websites that you visit. They are widely used in order to make websites work, or work more efficiently, as well as to provide information about your actions to the owners of the website. 
<br/>
Most web browsers allow you some control of cookies through browser settings. 
<br/>
Outlined below are the categories of cookies along with a description of what they are used for. 
 </p><br/>
 <ul class="main-text">
<li>Strictly Necessary Cookies – These cookies are needed to run our website, to keep it secure and to comply with regulations that apply to us. </li>
<li>Functional Cookies – We may use functional cookies on our website. These cookies allow us to remember information you enter or choices you make (such as your username, language, or your region) and provide you with enhanced, more personalized features. 
<li>Performance/Analytics Cookies – We may use performance/analytics cookies on our website. These cookies collect information about how visitors use our website and services, including which pages visitors go to most often and if they receive error messages from certain pages. It is used to improve how our website functions and performs. </li>
<li>Marketing Cookies – We may use marketing cookies on our website. These cookies help us decide which of our products, services and offers may be relevant for you. We may use this data to tailor the marketing and ads you see on our own and other website and mobile apps, including social media. For instance, you may see our ads on other sites after you have been to our website. </li>
<li>We may also use trackers (such as web beacons, tags, pixels) on our website and other digital properties to collect data about you.</li>
 </ul>
 <br/>
 <h4 class="mb-1 text-dark text-left"style="font-size:18px"><b> 
 HOW DO WE SECURE YOUR PERSONAL DATA?
    </b></h4>
    <br/>
<p class="main-text">
We are committed to protecting your Personal Data in our custody. We take reasonable steps to ensure appropriate physical, technical and managerial safeguards are in place to protect your Personal Data from unauthorized access, alteration, transmission and deletion. We ensure that the third parties who provide services to us under appropriate contracts take appropriate security measures to protect your Personal Data in line with our policies.
</p>
<br/>
<h4 class="mb-1 text-dark text-left"style="font-size:18px"><b> 
HOW LONG DO WE KEEP YOUR PERSONAL DATA?
    </b></h4>
    <br/>
    <p class="main-text">
We keep the Personal Data we collect about you for as long as it is required for the purposes set out in this Privacy Notice and for legal or regulatory reasons. We take reasonable steps to delete or permanently de-identify your Personal Data that is no longer needed.
    </p>
<br/>
<h4 class="mb-1 text-dark text-left"style="font-size:18px"><b> 
CHILDREN’S PRIVACY
    </b></h4>
<br/>
<p class="main-text">
We do not knowingly collect Personal Data from children. If you are a parent or guardian and aware that your Child has provided us with Personal Data, please contact us using the details in the ‘Contact us’ section of this notice.
</p>
<br/>
<h4 class="mb-1 text-dark text-left"style="font-size:18px"><b> 
LINKS TO OTHER WEBSITES
    </b></h4>
<br/>
    <p class="main-text">
Our website may contain links to websites of other organizations. This privacy notice does not cover how those organizations process your Personal Data. We encourage you to read the privacy notices on the other websites you visit.
    </p>
  <br/>
    <h4 class="mb-1 text-dark text-left"style="font-size:18px"><b> 
    CONTACT US
    </b></h4>
<br/>
<p class="main-text">
For any further queries and complaints related to privacy, or exercising your rights, you could reach us at: 
<br/>
Contact Email Address: privacy@niramai.com
</p>
<br/>
<h4 class="mb-1 text-dark text-left"style="font-size:18px"><b> 
NOTIFICATION OF CHANGES
    </b></h4>
    <br/>
<p class="main-text">
We regularly review and update our Privacy Notice to ensure it is up-to-date and accurate. Any changes we may make to this Privacy Notice in future will be posted on this page.
</p>
<br/>
<h4 class="mb-1 text-dark text-left"style="font-size:18px"><b> 
YOUR PRIVACY RIGHTS
    </b></h4><br/>
<p class="main-text">
If you are an EU resident, under the GDPR, you have the following rights and we commit to provide you with the same: 
</p>
<ul class="main-text">
<li>Right of Access: You have the right to get access to your Personal Data that is with us along with other supporting information. </li>
<li>Right to Rectification: You have the right to ask us to rectify your Personal Data that is with us that you think is inaccurate. You also have the right to ask us to complete your Personal Data that you think is incomplete. </li>
<li>Right to Erasure: You have the right to ask us to erase your Personal Data that is with us under certain circumstances. </li>
<li>Right to Restriction of Processing: You have the right to ask us to restrict the processing of your Personal Data under certain circumstances. </li>
<li>Right to Data Portability: You have the right to ask that we transfer the Personal Data you gave us to another organization, or to you, under certain circumstances.</li>
<li>Right to Object: You have the right to object to the processing of your Personal Data under certain circumstances. </li>
<li>Right to not be subjected to Automated individual decision-making, including profiling </li>
<li>Right to lodge a complaint with the Supervisory Authority: You have the right to lodge a complaint with a supervisory authority. </li>
</ul>
<p class="main-text">
If you wish to make a request to exercise any of your rights, you can contact us using the details in the ‘Contact us’ section of this notice.
</p>
</div>
         </div>
        </div>
    </div>
</section>
@endsection