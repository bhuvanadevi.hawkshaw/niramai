@extends('layouts.app')

@section('content')
<style>
    #customers {
        font-family: Arial, Helvetica, sans-serif;
        border-collapse: collapse;
        width: 100%;
    }

    #customers td,
    #customers th {
        border: 1px solid #ddd;
        text-align: center;
        padding: 8px;
    }

    #customers tr:nth-child(even) {
        background-color: #f2f2f2;
    }

    #customers tr:hover {
        background-color: #ddd;
    }

    #customers th {
        padding-top: 12px;
        padding-bottom: 12px;
        text-align: center;
        background-color: #d7358c;
        color: white;
    }
    h4{
        color: #d7358c;
    }
</style>

<section>
    <div class="women">
        <div class="container">
            <div class="vertical-space-70"></div>
            <h4 class="main-title mb-1">&nbsp;Observational Study</h4>
            <div class="row gx-0">
                <div class="col-lg-12">
                    <p class="main-text mb-5 text-center">
                    Observational Study to Evaluate the Clinical Efficacy of Thermalytix for Detecting Breast Cancer in Symptomatic and Asymptomatic Women
                  </div>
            </div>
        </div>
        <div class="container">
            <h4 class="mb-1">Objective</h4>
            <div class="row gx-0">
                <div class="col-lg-12">
                    <p class="main-text mb-5" style="text-align: justify;">
                        <i class="fa fa-circle" style="font-size:8px;color: #d7358c;"></i>
                        To evaluate the efficacy of Thermalytix as a breast cancer imaging modality in comparison to standard of care.
                     </p>
                </div>
            </div>
        </div>

        <div class="container">
            <h4 class="mb-1">Study Sites</h4>
            <div class="row gx-0">
                <div class="col-lg-12">
                    <p class="main-text mb-5" style="text-align: justify;">
                        <i class="fa fa-circle" style="font-size:8px;color: #d7358c;"></i>
                        Multisite study at two clinical institutions – Health Care Global (HCG) hospital and Central Diagnostics Research Foundation (CDRF) in Bangalore, India
                     </p>
                </div>
            </div>
        </div>

        <div class="container">
            <h4 class="mb-1">Data and Method</h4>
            <div class="row gx-0">
                <div class="col-lg-12">
                    <p class="main-text" style="text-align: justify;">
                        <i class="fa fa-circle" style="font-size:8px;color: #d7358c;"></i>
                        This multisite observational study included 470 symptomatic and asymptomatic women who presented themselves for a breast health checkup in two centers between 6th May 2016 and 2nd February 2019. Among them, 238 women had symptoms such as breast lump, nipple discharge or breast pain and the rest were asymptomatic. All participants went through the Thermalytix test and one or more standard-of-care tests for breast cancer screening as recommended by the radiologists.
                     </p>
                    <p class="main-text mb-5" style="text-align: justify;">
                        <i class="fa fa-circle" style="font-size:8px;color: #d7358c;"></i>
                        Results from Thermalytix and standard modalities were obtained independently in a blinded fashion for comparison. The final diagnosis used for analysis (normal or malignant) was the final impression of an expert clinician based on the symptoms and the available reports of standard modalities (mammography, ultrasonography, elastography, biopsy, FNAC, etc.) as per clinical practice.
                      </p>
                     </div>
            </div>
        </div>

        <div class="container">
            <h4 class="mb-1">Results</h4>
            <div class="row gx-0">
                <div class="col-lg-12">
                    <p class="main-text" style="text-align: justify;">
                        <i class="fa fa-circle" style="font-size:8px;color: #d7358c;"></i>
                        On the 470 women, Thermalytix resulted in a sensitivity of 91.02% (95% CI, 81.8% to 96%) and specificity of 82.39% (95% CI, 78.2% to 86%).in detection of breast malignancy. The sensitivity in symptomatic women was 89.85% and in asymptomatic women was 100%. The specificity of Thermalytix in symptomatic women was 69.04% while asymptomatic women had specificity of 92.41%. Thermalytix showed an overall area under curve (AUC) of 0.90 with an AUC of 0.82 for symptomatic and 0.98 for asymptomatic women. Mammography was performed only on 242 women. In these 242 women, Thermalytix demonstrated better sensitivity than mammography.
                     </p>
                   
                     <img src="{{url('/')}}/public/images/banner_1.jpg"  class="mt-5 rounded mx-auto d-block partner-img img-fluid" alt="...">
                     <p class="main-text mt-5 mb-3" style="text-align: center;">
                    <span style="color: #d7358c;font-size:14px"> Figure 4: </span> Performance of Thermalytix in a) overall study population, symptomatic and asymptomatic population, and b) across age groups

                
                    </div>
            </div>
        </div>

      
        <div class="container">
            <h4 class="mt-5">Conclusion</h4>
            <div class="row gx-0">
                <div class="col-lg-12">
                    <p class="main-text mb-5" style="text-align: justify;">
                        <i class="fa fa-circle" style="font-size:8px;color: #d7358c;"></i>
                        High sensitivity and high AUC of Thermalytix in women of all age groups demonstrates the efficacy of the tool for breast cancer screening. Thermalytix, with its automated scoring and image annotations of potential malignancies and vascularity, can assist the clinician in better decision making and improve the quality of care in an affordable and radiation-free manner. Thus, we believe Thermalytix is poised to be a promising modality for breast cancer screening.
                         </p>

                </div>
            </div>
        </div>

        <div class="container">
            <h4 class="mb-1">Publication</h4>
            <div class="row gx-0">
                <div class="col-lg-12">
                    <p class="main-text mb-5" style="text-align: justify;">
                        <i class="fa fa-circle" style="font-size:8px;color: #d7358c;"></i>
                        Kakileti ST, Madhu HJ, Krishnan L, Manjunath G, Sampangi S, Ramprakash HV. Observational Study to Evaluate the Clinical Efficacy of Thermalytix for Detecting Breast Cancer in Symptomatic and Asymptomatic Women. JCO Glob Oncol. 2020;6:1472-1480. doi:10.1200/GO.20.00168 https://pubmed.ncbi.nlm.nih.gov/33001739/ 
                         </p>

                </div>
            </div>
        </div>


    </div>
</section>
@endsection