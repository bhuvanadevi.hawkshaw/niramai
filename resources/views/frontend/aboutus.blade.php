@extends('layouts.app')

@section('content')
  <section>
      <div class="about-us">
          <div class="container">
              <div class="vertical-space-70"></div>
              <h4 class="main-title mb-5">&nbsp;&nbsp;Our Story</h4>
              <div class="row">
                  <div class="col-lg-6">
                      <div class="vertical-space-70"></div>
                      <img class="rounded mx-auto d-block partner-img img-fluid" src="{{url('/public/images/doctor/Group image Niramai.jpg')}}" width="95%">
                  </div>
                  <div class="col-lg-6">
                      <p class="main-text mt-2" style="text-align: justify;">
                          NIRAMAI was co-founded in July 2016 by Dr. Geetha Manjunath and Ms. Nidhi Mathur with a vision to create an affordable & accessible method of breast cancer detection which would work for women of all ages. NIRAMAI’s solution has led to more than 25 Indian & International patents and the novel algorithms have also been peer-reviewed in international scientific conferences.
                      </p>

                      <p class="main-text mt-3" style="text-align: justify;">
                          Niramai is the only Indian company to be listed on the 2019 cohort of AI 100 startups in the world by global business data intelligence platform by CB Insights. Niramai is accelerated by Google launchpad and has received support by the Bill & Melinda Gates Foundation to leverage technology to detect river blindness. We have so far raised a total of $7 from investors including Dream Incubator, Beenext pi Ventures, Ankur Capital, Axilor Ventures, 500 Startups, and Flipkart co-founder Binny Bansal. </p>
                  </div>
              </div>
          </div>
          <div class="vertical-space-80"></div>
      </div>
  </section>

  <style type="text/css">
      .card p {
          margin-bottom: 5px;
      }
      .journey-card {
          font-family: 'Poppins', sans-serif !important;
          padding: 10px;
      }
      .main-title::after {
    content: "";
    display: block;
    background-color: #d7355c;
    width: 70px !important;
    height: 4px;
    position: absolute;
    border-radius: 25px;
    content: "";
    position: absolute;
    /* margin: 0 0%; */
    top: -20px;
}
.dep-btn{
    border: 2px solid  #ccc;
    border-radius: 20px;
    padding: 7px 15px;
    color: #000;
    text-decoration: none;
    font-size: 13px;
    margin:5px !important;
  }
      .journey-card .img-box {
          background: #fff;
          /* box-shadow: rgb(100 100 111 / 20%) 0px 7px 29px 0px; */
          padding: 10%;
          display: flex;
          justify-content: center;
          align-items: center;
          width: 100px !important;
          height: 100px !important;
          border-radius: 10px;
          margin-bottom: 2rem;
          box-shadow: 10px 10px 50px 3px rgb(39 92 141 / 10%);
          padding: 0;
          position: relative;
      }

      .journey-card .img-box img {
          width: 70px;
      }

      .journey-card::before {
          content: '';
          width: 100%;
          height: 2px;
          background: #ff7869;
          display: inline-block !important;
          left: 60px;
          position: absolute;
          top: 40px;
      }

      .journey-card-white:before {
          background: #FFF;
      }

      .journey-card h3 {
          font-size: 20px;
      }

      .journey-card ul {
          padding: 0;
          margin: 0;
          list-style: none;
      }

      .journey-card li {
          font-size: 12px;
      }

      .ourjourney .owl-nav {
          position: absolute !important;
          right: 0;
      }

      .ourjourney .owl-nav button.owl-next,
      .ourjourney .owl-nav button.owl-prev {
          background: #d7358c !important;
          padding: 10px !important;
          margin-right: 3px;
          color: #FFF;
          border-radius: 3px;
      }

      .ourjourney .owl-nav button.owl-next.disabled,
      .ourjourney .owl-nav button.owl-prev.disabled {
          background: #f9c0df !important;

      }
      .members {
    border: 2px solid #ccc;
    border-radius: 20px;
    padding: 7px 15px;
    color: #000;
    text-decoration: none;
    font-size: 13px;
    margin: 5px !important;
    background-color: #d7358c;
      }
  </style>
  <section>
      <div class="container">
          <h4 class="main-title mb-5">&nbsp;&nbsp;&nbsp;Our Journey</h4>
          <div class="line"></div>
          <div class="row">
              <div class="col-lg-12">
                  <div class="ourjourney owl-carousel" id="ourjourney">

                      <div class="item">
                          <div class="journey-card">
                              <div class="img-box">
                                  <img src="{{url('public/images/journey/2016.png')}}" width="50">
                              </div>
                              <h3>2016</h3>
                              <ul>
                                  <li>Founded by Dr. Geetha Manjunath & Ms. Nidhi Mathur</li>
                              </ul>
                          </div>
                      </div>

                      <div class="item">
                          <div class="journey-card">
                              <div class="img-box">
                                  <img src="{{url('public/images/journey/2017.png')}}" width="50">
                              </div>
                              <h3>2017</h3>
                              <ul>
                                  <li>Seed Funding, First deployment of Niramai solution in a hospital, Selected among top 100 tech startups in Elevate 100, Aegis graham bell winner in Data science

                                  </li>                                </ul>
                          </div>
                      </div>

                      <div class="item">
                          <div class="journey-card">
                              <div class="img-box">
                                  <img src="{{url('public/images/journey/2018.png')}}" width="50">
                              </div>
                              <h3>2018</h3>
                              <ul>
                                  <li>30 deployments, 10,000 screening, Niramai among 4 startups selected Google launchpad, Aegis graham bell winner in mobile app consumer
                                  </li>
                                 </ul>
                          </div>
                      </div>


                      <div class="item">
                          <div class="journey-card">
                              <div class="img-box">
                                  <img src="{{url('public/images/journey/2019.png')}}" width="50">
                              </div>
                              <h3>2019</h3>
                              <ul>
                                  <li> 25,000 screenings, 100+ deployments, Only indian company in CB insights
                                  </li>
                              </ul>
                                                        </div>
                      </div>

                      <div class="item">
                          <div class="journey-card">
                              <div class="img-box">
                                  <img src="{{url('public/images/journey/2020.png')}}" width="50">
                              </div>
                              <h3>2020 </h3>
                              <ul>
                                  <li>Launch of breast screening at home, Launch of Fever Test for COVID-19 detection, Launch of breast cancer risk assessment service, CE and ISO approval
                                  </li>
                              </ul>
                          </div>
                      </div>


                      <div class="item">
                          <div class="journey-card journey-card-white">
                              <div class="img-box">
                                  <img src="{{url('public/images/journey/2021.png')}}" width="50">
                              </div>
                              <h3>2021</h3>
                              <ul>
                                  <li>
                              Successful mega camp with 500 screenings in a single day, Launch of NiHA - Niramai Health Assistant, 25 patent grants for Niramai
                                  </li>
                              </ul>
                             </div>
                      </div>

                    </div>
              </div>
          </div>
      </div>
      </div>
      <div class="vertical-space-120"></div>
  </section>


  <section>
      <div class="container">
          <h4 class="main-title mb-5">&nbsp;&nbsp;Our Highlights</h4>
          <div class="line"></div>
          <div class="row col-lg-12">
          <div class="col-lg-2">
          </div>
             <div class="col-lg-2">
                  <div class="our-high">
                      <img src="{{url('public/images/highlight/New deployments.jpg')}}" width="120">
                      <h3>100+ Deployments in 15+ Cities</h3>
                  </div>
              </div>
              <div class="col-lg-2">

                  <div class="our-high">
                      <img src="{{url('public/images/highlight/Screenings.jpg')}}" width="120">
                      <h3>200+ Home Screenings</h3>
                  </div>
              </div>
              <div class="col-lg-2">

                  <div class="our-high">
                      <img src="{{url('public/images/highlight/Clearances.jpg')}}" width="120">
                      <h3>Regulatory clearances : CE, ISO, MDSAP</h3>
                  </div>
              </div>
              <div class="col-lg-2">

                  <div class="our-high">
                      <img src="{{url('public/images/highlight/Patented.jpg')}}" width="120">
                      <h3>25+ Indian and International patents</h3>
                  </div>
              </div>
              <!-- <div class="col-lg-2">

                  <div class="our-high">
                      <img src="{{url('public/images/highlight/New services.jpg')}}" width="120">
                      <h3>Launch of new Service : Home screening, fever test, Risk assesment</h3>
                  </div>
              </div> -->
          </div>
          <!--  <div class="highlits-links">
                    <a href="#">60+ Deployments in 15+ Cities</a>
                    <a href="#">37000+ Screenings</a>
                    <a href="#">Regulatory clearances : CE, ISO, MDSAP</a>
                    <a href="#">24+ Indian and International patents</a>
                    <a href="#"
                      >Launch of new Service : Home screening, fever test, Risk assesment</a
                    >
                  </div> -->
          <div class="vertical-space-50"></div>
      </div>
  </section>

  <section class="bac-color">
      <div class="container">
          <div class="vertical-space-50"></div>
          <h4 class="main-title mb-3">&nbsp;&nbsp;Our Team</h4>
          <p class="main-text text-center">Meet our Amazing Team at Niramai</p>
          <div class="vertical-space-30"></div>
          <div class="row col-lg-12 justify-content-center gx-0">
              <!-- <div class="col">
                  <a href="#technology" id="atechnology" class="dep-btn">Technology</a>
              </div> -->
              <div class="col">
                  <a href="#Leadership" id="aleadership" class="dep-btn">Leadership</a>
              </div>
               <div class="col">
                  <a href="#clinicalaffairs" id="aclinicalaffairs" class="dep-btn">Clinical&nbsp;Affairs</a>
              </div>
          <div class="col">
                  <a href="#enterprisedivision" id="aenterprisedivision" class="dep-btn">Enterprise&nbsp;Division</a>
              </div>
              <div class="col">
                  <a href="#businessinitiatives" id="abusinessinitiatives" class="dep-btn">New&nbsp;Business&nbsp;Initiatives</a>
              </div>
              <!-- <div class="col">
                  <a href="#internationalbusiness" id="ainternationalbusiness" class="dep-btn">International&nbsp;Business</a>
              </div> -->
             <!-- <div class="col-lg-1">
                  <a href="#Products" id="aproducts" class="dep-btn">Products</a>
              </div> -->
              <!-- <div class="col-lg-2">
                  <a href="#engineering" id="aengineering" class="dep-btn">Engineering</a>
              </div> -->
               <div class="col">
                  <a href="#screeningoperations" id="ascreeningoperations" class="dep-btn">Screening&nbsp;Operations</a>
              </div>
              <div class="col">
                  <a href="#Keymembers" id="akeymembers" class="dep-btn">Key&nbsp;Members</a>
              </div>
              <div class="col">
                  <a href="#Datascience" id="adatascience" class="dep-btn">Data&nbsp;Science</a>
              </div>
              <div class="col">
                  <a href="#Finance" id="afinance" class="dep-btn">Finance</a>
              </div>
              
              <div class="col">
                  <a href="#products" id="aproducts" class="dep-btn">Products</a>
              </div>
          <!-- <div class="col">
                  <a href="#HRAdmin" id="ahradmin" class="dep-btn">HR&nbsp;&&nbsp;Admin</a>
              </div> -->
             </div>

              <div class="vertical-space-5"></div>
              <div class="row col-lg-4 justify-content-center gx-0">
              <div class="col">
                  <a href="#engineering" id="aengineering" class="dep-btn">Engineering</a>
              </div>
              <div class="col">
                  <a href="#QARA" id="aqara" class="dep-btn">QA&nbsp;&&nbsp;RA</a>
              </div>
              <div class="col">
              </div>
              <div class="col">
              </div>
            
            </div>
              <div class="vertical-space-50"></div>
          <div class="team-box" id="technology">
              <div class="container">
                  <div class="row gx-1">
                  <div class="col-lg-3"></div>
                      <div class="col-lg-6">
                          <div class="card team-card">
                              <div class="card-body">
                                  <h5 class="card-title">No Members Found</h5>
                              </div>
                          </div>
                      </div>
                  </div>
              </div>
          </div>
          <div class="team-box" id="businessinitiatives">
          <div class="container">
          <div class="row gx-1">
          <div class="col-lg-2"></div>
                   <div class="col-lg-4">
                          <div class="card team-card">
                              <img src="{{url('/')}}/public/images/team/Somdev Upadhyay.jpg" class="card-img-top" alt="...">
                              <div class="card-body">
                                  <h5 class="card-title">Somdev Upadhyay</h5>
                                   <!-- <p class="card-text">Lead- Corporate Sales & BD</p>  -->
                                  <p class="text-center"><a href="https://www.linkedin.com/in/somdev-upadhyay-90620013/" class="btn btn-primary"><i class="fab fa-linkedin-in"></i> </a></p>
                              </div>
                          </div>
                      </div> 
                       <div class="col-lg-4">
                          <div class="card team-card">
                              <img src="{{url('/')}}/public/images/team/thumbnail.png" class="card-img-top" alt="...">
                              <div class="card-body">
                                  <h5 class="card-title">Dr. Nandita Bhargava</h5>
                                  <!-- <p class="card-text">Program  Manager</p> -->
                                  <p class="text-center"><a href="https://www.linkedin.com/in/dr-nandita-bhargava-3a8b08a2/" class="btn btn-primary"><i class="fab fa-linkedin-in"></i> </a></p>
                              </div>
                          </div>
                      </div>
                       </div>
              </div>
          
          </div>
          <div class="team-box" id="internationalbusiness">
              <div class="container">
                  <div class="row gx-1">
                  <div class="col-lg-3"></div>
                      <div class="col-lg-6">
                          <div class="card team-card">
                              <div class="card-body">
                                  <h5 class="card-title">No Members Found</h5>
                              </div>
                          </div>
                      </div>
                  </div>
              </div>
          </div>
          <div class="team-box" id="HRAdmin">
              <div class="container">
                  <div class="row gx-1">
                  <div class="col-lg-3"></div>
                      <div class="col-lg-6">
                          <div class="card team-card">
                              <div class="card-body">
                                  <h5 class="card-title">No Members Found</h5>
                              </div>
                          </div>
                      </div>
                  </div>
              </div>
          </div>
          <div class="team-box" id="clinicalaffairs">
              <div class="container">
                  <div class="row gx-1">
                  <div class="col-lg-4">
                          <div class="card team-card">
                              <img src="{{url('/')}}/public/images/team/Dr Charitha G.jpg" class="card-img-top" alt="...">
                              <div class="card-body">
                                  <h5 class="card-title">Dr Charitha G</h5>
                                  <!-- <p class="card-text">Clinical Research Scientist</p> -->
                                  <p class="text-center"><a href="https://www.linkedin.com/in/charitha-gangadharan-a88b9191/?originalSubdomain=in" class="btn btn-primary"><i class="fab fa-linkedin-in"></i> </a></p>
                              </div>
                          </div>
                      </div>  
                      <div class="col-lg-4">
                          <div class="card team-card">
                              <img src="{{url('/')}}/public/images/team/Dr. Gargi Deshpande.jpg" class="card-img-top" alt="...">
                              <div class="card-body">
                                  <h5 class="card-title">Dr. Gargi Deshpande</h5>
                                  <!-- <p class="card-text">Clinical Research Associate</p> -->
                                  <p class="text-center"><a href="www.linkedin.com/in/gargingp" class="btn btn-primary"><i class="fab fa-linkedin-in"></i> </a></p>
                              </div>
                          </div>
                      </div>
                      <div class="col-lg-4">
                          <div class="card team-card">
                              <img src="{{url('/')}}/public/images/team/Dr Chandan Kumar.jpg" class="card-img-top" alt="...">
                              <div class="card-body">
                                  <h5 class="card-title">Dr Chandan Kumar</h5>
                                  <!-- <p class="card-text">Medical Consultant </p> -->
                                  <p class="text-center"><a href="www.linkedin.com/in/chandan-kumar-aiims" class="btn btn-primary"><i class="fab fa-linkedin-in"></i> </a></p>
                              </div>
                          </div>
                      </div>
                   <!-- <div class="col-lg-2"></div>
                    
                   <div class="col-lg-4">
                          <div class="card team-card">
                              <img src="{{url('/')}}/public/images/team/Dr. Tanveen Kaur.jpg" class="card-img-top" alt="...">
                              <div class="card-body">
                                  <h5 class="card-title">Dr. Tanveen Kaur</h5>
                                   <p class="card-text">Clinical Marketing Researcher</p> 
                                  <p class="text-center"><a href="https://www.linkedin.com/in/tanveenkaur23/" class="btn btn-primary"><i class="fab fa-linkedin-in"></i> </a></p>
                              </div>
                          </div>
                      </div>
                      <div class="col-lg-4">
                          <div class="card team-card">
                              <img src="{{url('/')}}/public/images/team/Dr. Lakshmi Krishnan.jpg" height="254px" class="card-img-top" alt="...">
                              <div class="card-body">
                                  <h5 class="card-title">Dr. Lakshmi Krishnan</h5>
                                  <p class="card-text">Senior Clinical Research Scientist</p> 
                                  <p class="text-center"><a href="https://www.linkedin.com/in/dr-lakshmi-krishnan-b694a4146" class="btn btn-primary"><i class="fab fa-linkedin-in"></i> </a></p>
                              </div>
                          </div>
                      </div> -->
                      </div>
              </div>
          </div>
          <div class="team-box" id="enterprisedivision">
              <div class="container">
                  <div class="row gx-1">
                  <div class="col-lg-4">
                          <div class="card team-card">
                              <img src="{{url('/')}}/public/images/team/Kamal Valecha.jpg" class="card-img-top" alt="...">
                              <div class="card-body">
                                  <h5 class="card-title">Kamal Valecha</h5>
                                  <!-- <p class="card-text">Business Development Manager</p> -->
                                  <p class="text-center"><a href="https://www.linkedin.com/in/kamalvalecha/" class="btn btn-primary"><i class="fab fa-linkedin-in"></i> </a></p>
                              </div>
                          </div>
                      </div>   
                      <div class="col-lg-4">
                          <div class="card team-card">
                              <img src="{{url('/')}}/public/images/team/thumbnail.png" class="card-img-top" alt="...">
                              <div class="card-body">
                                  <h5 class="card-title">Siddaraju N G </h5>
                                  <!-- <p class="card-text">Regional Sales Head</p> -->
                                  <p class="text-center"><a href="https://www.linkedin.com/in/siddaraju-n-g-13335411" class="btn btn-primary"><i class="fab fa-linkedin-in"></i> </a></p>
                              </div>
                          </div>
                      </div>
               
                      <div class="col-lg-4">
                          <div class="card team-card">
                              <img src="{{url('/')}}/public/images/team/Vignesh SJ.jpg" class="card-img-top" alt="...">
                              <div class="card-body">
                                  <h5 class="card-title">Vignesh SJ</h5>
                                  <!-- <p class="card-text">Zonal Head- Tamil Nadu</p> -->
                                  <p class="text-center"><a href="https://www.linkedin.com/in/vignesh-sundararajan-jayanthi-6282336a?lipi=urn%3Ali%3Apage%3Ad_flagship3_profile_view_base_contact_details%3Bmdi1Yj1UQ72Jk1nDC4cUUg%3D%3D" class="btn btn-primary"><i class="fab fa-linkedin-in"></i> </a></p>
                              </div>
                          </div>
                      </div>
                      <div class="col-lg-4"></div>
                      <div class="col-lg-4">
                          <div class="card team-card">
                              <img src="{{url('/')}}/public/images/team/Aastha Mishra.jpg" height="254px" class="card-img-top" alt="...">
                              <div class="card-body">
                                  <h5 class="card-title">Aastha Mishara</h5>
                                  <!-- <p class="card-text">Chief Business Officer</p> -->
                                  <p class="text-center"><a href="https://www.linkedin.com/in/aastha-mishra-947878135/" class="btn btn-primary"><i class="fab fa-linkedin-in"></i> </a></p>
                              </div>
                          </div>
                      </div>  <div class="col-lg-4"></div>
                       </div>
              </div>
          </div>
          <div class="team-box" id="QARA">
              <div class="container">
                  <div class="row gx-1">
                  <div class="col-lg-4">
                          <div class="card team-card">
                              <img src="{{url('/')}}/public/images/team/thumbnail.png" class="card-img-top" alt="...">
                              <div class="card-body">
                                  <h5 class="card-title">Sneha Valluru</h5>
                                  <!-- <p class="card-text">Senior Associate</p> -->
                                  <p class="text-center"><a href="https://www.linkedin.com/in/sneha-s-p-339577122" class="btn btn-primary"><i class="fab fa-linkedin-in"></i> </a></p>
                              </div>
                          </div>
                      </div>
                     <!-- <div class="col-lg-4">
                          <div class="card team-card">
                              <img src="{{url('/')}}/public/images/team/thumbnail.png" class="card-img-top" alt="...">
                              <div class="card-body">
                                  <h5 class="card-title">Nischith Raphael</h5>
                                   <p class="card-text">Senior Specialist & MR</p>
                                  <p class="text-center"><a href="#" class="btn btn-primary"><i class="fab fa-linkedin-in"></i> </a></p>
                              </div>
                          </div>
                      </div> -->
                        <div class="col-lg-4">
                          <div class="card team-card">
                              <img src="{{url('/')}}/public/images/team/Tanmayee Jahagirdar.jpg" class="card-img-top" alt="...">
                              <div class="card-body">
                                  <h5 class="card-title">Tanmayee Jahagirdar</h5>
                                  <!-- <p class="card-text">Senior Associate</p> -->
                                  <p class="text-center"><a href="https://www.linkedin.com/in/tanmayee-jahagirdar-3630343a" class="btn btn-primary"><i class="fab fa-linkedin-in"></i> </a></p>
                              </div>
                          </div>
                      </div>  
                      <div class="col-lg-4">
                          <div class="card team-card">
                              <img src="{{url('/')}}/public/images/team/thumbnail.png" class="card-img-top" alt="...">
                              <div class="card-body">
                                  <h5 class="card-title">Avinash Donthula</h5>
                                  <!-- <p class="card-text">Senior Specialist</p> -->
                                  <p class="text-center"><a href="https://www.linkedin.com/in/avinash-donthula-36923058/" class="btn btn-primary"><i class="fab fa-linkedin-in"></i> </a></p>
                              </div>
                          </div>
                      </div>
                  </div>
              </div>
          </div>
          <div class="team-box" id="screeningoperations">
              <div class="container">
                  <div class="row gx-1">
                   <div class="col-lg-4">
                          <!-- <div class="card team-card">
                              <img src="{{url('/')}}/public/images/team/Somdev Upadhyay.jpg" class="card-img-top" alt="...">
                              <div class="card-body">
                                  <h5 class="card-title">Somdev Upadhyay</h5>
                                   <p class="card-text">Lead- Corporate Sales & BD</p> 
                                  <p class="text-center"><a href="https://www.linkedin.com/in/somdev-upadhyay-90620013/" class="btn btn-primary"><i class="fab fa-linkedin-in"></i> </a></p>
                              </div>
                          </div> -->
                      </div> 
                       <div class="col-lg-4">
                          <div class="card team-card">
                              <img src="{{url('/')}}/public/images/team/Sanjeev Sidaraddi.jpg" class="card-img-top" alt="...">
                              <div class="card-body">
                                  <h5 class="card-title">Sanjeev Sidaraddi</h5>
                                  <!-- <p class="card-text">Program  Manager</p> -->
                                  <p class="text-center"><a href="https://www.linkedin.com/in/sanjeev-sidaraddi-14905775" class="btn btn-primary"><i class="fab fa-linkedin-in"></i> </a></p>
                              </div>
                          </div>
                      </div>
                       </div>
              </div>
          </div>
          <div class="team-box" id="Finance">
              <div class="container">
                  <div class="row gx-1">
                  <div class="col-lg-4"></div>
                  <div class="col-lg-4">
                          <div class="card team-card">
                              <img src="{{url('/')}}/public/images/team/Shankar Boorla.jpg" class="card-img-top" alt="...">
                              <div class="card-body">
                                  <h5 class="card-title">Shankar Boorla</h5>
                                  <!-- <p class="card-text">Financial Controller</p> -->
                                  <p class="text-center"><a href="https://www.linkedin.com/in/shankar-boorla-816b6485/" class="btn btn-primary"><i class="fab fa-linkedin-in"></i> </a></p>
                              </div>
                          </div>
                      </div>
                       </div>
              </div>
          </div>
          <div class="team-box" id="Keymembers">
              <div class="container">
                  <div class="row gx-1">
                  <div class="col-lg-4">
                          <div class="card team-card">
                              <img src="{{url('/')}}/public/images/team/thumbnail.png" class="card-img-top" alt="...">
                              <div class="card-body">
                                  <h5 class="card-title">Dr. HV Ramprakash</h5>
                                  <!-- <p class="card-text">Financial Controller</p> -->
                                  <p class="text-center"><a href="#" class="btn btn-primary"><i class="fab fa-linkedin-in"></i> </a></p>
                              </div>
                          </div>
                      </div>
                      <div class="col-lg-4">
                          <div class="card team-card">
                          <img src="{{url('/')}}/public/images/team/thumbnail.png" class="card-img-top" alt="...">
                              <div class="card-body">
                                  <h5 class="card-title">Dr. Sudhakar</h5>
                                  <!-- <p class="card-text">Financial Controller</p> -->
                                  <p class="text-center"><a href="https://www.linkedin.com/in/dr-sudhakar-sampangi-456206112/" class="btn btn-primary"><i class="fab fa-linkedin-in"></i> </a></p>
                              </div>
                          </div>
                      </div>
                      <div class="col-lg-4">
                          <div class="card team-card">
                          <img src="{{url('/')}}/public/images/team/thumbnail.png" class="card-img-top" alt="...">
                              <div class="card-body">
                                  <h5 class="card-title">Dr. Venkat Ramana Sudigali</h5>
                                  <!-- <p class="card-text">Financial Controller</p> -->
                                  <p class="text-center"><a href="https://www.linkedin.com/in/drvenkatfrcr/" class="btn btn-primary"><i class="fab fa-linkedin-in"></i> </a></p>
                              </div>
                          </div>
                      </div>
                      <div class="col-lg-4">
                          <div class="card team-card">
                          <img src="{{url('/')}}/public/images/team/thumbnail.png" class="card-img-top" alt="...">
                              <div class="card-body">
                                  <h5 class="card-title">Himanshu Madhu</h5>
                                  <!-- <p class="card-text">Financial Controller</p> -->
                                  <p class="text-center"><a href="https://www.linkedin.com/in/himanshumadhu/" class="btn btn-primary"><i class="fab fa-linkedin-in"></i> </a></p>
                              </div>
                          </div>
                      </div>
                      <div class="col-lg-4">
                          <div class="card team-card">
                          <img src="{{url('/')}}/public/images/team/thumbnail.png" class="card-img-top" alt="...">
                               <div class="card-body">
                                  <h5 class="card-title">Dr. Siva Teja Kakileti</h5>
                                  <!-- <p class="card-text">Financial Controller</p> -->
                                  <p class="text-center"><a href="https://www.linkedin.com/in/siva-teja-kakileti-267a3757/" class="btn btn-primary"><i class="fab fa-linkedin-in"></i> </a></p>
                              </div>
                          </div>
                      </div>
                      <div class="col-lg-4">
                          <div class="card team-card">
                          <img src="{{url('/')}}/public/images/team/thumbnail.png" class="card-img-top" alt="...">
                              <div class="card-body">
                                  <h5 class="card-title">Anurag Verma</h5>
                                  <!-- <p class="card-text">Financial Controller</p> -->
                                  <p class="text-center"><a href="https://www.linkedin.com/in/anurag-verma-70b68a/" class="btn btn-primary"><i class="fab fa-linkedin-in"></i> </a></p>
                              </div>
                          </div>
                      </div>
                      <div class="col-lg-2"></div>
                      <div class="col-lg-4">
                          <div class="card team-card">
                          <img src="{{url('/')}}/public/images/team/thumbnail.png" class="card-img-top" alt="...">
                                <div class="card-body">
                                  <h5 class="card-title">Dr. Lakshmi Krishnan</h5>
                                  <!-- <p class="card-text">Financial Controller</p> -->
                                  <p class="text-center"><a href="https://www.linkedin.com/in/dr-lakshmi-krishnan-b694a4146" class="btn btn-primary"><i class="fab fa-linkedin-in"></i> </a></p>
                              </div>
                          </div>
                      </div>
                      <div class="col-lg-4">
                          <div class="card team-card">
                          <img src="{{url('/')}}/public/images/team/thumbnail.png" class="card-img-top" alt="...">
                              <div class="card-body">
                                  <h5 class="card-title">Nischith Raphael</h5>
                                  <!-- <p class="card-text">Financial Controller</p> -->
                                  <p class="text-center"><a href="https://www.linkedin.com/in/nischith-raphael-477a7731/" class="btn btn-primary"><i class="fab fa-linkedin-in"></i> </a></p>
                              </div>
                          </div>
                      </div>
                       </div>
              </div>
          </div>
          <div class="team-box" id="Datascience">
              <div class="container">
                  <div class="row gx-1">
                  <div class="col-lg-4">
                  </div> <div class="col-lg-4">
                          <div class="card team-card">
                              <img src="{{url('/')}}/public/images/team/thumbnail.png" class="card-img-top" alt="...">
                              <div class="card-body">
                                  <h5 class="card-title">Akash Narayana</h5>
                                  <!-- <p class="card-text">Financial Controller</p> -->
                                  <p class="text-center"><a href="https://www.linkedin.com/in/akash-narayana-4667881/" class="btn btn-primary"><i class="fab fa-linkedin-in"></i> </a></p>
                              </div>
                          </div>
                      </div>
                       </div>
              </div>
          </div>
    
          <div class="team-box" id="Leadership">
              <div class="container">
                  <div class="row gx-1">
                  <div class="col-lg-4">
                          <div class="card team-card">
                          <img src="{{url('/')}}/public/images/team/thumbnail.png" class="card-img-top" alt="...">
                                  <div class="card-body">
                                  <h5 class="card-title">Dr. Geetha Manjunath</h5>
                                  <!-- <p class="card-text">Financial Controller</p> -->
                                  <p class="text-center"><a href="https://www.linkedin.com/in/geetha-manjunath-82b8058/" class="btn btn-primary"><i class="fab fa-linkedin-in"></i> </a></p>
                              </div>
                          </div>
                      </div>
                      <div class="col-lg-4">
                          <div class="card team-card">
                          <img src="{{url('/')}}/public/images/team/thumbnail.png" class="card-img-top" alt="...">
                              <div class="card-body">
                                  <h5 class="card-title">Shireen Chopra</h5>
                                  <!-- <p class="card-text">Financial Controller</p> -->
                                  <p class="text-center"><a href="https://www.linkedin.com/in/shireen-chopra-2a680a7/" class="btn btn-primary"><i class="fab fa-linkedin-in"></i> </a></p>
                              </div>
                          </div>
                      </div>
                      <div class="col-lg-4">
                          <div class="card team-card">
                          <img src="{{url('/')}}/public/images/team/thumbnail.png" class="card-img-top" alt="...">
                              <div class="card-body">
                                  <h5 class="card-title">Surajit Mukherjee </h5>
                                  <!-- <p class="card-text">Financial Controller</p> -->
                                  <p class="text-center"><a href="https://www.linkedin.com/in/surajit-mukherjee-05/" class="btn btn-primary"><i class="fab fa-linkedin-in"></i> </a></p>
                              </div>
                          </div>
                      </div>
                      <div class="col-lg-4">
                      </div>
                       <div class="col-lg-4">
                          <div class="card team-card">
                          <img src="{{url('/')}}/public/images/team/thumbnail.png" class="card-img-top" alt="...">
                              <div class="card-body">
                                  <h5 class="card-title">Vicky Nanda</h5>
                                  <!-- <p class="card-text">Financial Controller</p> -->
                                  <p class="text-center"><a href="https://www.linkedin.com/in/vickynanda/" class="btn btn-primary"><i class="fab fa-linkedin-in"></i> </a></p>
                              </div>
                          </div>
                      </div>
                     
                       </div>
              </div>
          </div>
          <div class="team-box" id="Products">
              <div class="container">
                  <div class="row gx-1">
                  <div class="col-lg-4">
                          <div class="card team-card">
                              <img src="{{url('/')}}/public/images/team/Ankita Chaudhari.jpg"  class="card-img-top" alt="...">
                              <div class="card-body">
                                  <h5 class="card-title">Ankita Chaudhari</h5>
                                  <!-- <p class="card-text">Product & UX Designer - Consultant</p> -->
                                  <p class="text-center"><a href="https://www.linkedin.com/in/ankitachaudhari1/" class="btn btn-primary"><i class="fab fa-linkedin-in"></i> </a></p>
                              </div>
                          </div>
                      </div> 
                      <div class="col-lg-4">
                          <div class="card team-card">
                              <img src="{{url('/')}}/public/images/team/Kaundinya Panyam.jpg" class="card-img-top" alt="...">
                              <div class="card-body">
                                  <h5 class="card-title">Kaundinya Panyam</h5>
                                  <!-- <p class="card-text">Lead Product Manager</p> -->
                                  <p class="text-center"><a href="https://www.linkedin.com/in/kaundinyapanyam/" class="btn btn-primary"><i class="fab fa-linkedin-in"></i> </a></p>
                              </div>
                          </div>
                      </div>
                      <div class="col-lg-4">
                          <div class="card team-card">
                              <img src="{{url('/')}}/public/images/team/thumbnail.png" class="card-img-top" alt="...">
                              <div class="card-body">
                                  <h5 class="card-title">Sankar sun Mohapatra</h5>
                                  <!-- <p class="card-text">Product Management Trainee</p> -->
                                  <p class="text-center"><a href="#" class="btn btn-primary"><i class="fab fa-linkedin-in"></i> </a></p>
                              </div>
                          </div>
                      </div>
                      <div class="col-lg-4">
                      </div>   <div class="col-lg-4">
                          <div class="card team-card">
                              <img src="{{url('/')}}/public/images/team/Pratik Katte.jpg" style="height: 278px;" class="card-img-top" alt="...">
                              <div class="card-body">
                                  <h5 class="card-title">Pratik Katte</h5>
                                  <!-- <p class="card-text">Research Engineer</p> -->
                                  <p class="text-center"><a href="https://linkedin.com/in/pratikkatte" class="btn btn-primary"><i class="fab fa-linkedin-in"></i> </a></p>
                              </div>
                          </div>
                      </div>
                       </div>
              </div>
          </div>
          <div class="team-box" id="engineering">
              <div class="container">
                  <div class="row gx-1">
                      <div class="col-lg-4">
                          <div class="card team-card">
                              <img src="{{url('/')}}/public/images/team/Dr Kanchana Gopinath.jpg" class="card-img-top" alt="...">
                             <div class="card-body">
                                  <h5 class="card-title">Dr Kanchana Gopinath</h5>
                                  <!-- <p class="card-text">Senior Manager Special projects</p> -->
                                  <p class="text-center"><a href="https://www.linkedin.com/in/dr-kanchana-gopinath-91b93a15/?lipi=urn%3Ali%3Apage%3Ad_flagship3_feed%3Bz5HogNO6S7WD2f5GTdZBcA%3D%3D" class="btn btn-primary"><i class="fab fa-linkedin-in"></i> </a></p>
                              </div>
                          </div>
                      </div>
                      <div class="col-lg-4">
                          <div class="card team-card">
                              <img src="{{url('/')}}/public/images/team/Manjula Patel.jpg" class="card-img-top" alt="...">
                              <div class="card-body">
                                  <h5 class="card-title">Manjula Patel</h5>
                                  <!-- <p class="card-text">Mobile Lead</p> -->
                                  <p class="text-center"><a href="https://www.linkedin.com/in/manjula-patel-04618018/" class="btn btn-primary"><i class="fab fa-linkedin-in"></i> </a></p>
                              </div>
                          </div>
                      </div>
                      <div class="col-lg-4">
                          <div class="card team-card">
                              <img src="{{url('/')}}/public/images/team/Ronak Dedhiya.jpg" style="height: 278px;" class="card-img-top" alt="...">
                              <div class="card-body">
                                  <h5 class="card-title">Ronak Dedhiya</h5>
                                  <!-- <p class="card-text">Research Engineer</p> -->
                                  <p class="text-center"><a href="https://www.linkedin.com/in/ronak-dedhiya/" class="btn btn-primary"><i class="fab fa-linkedin-in"></i> </a></p>
                              </div>
                          </div>
                      </div> 
                      <div class="col-lg-4">
                          <div class="card team-card">
                              <img src="{{url('/')}}/public/images/team/Aniket Chaturvedi.jpg" class="card-img-top" alt="...">
                              <div class="card-body">
                                  <h5 class="card-title">Aniket Chaturvedi</h5>
                                  <!-- <p class="card-text">Senior Software Engineer</p> -->
                                  <p class="text-center"><a href="https://linkedin.com/in/aniketchtaurvedi19" class="btn btn-primary"><i class="fab fa-linkedin-in"></i> </a></p>
                              </div>
                          </div>
                      </div>
                      <div class="col-lg-4">
                          <div class="card team-card">
                              <img src="{{url('/')}}/public/images/team/Karthik A.jpg" style="height: 278px;" class="card-img-top" alt="...">
                              <div class="card-body">
                                  <h5 class="card-title">Karthik A</h5>
                                  <!-- <p class="card-text">Mobile Application Developer</p> -->
                                  <p class="text-center"><a href="https://www.linkedin.com/in/karthik-a-6064501a2" class="btn btn-primary"><i class="fab fa-linkedin-in"></i> </a></p>
                              </div>
                          </div>
                      </div>
                      <div class="col-lg-4">
                          <div class="card team-card">
                              <img src="{{url('/')}}/public/images/team/thumbnail.png" class="card-img-top" alt="...">
                              <div class="card-body">
                                  <h5 class="card-title">Nethaji V</h5>
                                  <!-- <p class="card-text">Mobile Application Developer</p> -->
                                  <p class="text-center"><a href="https://www.linkedin.com/in/nethaji-v-658a8a109/" class="btn btn-primary"><i class="fab fa-linkedin-in"></i> </a></p>
                              </div>
                          </div>
                      </div>
                      <div class="col-lg-4">
                          <div class="card team-card">
                              <img src="{{url('/')}}/public/images/team/thumbnail.png" class="card-img-top" alt="...">
                              <div class="card-body">
                                  <h5 class="card-title">Akhil D</h5>
                                  <!-- <p class="card-text">Mobile Application Developer</p> -->
                                  <p class="text-center"><a href="https://www.linkedin.com/in/akhil-damodar-05b22b234" class="btn btn-primary"><i class="fab fa-linkedin-in"></i> </a></p>
                              </div>
                          </div>
                      </div>
                      <div class="col-lg-4">
                          <div class="card team-card">
                              <img src="{{url('/')}}/public/images/team/Sunag R A.jpg" class="card-img-top" alt="...">
                              <div class="card-body">
                                  <h5 class="card-title">Sunag R A</h5>
                                  <!-- <p class="card-text">Research Engineer</p> -->
                                  <p class="text-center"><a href="www.linkedin.com/in/sunagra" class="btn btn-primary"><i class="fab fa-linkedin-in"></i> </a></p>
                              </div>
                          </div>
                      </div>
                      <div class="col-lg-4">
                          <div class="card team-card">
                              <img src="{{url('/')}}/public/images/team/Indu.jpg" class="card-img-top" alt="...">
                              <div class="card-body">
                                  <h5 class="card-title">Indu</h5>
                                  <!-- <p class="card-text">Test Lead</p> -->
                                  <p class="text-center"><a href="https://www.linkedin.com/in/indu-srinivasachar-526ab326/" class="btn btn-primary"><i class="fab fa-linkedin-in"></i> </a></p>
                              </div>
                          </div>
                      </div>
                      </div>
              </div>
          </div>
          <div class="vertical-space-50"></div>
      </div>

  </section>

  <section class="ceo-section">
      <div class="container">
          <div class="vertical-space-50"></div>
          <!-- <h4 class="main-title mb-3">&nbsp;Message From CEO</h4> -->
          <div class="vertical-space-50"></div>
         
          <div class="row">
              <div class="col-lg-6">
              <img class="rounded mx-auto d-block partner-img img-fluid" src="{{url('/public/images/doctor/Dr. Geetha Manjunath - Founder _ CEO.jpg')}}" width="40%">
                                      </div>
              <div class="col-lg-6">
                  <div class="partnership">
                  <div class="vertical-space-40"></div>
                      <div class="title ">Dr. Geetha Manjunath</div>
                      <p class="main-text mt-3">
                      When I lost my cousins, I started Niramai because it motivated my curiosity to know ways for early detection of breast cancer. I picked up the topics of Artificial Intelligence & data mining & thought that there could be a new way through which we could detect the early signs of breast cancer. After successfully testing more than 50,000 women in India, we are now trying to prove this test useful for other countries as well. I believe it has the capacity to change the health sector in a significant way. 
                                 </div>
                          
              </div>
          </div>
      </div>
  </section>
  <div class="vertical-space-50"></div>
         
  <section>
      <div class="container">
          <div class="blog-detail">
              <div class="vertical-space-70"></div>
              <h5 class="main-title">&nbsp;&nbsp;&nbsp;Read Our Blog</h5>
        
              <div class="row">

                  <div class="col-lg-12">
                      <div class="vertical-space-50"></div>
                      <div class="ourblog owl-carousel" id="ourblog">
                          <div class="item">
                              <div class="blog-card">
                                  <div class="">
                                      <img src="https://www.niramai.com/wp-content/uploads/2022/02/winners-732x412.png" class="" alt="...">
                                  </div>
                                  <div class="blog-card-content">
                                      <h5 class="blog-card-subtitle">World Bank Group and CTA announce winners of Global Women’s HealthTech Awards</h5>
                                      <!-- <h5 class="blog-card-title">Online Appointment</h5> -->
                                      <p class="blog-card-text" style="text-align: justify;">
                                          Niramai among winners of The Global Women’s HealthTech Awards announced by The World Bank Group and the Consumer Technology Association (CTA).
                                      </p>
                                      <p><a href="https://pressroom.ifc.org/all/pages/PressDetail.aspx?ID=26778" class="btn blog-btn">Learn More <i class="fas fa-long-arrow-alt-right ms-2"></i></a></p>
                                  </div>
                              </div>
                          </div>
                          <div class="item">
                              <div class="blog-card">
                                  <div class="">
                                      <img src="https://www.niramai.com/wp-content/uploads/2021/04/Geethamajunath-1617690828298-732x586.jpg" class="" alt="...">
                                  </div>
                                  <div class="blog-card-content">
                                      <h5 class="blog-card-subtitle">Niramai receives CE Mark – Regulatory approval for Europe</h5>
                                      <!-- <h5 class="blog-card-title">Online Appointment</h5> -->
                                      <p class="blog-card-text" style="text-align: justify;">
                                          Bengaluru-based healthtech startup Niramai Health Analytix’s patented, radiation-free breast
                                      </p>
                                      <p><a href="https://yourstory.com/2021/04/healthtech-startup-niramai-set-expand-global-markets" class="btn blog-btn">Learn More <i class="fas fa-long-arrow-alt-right ms-2"></i></a></p>
                                  </div>
                              </div>
                          </div>
                          <div class="item">
                              <div class="blog-card">
                                  <div class="">
                                      <img src="https://www.niramai.com/wp-content/uploads/2019/12/cnb_447_732.png" class="" alt="...">
                                  </div>
                                  <div class="blog-card-content">
                                      <h5 class="blog-card-subtitle">Meet India’s only startup among CB Insights’ 100 most promising AI startups</h5>
                                      <!-- <h5 class="blog-card-title">Online Appointment</h5> -->
                                      <p class="blog-card-text">
                                          Niramai – a Bengaluru-based AI-powered breast cancer screening solution is the only start-up from
                                          India that has made it to the list <br /></p>
                                      <p><a href="https://www.financialexpress.com/industry/sme/meet-indias-only-startup-among-cb-insights-100-most-promising-ai-startups/1490309/" class="btn blog-btn">Learn More <i class="fas fa-long-arrow-alt-right ms-2"></i></a></p>
                                  </div>
                              </div>
                          </div>
                          <div class="item">
                              <div class="blog-card">
                                  <div class="">
                                      <img src="https://www.niramai.com/wp-content/uploads/2018/07/Webp.net-resizeimage-16.jpg" class="" alt="...">
                                  </div>
                                  <div class="blog-card-content">
                                      <h5 class="blog-card-subtitle">Niramai: The bot will see you now – Forbes India</h5>

                                      <p class="blog-card-text">The health care startup’s AI-based, radiation-free method is making
                                          breast cancer screening more accessible
                                          In 2013, as head of the data analytics team at a multinational technology
                                      <p><a href="https://www.forbesindia.com/article/ai-work/niramai-the-bot-will-see-you-now/50709/1" class="btn blog-btn">Learn More <i class="fas fa-long-arrow-alt-right ms-2"></i></a></p>
                                  </div>
                              </div>
                          </div>
                          <div class="item">
                              <div class="blog-card">
                                  <div class="">
                                      <img src="https://www.niramai.com/wp-content/uploads/2017/08/Webp.net-resizeimage-1.jpg" class="" alt="...">
                                  </div>
                                  <div class="blog-card-content">
                                      <h5 class="blog-card-subtitle">Forbes: These Four Indian Startups Are Revolutionizing How To Detect Cancer</h5>
                                      <p class="blog-card-text">
                                          NIRAMAI, which stands for Non-Invasive Risk Assessment with Machine Intelligence, uses thermography and machine learning to detect early signs of breast cancer.
                                      </p>
                                      <p><a href="https://www.forbes.com/sites/sindhujabalaji/2017/07/18/mission-critical-four-indian-startups-tackling-early-stage-cancer-screening/?sh=4775431e5e49" class="btn blog-btn">Learn More <i class="fas fa-long-arrow-alt-right ms-2"></i></a></p>
                                  </div>
                              </div>
                          </div>

                      </div>
                  </div>
                  <div class="col-lg-1"></div>
              </div>
          </div>
          <div class="vertical-space-50"></div>
      </div>
  </section>

  <section class="bac-color">
      <div class="container">
          <div class="blog-detail text-center">
              <div class="vertical-space-70"></div>
              <h5 class="main-title">&nbsp;&nbsp;Our Twitter Feed</h5>
              <div class="vertical-space-50"></div>
               
              <a class="twitter-timeline" data-width="500" data-height="500" href="https://twitter.com/niramaianalytix?ref_src=twsrc%5Etfw">Tweets by niramaianalytix</a> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
          </div>
      </div>
  </section>

  <!-- <section>
      <div class="container">
          <div class="blog-detail">
              <div class="vertical-space-70"></div>
              <h5 class="main-title">&nbsp;&nbsp;&nbsp;Testimonials</h5>
              <p class="main-text text-center">Problems trying to resolve the conflict between the two major realms of Classical physics:Newtonian mechanics</p>
              <div class="vertical-space-50"></div>
              <div class="row">
                  <div class="col-lg-6">
                      <div class="testimonial-card">
                          <p class="text-center "><img src="{{url('/')}}/public/images/kiran-mazundar-rect.png" class="testimonial-img"></p>
                          <h5 class="testimonial-title">Dr. Kiran Mazumdar-Shaw</h5>
                          <h6 class="testimonial-subtitle">Chairperson & Managing Director, Biocon Limited</h6>
                          <p class="testimonial-text">
                          It is very simple and everybody should be aware of it. It will be performed by a female technician who will explain how the test is performed before the screening procedure. No one else will be there with the patient or seeing the patient. Mammogram is the gold standard for breast cancer screening but can be done only after 40 yrs because of radiation effects. High-risk people with a family history of cancer are screened early at the age of 35. When a woman is pregnant and has a lump in her breast, ultrasound was once the only option, but now thermography is an option as well. When we can't perform mammography on teenage girls or younger women under the age of 30, thermography is a boon. We can perform the test more regularly and frequently because it is radiation-free. 
                          </p>
                             </div>
                  </div>
                  <div class="col-lg-6">
                      <div class="testimonial-card">
                          <p class="text-center "><img src="{{url('/')}}/public/images/kiran-mazundar-rect.png" class="testimonial-img "></p>
                          <h5 class="testimonial-title">Dr. Kiran Mazumdar-Shaw</h5>
                          <h6 class="testimonial-subtitle">Chairperson & Managing Director, Biocon Limited</h6>
                          <p class="testimonial-text">
                          Mammography, a good diagnostic instrument, was used to screen for breast cancer. Unfortunately mammography is not feasible to all the people in India. In recent times, thermal mammography, a fusion of thermal imaging and artificial intelligence has come up. It is very easy to test with no touch technology, good sensitivity and accuracy. It can also detect lesions of  less than 5 mm in both the breasts. The report is also generated in 5 to 10 mins. Dr. Geeta Manjunath is the founder of this technology, Niramai. One of its foundations is easy screening of breast cancer .They are going to rural places, corporate hospitals and several other places to screen breast cancer. They are also in association with the Karnataka cancer society. They have conducted several programs to increase awareness and help in the early detection of breast cancer. 
    </div>
                  </div>
              </div>
          </div>
      </div>
  </section> -->
  <section>
    <div class="container">
        <div class="vertical-space-100"></div>
        <h4 class="main-title mb-5">&nbsp;Testimonials</h4>
        <div class="owl-carousel" id="advisory-carousel">
        <div class="item">
                <div class="advisory">
                    <div class="row">
                        <div class="col-lg-1">
                          </div>
                        <div class="col-lg-10">
                            <div class="council-desc-block">
                                <div class="council-name">Dr Meenakshi Paramasivam</div>
                                <div class="council-post" style="text-align: justify;">
                                    It is very simple and everybody should be aware of it. It will be performed by a female technician who will explain how the test is performed before the screening procedure. No one else will be there with the patient or seeing the patient. Mammogram is the gold standard for breast cancer screening but can be done only after 40 yrs because of radiation effects. High-risk people with a family history of cancer are screened early at the age of 35. When a woman is pregnant and has a lump in her breast, ultrasound was once the only option, but now thermography is an option as well. When we can't perform mammography on teenage girls or younger women under the age of 30, thermography is a boon. We can perform the test more regularly and frequently because it is radiation-free. 
                                </div>
 </div>
                        </div>
                    </div>
                    
                        </div>

            </div>
            <div class="item">
                <div class="advisory">
                    <div class="row">
                        <div class="col-lg-1">
                           </div>
                        <div class="col-lg-10">
                            <div class="council-desc-block">
                                <div class="council-name">Dr. J.S. Sunitha
</div>
                                <div class="council-post" style="text-align: justify;">
                                Mammography has been used all these years to screen for breast cancer. Unfortunately, mammography is not feasible to all the people in India. In recent times, thermal mammography, a fusion of thermal imaging and artificial intelligence has come up which is a very easy test with no touch technology, good sensitivity and accuracy. It can also detect lesions of  less than 5 mm in both the breasts. The report is generated in 5 to 10 mins. Niramai is going to rural places, corporate hospitals and several other places to screen for breast cancer. They are also in association with the Karnataka cancer society and have done a commendable job in increasing awareness about early detection of breast cancer. 
                            </div>
                            </p>
                            </div>
                        </div>
                    </div>
                    
                        </div>

            </div>
 
            <div class="item">
                <div class="advisory">
                    <div class="row">
                        <div class="col-lg-1">
                        </div>
                        <div class="col-lg-10">
                            <div class="council-desc-block">
                                <div class="council-name">Dr. Sudhakar Sampanagi</div>
                                <div class="council-post" style="text-align: justify;">
                                Mammogram and ultrasound, the most common modalities used for the diagnosis of the disease, have their limitations. For instance, with a mammogram it’s difficult to detect the micro calcification in dense breasts (common in the case of young women), while ultrasound scans are largely subjective, wherein the results vary from radiologist to radiologist. Now, with Thermalytix, bringing together the dual advantages of thermography and artificial intelligence, it’s possible to do away with errors of manual interpretation. It’s a highly accurate and effective way of detecting malignant tumors or lesions at an early stage, thereby preventing a majority of cancer deaths. Unlike traditional diagnostic modalities, here the patient doesn’t have to endure any pain, touch, or radiation. It’s completely non-invasive, yet highly accurate. We can repeat the process any number of times; it’s as easy and quick as taking a photograph. Besides, there are no side-effects. Patients appreciate the fact that they are not exposed to additional pain, discomfort, or unnecessary biopsies, while medical practitioners are able to make early and accurate breast cancer diagnosis. In my clinical practice, I see immense value in a health-tech innovation like Thermalytix that makes healthcare more accessible to  people
                            </div>
</div>
                        </div>
                    </div>
                    
                        </div>

            </div>
        </div>
    </div>
</section>
   @endsection
  @push('scripts')
  <script type="text/javascript">
         $(document).ready(function(){
            var pageURL = $(location).attr("href");
           baseUrl="http://localhost/nirmai";
           if(pageURL==baseUrl+"/aboutus"){
            $('#aleadership').attr("style", "background: #d7358c !important; color: #fff !important");
        }
        
         });

           $('#technology').hide();
        $('#clinicalaffairs').hide();
        $('#enterprisedivision').hide();
        $('#businessinitiatives').hide();
        $('#internationalbusiness').hide();
        $('#HRAdmin').hide();
        $('#QARA').hide();
        $('#Keymembers').hide();
        $('#screeningoperations').hide();
        $('#Finance').hide();
        $('#Products').hide();
        $('#engineering').hide();
        $('#Datascience').hide();
        $('#Leadership').show();
        
$('#atechnology').click(function() {
  $('#technology').show();
  $('#clinicalaffairs').hide();
  $('#enterprisedivision').hide();
  $('#businessinitiatives').hide();
  $('#internationalbusiness').hide();
        $('#QARA').hide();
        $('#HRAdmin').hide();
  $('#screeningoperations').hide();
  $('#Finance').hide();
  $('#Leadership').hide();
  $('#Datascience').hide();
  $('#Keymembers').hide();
  $('#Products').hide();
});

$('#abusinessinitiatives').click(function() {
    window.location.replace(baseUrl+"/aboutus#businessinitiatives");
    $('#abusinessinitiatives').attr("style", "background: #d7358c !important; color: #fff !important");
    $('#aleadership').attr("style", "background: #fff !important; color: #000 !important");
    $('#aclinicalaffairs').attr("style", "background: #fff !important; color: #000 !important");
    $('#aenterprisedivision').attr("style", "background: #fff !important; color: #000 !important");
    $('#ascreeningoperations').attr("style", "background: #fff !important; color: #000 !important");
    $('#akeymembers').attr("style", "background: #fff !important; color: #000 !important");
    $('#adatascience').attr("style", "background: #fff !important; color: #000 !important");
    $('#afinance').attr("style", "background: #fff !important; color: #000 !important");
    $('#aproducts').attr("style", "background: #fff !important; color: #000 !important");
    $('#aengineering').attr("style", "background: #fff !important; color: #000 !important");
    $('#aqara').attr("style", "background: #fff !important; color: #000 !important");
  
  $('#technology').hide();
  $('#clinicalaffairs').hide();
  $('#enterprisedivision').hide();
  $('#businessinitiatives').show();
  $('#internationalbusiness').hide();
  $('#HRAdmin').hide();
       $('#QARA').hide();
  $('#screeningoperations').hide();
  $('#Finance').hide();
  $('#Leadership').hide();
  $('#Datascience').hide();
  $('#Keymembers').hide();
     $('#Products').hide();
});

$('#ainternationalbusiness').click(function() {
  $('#technology').hide();
  $('#clinicalaffairs').hide();
  $('#enterprisedivision').hide();
  $('#businessinitiatives').hide();
  $('#internationalbusiness').show();
  $('#HRAdmin').hide();
        $('#QARA').hide();
  $('#screeningoperations').hide();
  $('#Finance').hide();
  $('#Leadership').hide();
  $('#Datascience').hide(); 
    $('#Keymembers').hide();
  $('#Products').hide();
});
$('#ahradmin').click(function() {
  $('#technology').hide();
  $('#clinicalaffairs').hide();
  $('#enterprisedivision').hide();
  $('#businessinitiatives').hide();
  $('#internationalbusiness').hide();
  $('#HRAdmin').show();
        $('#QARA').hide();
  $('#screeningoperations').hide();
  $('#Finance').hide();
  $('#Leadership').hide();
  $('#Keymembers').hide();
     $('#Datascience').hide();
  $('#Products').hidse();
});
$('#aclinicalaffairs').click(function() {     
    window.location.replace(baseUrl+"/aboutus#clinicalaffairs");
    $('#aclinicalaffairs').attr("style", "background: #d7358c !important; color: #fff !important");
    $('#aleadership').attr("style", "background: #fff !important; color: #000 !important");
    $('#aenterprisedivision').attr("style", "background: #fff !important; color: #000 !important");
    $('#abusinessinitiatives').attr("style", "background: #fff !important; color: #000 !important");
    $('#ascreeningoperations').attr("style", "background: #fff !important; color: #000 !important");
    $('#akeymembers').attr("style", "background: #fff !important; color: #000 !important");
    $('#adatascience').attr("style", "background: #fff !important; color: #000 !important");
    $('#afinance').attr("style", "background: #fff !important; color: #000 !important");
    $('#aproducts').attr("style", "background: #fff !important; color: #000 !important");
    $('#aengineering').attr("style", "background: #fff !important; color: #000 !important");
    $('#aqara').attr("style", "background: #fff !important; color: #000 !important");
  
     $('#technology').hide();
    $('#clinicalaffairs').show();
  $('#enterprisedivision').hide();
  $('#businessinitiatives').hide();
  $('#internationalbusiness').hide();
       $('#QARA').hide();
       $('#HRAdmin').hide();
  $('#screeningoperations').hide();
  $('#Finance').hide();
  $('#Products').hide();
  $('#Keymembers').hide();
     $('#Leadership').hide();
  $('#Datascience').hide();
  $('#engineering').hide();
});
$('#aenterprisedivision').click(function() {
    window.location.replace(baseUrl+"/aboutus#enterprisedivision");
    $('#aenterprisedivision').attr("style", "background: #d7358c !important; color: #fff !important");
    $('#aleadership').attr("style", "background: #fff !important; color: #000 !important");
    $('#aclinicalaffairs').attr("style", "background: #fff !important; color: #000 !important");
    $('#abusinessinitiatives').attr("style", "background: #fff !important; color: #000 !important");
    $('#ascreeningoperations').attr("style", "background: #fff !important; color: #000 !important");
    $('#akeymembers').attr("style", "background: #fff !important; color: #000 !important");
    $('#adatascience').attr("style", "background: #fff !important; color: #000 !important");
    $('#afinance').attr("style", "background: #fff !important; color: #000 !important");
    $('#aproducts').attr("style", "background: #fff !important; color: #000 !important");
    $('#aengineering').attr("style", "background: #fff !important; color: #000 !important");
    $('#aqara').attr("style", "background: #fff !important; color: #000 !important");
  
     
    $('#technology').hide();
  $('#clinicalaffairs').hide();
  $('#enterprisedivision').show();
  $('#businessinitiatives').hide();
  $('#internationalbusiness').hide();
       $('#QARA').hide();
       $('#HRAdmin').hide();
$('#screeningoperations').hide();
$('#Finance').hide();  
$('#Products').hide(); 
$('#Leadership').hide();
$('#Keymembers').hide();
     $('#Datascience').hide();
$('#engineering').hide(); 
});
$('#aqara').click(function() {
    window.location.replace(baseUrl+"/aboutus#QARA");
    $('#aqara').attr("style", "background: #d7358c !important; color: #fff !important");
    $('#aleadership').attr("style", "background: #fff !important; color: #000 !important");
    $('#aclinicalaffairs').attr("style", "background: #fff !important; color: #000 !important");
    $('#aenterprisedivision').attr("style", "background: #fff !important; color: #000 !important");
    $('#abusinessinitiatives').attr("style", "background: #fff !important; color: #000 !important");
    $('#ascreeningoperations').attr("style", "background: #fff !important; color: #000 !important");
    $('#akeymembers').attr("style", "background: #fff !important; color: #000 !important");
    $('#afinance').attr("style", "background: #fff !important; color: #000 !important");
    $('#adatascience').attr("style", "background: #fff !important; color: #000 !important");
    $('#aengineering').attr("style", "background: #fff !important; color: #000 !important");
    $('#aproducts').attr("style", "background: #fff !important; color: #000 !important");
 
    $('#technology').hide();
  $('#clinicalaffairs').hide();
  $('#enterprisedivision').hide();
  $('#businessinitiatives').hide();
  $('#internationalbusiness').hide();
        $('#QARA').show();
        $('#HRAdmin').hide();
  $('#screeningoperations').hide();
  $('#Finance').hide();
  $('#Products').hide();
  $('#Keymembers').hide();
     $('#Leadership').hide();
  $('#Datascience').hide();
  $('#engineering').hide();
});
$('#ascreeningoperations').click(function() {
    window.location.replace(baseUrl+"/aboutus#screeningoperations");
    $('#ascreeningoperations').attr("style", "background: #d7358c !important; color: #fff !important");
    $('#aleadership').attr("style", "background: #fff !important; color: #000 !important");
    $('#aclinicalaffairs').attr("style", "background: #fff !important; color: #000 !important");
    $('#aenterprisedivision').attr("style", "background: #fff !important; color: #000 !important");
    $('#abusinessinitiatives').attr("style", "background: #fff !important; color: #000 !important");
    $('#akeymembers').attr("style", "background: #fff !important; color: #000 !important");
    $('#adatascience').attr("style", "background: #fff !important; color: #000 !important");
    $('#afinance').attr("style", "background: #fff !important; color: #000 !important");
    $('#aproducts').attr("style", "background: #fff !important; color: #000 !important");
    $('#aengineering').attr("style", "background: #fff !important; color: #000 !important");
    $('#aqara').attr("style", "background: #fff !important; color: #000 !important");
  
    $('#technology').hide();
  $('#clinicalaffairs').hide();
  $('#enterprisedivision').hide();
  $('#businessinitiatives').hide();
  $('#internationalbusiness').hide();
        $('#QARA').hide();
        $('#HRAdmin').hide();
  $('#screeningoperations').show();
  $('#Finance').hide();
  $('#Products').hide();
  $('#Leadership').hide();
  $('#Datascience').hide();
  $('#Keymembers').hide();
     
  $('#engineering').hide();
});
$('#afinance').click(function() {
    window.location.replace(baseUrl+"/aboutus#Finance");
    $('#afinance').attr("style", "background: #d7358c !important; color: #fff !important");
    $('#aleadership').attr("style", "background: #fff !important; color: #000 !important");
    $('#aclinicalaffairs').attr("style", "background: #fff !important; color: #000 !important");
    $('#aenterprisedivision').attr("style", "background: #fff !important; color: #000 !important");
    $('#abusinessinitiatives').attr("style", "background: #fff !important; color: #000 !important");
    $('#ascreeningoperations').attr("style", "background: #fff !important; color: #000 !important");
    $('#akeymembers').attr("style", "background: #fff !important; color: #000 !important");
    $('#aqara').attr("style", "background: #fff !important; color: #000 !important");
    $('#adatascience').attr("style", "background: #fff !important; color: #000 !important");
    $('#aengineering').attr("style", "background: #fff !important; color: #000 !important");
    $('#aproducts').attr("style", "background: #fff !important; color: #000 !important");
 
    $('#technology').hide();
  $('#clinicalaffairs').hide();
  $('#enterprisedivision').hide();
  $('#businessinitiatives').hide();
  $('#internationalbusiness').hide();
        $('#QARA').hide();
        $('#HRAdmin').hide();
  $('#screeningoperations').hide();
  $('#Finance').show();
  $('#Datascience').hide();
  $('#Leadership').hide();
  $('#Products').hide();
  $('#Keymembers').hide();
     
  $('#engineering').hide();
});
$('#adatascience').click(function() {
    window.location.replace(baseUrl+"/aboutus#Datascience");
    $('#adatascience').attr("style", "background: #d7358c !important; color: #fff !important");
    $('#aleadership').attr("style", "background: #fff !important; color: #000 !important");
    $('#aclinicalaffairs').attr("style", "background: #fff !important; color: #000 !important");
    $('#aenterprisedivision').attr("style", "background: #fff !important; color: #000 !important");
    $('#abusinessinitiatives').attr("style", "background: #fff !important; color: #000 !important");
    $('#ascreeningoperations').attr("style", "background: #fff !important; color: #000 !important");
    $('#akeymembers').attr("style", "background: #fff !important; color: #000 !important");
    $('#afinance').attr("style", "background: #fff !important; color: #000 !important");
    $('#aproducts').attr("style", "background: #fff !important; color: #000 !important");
    $('#aengineering').attr("style", "background: #fff !important; color: #000 !important");
    $('#aqara').attr("style", "background: #fff !important; color: #000 !important");
 
    $('#technology').hide();
  $('#clinicalaffairs').hide();
  $('#enterprisedivision').hide();
  $('#businessinitiatives').hide();
  $('#internationalbusiness').hide();
        $('#QARA').hide();
        $('#HRAdmin').hide();
  $('#screeningoperations').hide();
  $('#Finance').hide();
  $('#Datascience').show();
  $('#Leadership').hide();
  $('#Products').hide();
  $('#Keymembers').hide();
     
  $('#engineering').hide();
});
$('#aproducts').click(function() {
    window.location.replace(baseUrl+"/aboutus#Products");
    $('#aproducts').attr("style", "background: #d7358c !important; color: #fff !important");
    $('#aleadership').attr("style", "background: #fff !important; color: #000 !important");
    $('#aclinicalaffairs').attr("style", "background: #fff !important; color: #000 !important");
    $('#aenterprisedivision').attr("style", "background: #fff !important; color: #000 !important");
    $('#abusinessinitiatives').attr("style", "background: #fff !important; color: #000 !important");
    $('#ascreeningoperations').attr("style", "background: #fff !important; color: #000 !important");
    $('#akeymembers').attr("style", "background: #fff !important; color: #000 !important");
    $('#afinance').attr("style", "background: #fff !important; color: #000 !important");
    $('#adatascience').attr("style", "background: #fff !important; color: #000 !important");
    $('#aengineering').attr("style", "background: #fff !important; color: #000 !important");
    $('#aqara').attr("style", "background: #fff !important; color: #000 !important");
 
    $('#technology').hide();
  $('#clinicalaffairs').hide();
  $('#enterprisedivision').hide();
  $('#businessinitiatives').hide();
  $('#internationalbusiness').hide();
       $('#QARA').hide();
       $('#HRAdmin').hide();
  $('#screeningoperations').hide();
  $('#Finance').hide();
  $('#Leadership').hide();
  $('#Datascience').hide();
  $('#Keymembers').hide();
     
  $('#Products').show();
  $('#engineering').hide();
});
$('#akeymembers').click(function() {
    window.location.replace(baseUrl+"/aboutus#Keymembers");
    $('#akeymembers').attr("style", "background: #d7358c !important; color: #fff !important");
    $('#aleadership').attr("style", "background: #fff !important; color: #000 !important");
    $('#aclinicalaffairs').attr("style", "background: #fff !important; color: #000 !important");
    $('#aenterprisedivision').attr("style", "background: #fff !important; color: #000 !important");
    $('#abusinessinitiatives').attr("style", "background: #fff !important; color: #000 !important");
    $('#ascreeningoperations').attr("style", "background: #fff !important; color: #000 !important");
    $('#adatascience').attr("style", "background: #fff !important; color: #000 !important");
    $('#afinance').attr("style", "background: #fff !important; color: #000 !important");
    $('#aproducts').attr("style", "background: #fff !important; color: #000 !important");
    $('#aengineering').attr("style", "background: #fff !important; color: #000 !important");
    $('#aqara').attr("style", "background: #fff !important; color: #000 !important");
 
    $('#technology').hide();
  $('#clinicalaffairs').hide();
  $('#enterprisedivision').hide();
  $('#businessinitiatives').hide();
  $('#internationalbusiness').hide();
       $('#QARA').hide();
       $('#HRAdmin').hide();
  $('#screeningoperations').hide();
  $('#Finance').hide();
  $('#Leadership').hide();
  $('#Datascience').hide();
  $('#Products').hide();
  $('#Keymembers').show();
  $('#engineering').hide();
});
$('#aleadership').click(function() {
    window.location.replace(baseUrl+"/aboutus#Leadership");
    $('#aleadership').attr("style", "background: #d7358c !important; color: #fff !important");
    $('#aclinicalaffairs').attr("style", "background: #fff !important; color: #000 !important");
    $('#aenterprisedivision').attr("style", "background: #fff !important; color: #000 !important");
    $('#abusinessinitiatives').attr("style", "background: #fff !important; color: #000 !important");
    $('#ascreeningoperations').attr("style", "background: #fff !important; color: #000 !important");
    $('#akeymembers').attr("style", "background: #fff !important; color: #000 !important");
    $('#adatascience').attr("style", "background: #fff !important; color: #000 !important");
    $('#afinance').attr("style", "background: #fff !important; color: #000 !important");
    $('#aproducts').attr("style", "background: #fff !important; color: #000 !important");
    $('#aengineering').attr("style", "background: #fff !important; color: #000 !important");
    $('#aqara').attr("style", "background: #fff !important; color: #000 !important");
    
   $('#technology').hide();
  $('#clinicalaffairs').hide();
  $('#enterprisedivision').hide();
  $('#businessinitiatives').hide();
  $('#internationalbusiness').hide();
       $('#QARA').hide();
       $('#HRAdmin').hide();
  $('#screeningoperations').hide();
  $('#Finance').hide();
  $('#Products').hide();
  $('#Leadership').hide();
  $('#Leadership').show();
  $('#Keymembers').hide();
     
  $('#Datascience').hide();
  $('#engineering').hide();
});
$('#aengineering').click(function() {
    window.location.replace(baseUrl+"/aboutus#engineering");
    $('#aengineering').attr("style", "background: #d7358c !important; color: #fff !important");
    $('#aclinicalaffairs').attr("style", "background: #fff !important; color: #000 !important");
    $('#aenterprisedivision').attr("style", "background: #fff !important; color: #000 !important");
    $('#abusinessinitiatives').attr("style", "background: #fff !important; color: #000 !important");
    $('#ascreeningoperations').attr("style", "background: #fff !important; color: #000 !important");
    $('#akeymembers').attr("style", "background: #fff !important; color: #000 !important");
    $('#adatascience').attr("style", "background: #fff !important; color: #000 !important");
    $('#afinance').attr("style", "background: #fff !important; color: #000 !important");
    $('#aproducts').attr("style", "background: #fff !important; color: #000 !important");
    $('#aleadership').attr("style", "background: #fff !important; color: #000 !important");
    $('#aqara').attr("style", "background: #fff !important; color: #000 !important");
    
    $('#technology').hide();
  $('#clinicalaffairs').hide();
  $('#enterprisedivision').hide();
  $('#businessinitiatives').hide();
  $('#internationalbusiness').hide();
       $('#QARA').hide();
       $('#HRAdmin').hide();
  $('#screeningoperations').hide();
  $('#Finance').hide();
  $('#Datascience').hide();
  $('#Leadership').hide();
  $('#Keymembers').hide();
     
  $('#Products').hide();
  $('#engineering').show();
});


      $("#ourjourney").owlCarousel({
          loop: false,
          margin: 0,
          nav: true,
          dots: false,
          autoplay: true,
          autoplayTimeout: 3000,
          autoplayHoverPause: true,
          responsive: {
              0: {
                  items: 1,
              },
              600: {
                  items: 4,
              },
              1000: {
                  items: 5,
              },
          },
      });

      $("#ourblog").owlCarousel({
          loop: true,
          margin: 0,
          nav: true,
          dots: true,
          autoplay: true,
          autoplayTimeout: 3000,
          autoplayHoverPause: true,
          responsive: {
              0: {
                  items: 1,
              },
              600: {
                  items: 1,
              },
              1000: {
                  items: 3,
              },
          },
      });

      $("#advisory-carousel").owlCarousel({
        loop: true,
        margin: 10,
        nav: true,
        autoplay: true,
        autoplayTimeout: 6000,
        autoplayHoverPause: true,
        responsive: {
            0: {
                items: 1,
            },
            600: {
                items: 1,
            },
            1000: {
                items: 1,
            },
        },
    });
    
    $(".owl-prev").html('<i class="fa fa-chevron-left"></i>');
    $(".owl-next").html('<i class="fa fa-chevron-right"></i>');
  </script>
  @endpush