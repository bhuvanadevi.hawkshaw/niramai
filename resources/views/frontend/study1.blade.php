@extends('layouts.app')

@section('content')
<style>
    #customers {
        font-family: Arial, Helvetica, sans-serif;
        border-collapse: collapse;
        width: 100%;
    }

    #customers td,
    #customers th {
        border: 1px solid #ddd;
        text-align: center;
        padding: 8px;
    }

    #customers tr:nth-child(even) {
        background-color: #f2f2f2;
    }

    #customers tr:hover {
        background-color: #ddd;
    }

    #customers th {
        padding-top: 12px;
        padding-bottom: 12px;
        text-align: center;
        background-color: #d7358c;
        color: white;
    }

    h4{
        color: #d7358c;
    }
</style>

<section>
    <div class="women">
        <div class="container">
            <div class="vertical-space-70"></div>
            <h4 class="main-title mb-1">Multi-site Prospective</h4>
            <div class="row gx-0">
                <div class="col-lg-12">
                    <p class="main-text mb-5 text-center">
                        Multi-site prospective study using a trial design to show 10% non-inferiority to standard screening modalities (2017-2018) </p>
                </div>
            </div>
        </div>
        <div class="container">
            <h4 class="mb-1">Objective</h4>
            <div class="row gx-0">
                <div class="col-lg-12">
                    <p class="main-text mb-5" style="text-align: justify;">
                        <i class="fa fa-circle" style="font-size:8px;color: #d7358c;"></i>
                        To assess non-inferiority of sensitivity of Thermalytix solution to not more than 10% of the sensitivity of standard screening modalities for detecting breast cancers in women
                    </p>
                </div>
            </div>
        </div>

        <div class="container">
            <h4 class="mb-1">Study Sites</h4>
            <div class="row gx-0">
                <div class="col-lg-12">
                    <p class="main-text mb-5" style="text-align: justify;">
                        <i class="fa fa-circle" style="font-size:8px;color: #d7358c;"></i>
                        Multisite study at two hospitals – Narayana Hrudayalaya (NH) and Health Care
                        Global (HCG) hospitals in Bangalore, India
                    </p>
                </div>
            </div>
        </div>

        <div class="container">
            <h4 class="mb-1">Data and Method</h4>
            <div class="row gx-0">
                <div class="col-lg-12">
                    <p class="main-text" style="text-align: justify;">
                        <i class="fa fa-circle" style="font-size:8px;color: #d7358c;"></i>
                        A prospective multi-centric study was conducted from 21st September 2017 and 31st July 2018 to evaluate the diagnostic screening capabilities of ThermalytixTM among women seeking screening for diagnosis of suspected breast cancer. Ethics committee approvals were obtained from both the institutions prior to the start of the study. The study was registered with the Clinical Trial Registry India (CTRI) prior to the start of the study and the registration number is CTRI/2017/10/ 010115.
                    </p>
                    <p class="main-text" style="text-align: justify;">
                        <i class="fa fa-circle" style="font-size:8px;color: #d7358c;"></i>
                        In total, 326 women who had breast complaints on clinical examinations were recruited for this study. Of these, 68 women were excluded from the analysis, the reasons being - not meeting the eligibility criteria, non-availability of test reports/confirmed final diagnosis/histopathology report, and insufficient cooling effect. All 258 women underwent ThermalytixTM, followed by standard screening procedure, which included one or more of standard 2D mammography (124 women [48.06%]), whole breast ultrasonography (221 women [85.65%]), or biopsy (66 women [25.58%]). ThermalytixTM screening was conducted prior to the mammographic test to avoid thermal interference due to compression of the breast.
                    </p>
                    <p class="main-text mb-5" style="text-align: justify;">
                        <i class="fa fa-circle" style="font-size:8px;color: #d7358c;"></i>
                        Out of 258 women, 63 (24.41%) women were diagnosed as positive for malignancy and 195 (75.58%) were found to be negative as per the findings from standard-of-diagnosis modalities. Results from ThermalytixTM and standard modalities were obtained independently in a blinded fashion for comparison. For each participant, ThermalytixTM output consisted of an overall score ranging from 0 to 1, representing the probability of malignancy, and a vascular score ranging from 0 to 1 indicating the asymmetry in vascular structures between the two breasts.
                    </p>

                </div>
            </div>
        </div>

        <div class="container">
            <h4 class="mb-1">Results</h4>
            <div class="row gx-0">
                <div class="col-lg-12">
                    <p class="main-text" style="text-align: justify;">
                        <i class="fa fa-circle" style="font-size:8px;color: #d7358c;"></i>
                        The sensitivity of ThermalytixTM on 258 symptomatic women was 86% (90% CI, 78% to 93%) and specificity was 70% (90% CI, 64% to 75%). The sensitivity of standard modality (combination of mammography and ultrasound) on 258 women was observed to be 86% (90% CI, 78% to 93%) and specificity was 95% (90% CI, 93% to 98%). Based on mammography reports available for the 124 women who underwent mammography, the sensitivity was 82% (90% CI, 73% to 91%) and specificity was 93% (90% CI, 88% to 98%). The 90% CIs of sensitivity demonstrate that the CI of Thermalytix is within 10% of sensitivity of mammography in the 124 women or the standard modality on all 258 women.
                    </p>
                    <p class="main-text" style="text-align: justify;">
                        <i class="fa fa-circle" style="font-size:8px;color: #d7358c;"></i>
                        Sensitivity derived from considering Thermalytix followed by ultrasound (sono-mammography) reports was found to be 84.2% (90% CI, 76.3%, 92.1%), while the specificity drastically improved from 70% to 99.3%. Since this new workflow of Thermalytix™ followed by ultrasound has high PPV, NPV, specificity, and good overall sensitivity, this combination demonstrates good potential to be implemented as clinical practice, especially for symptomatic women.
                    </p>
                    <p class="main-text mb-5" style="text-align: justify;">
                        <i class="fa fa-circle" style="font-size:8px;color: #d7358c;"></i>
                        Additionally, Thermalytix could identify the smallest malignant lesion of size 4mm x 2.5mm in the study population, highlighting its potential to identify lesions earlier in the course of the disease.
                    </p>

                </div>
            </div>
        </div>

        <div class="container ">
            <h4 class="mb-1">Table 1
            </h4>
            <div class="row gx-0 main-text">
                <div class="col-lg-12">
                    <p class="main-text mb-3" style="text-align: justify;">
                        Comparison of clinical performances of Thermalytix and standard modality (mammography and ultrasound) (n = 258), and mammography (n = 124)
                    </p>

                    <table id="customers">
                        <tr>
                            <th>Device and population</th>
                            <th>Sensitivity, % (90% CI)</th>
                            <th>Specificity, % (90% CI)</th>
                            <th>PPV, % (90% CI)</th>
                            <th>NPV, % (90% CI)</th>
                        </tr>
                        <tr>
                            <td>Thermalytix alone
                                (n = 258)
                            </td>
                            <td>86%
                                (78% - 93%)
                            </td>
                            <td>70%
                                (64% - 75%)
                            </td>
                            <td>
                                48%
                                (40% - 55%)
                            </td>
                            <td>
                                94%
                                (90% - 97%)
                            </td>
                        </tr>
                        <tr>
                            <td>Standard modality (both mammography & ultrasound) (n = 258)</td>
                            <td>86%
(78% - 93%)
</td>
                            <td>95%
(93% - 98%)
</td>
<td>86%
(78% - 93%)
</td>
<td>95%
(93% - 98%)
</td>
                        </tr>
                        <tr>
                            <td>Mammography alone 
(n = 124)
</td>
                            <td>82%
(73% - 91%)
</td>
                            <td>93%
(88% - 98%)
</td>
<td>89%
(82% - 97%)
</td>
<td>88%
(82% - 94%)
</td>
                        </tr>
                        <tr>
                            <td>Thermalytix followed by ultrasound (n = 194)</td>
                            <td>84%
(76% - 92%)
</td>
                            <td>99%
(98%, 100%)
</td>
<td>98%
(95%, 100%)
</td>
<td>
94%
(91%, 97%)
</td>                        </tr>
                    </table>

                </div>
            </div>
        </div>

        <div class="container mt-5">
            <h4 class="mb-1">Conclusion</h4>
            <div class="row gx-0">
                <div class="col-lg-12">
                    <p class="main-text mb-5" style="text-align: justify;">
                        <i class="fa fa-circle" style="font-size:8px;color: #d7358c;"></i>
                        The study showed that the sensitivity and specificity of Thermalytix is non-inferior to mammography with a 10% non-inferiority margin in the study population. Thermalytix test followed by an ultrasound produced better results compared to current standard modality of using both mammography and ultrasound.
                    </p>

                </div>
            </div>
        </div>

        <div class="container">
            <h4 class="mb-1">Publication</h4>
            <div class="row gx-0">
                <div class="col-lg-12">
                    <p class="main-text mb-5" style="text-align: justify;">
                        <i class="fa fa-circle" style="font-size:8px;color: #d7358c;"></i>
                        Singh A, Bhat V, Sudhakar S, et al. Multicentric study to evaluate the effectiveness of Thermalytix as compared with standard screening modalities in subjects who show possible symptoms of suspected breast cancer. BMJ Open. 2021;11(10):e052098. Published 2021 Oct 19. doi:10.1136/bmjopen-2021-052098.
                    </p>

                </div>
            </div>
        </div>


    </div>
</section>
@endsection