@extends('layouts.app')

@section('content')
<style>
.sample::after {
    content: "";
    display: block;
    background-color: #d7355c;
    width: 100px !important;
    height: 4px;
    position: absolute;
    border-radius: 25px;
    content: "";
    position: absolute;
    margin: 0 43%;
    top: -20px;
}
.sample {
    font-family: "Roboto Slab", serif;
    /* font-weight: 200; */
    font-size: 35px;
    padding: 0 0 15px;
    text-align: center;
    width: auto;
    margin: 0 auto;
    text-align: center;
    color: #000;
    font-weight: 400;
    position: relative;
    margin-bottom: 30px;
    display: table;
  }    #customers {
        font-family: Arial, Helvetica, sans-serif;
        border-collapse: collapse;
        width: 100%;
    }

    #customers td,
    #customers th {
        border: 1px solid #ddd;
        text-align: center;
        padding: 8px;
    }

    #customers tr:nth-child(even) {
        background-color: #f2f2f2;
    }

    #customers tr:hover {
        background-color: #ddd;
    }

    #customers th {
        padding-top: 12px;
        padding-bottom: 12px;
        text-align: center;
        background-color: #d7358c;
        color: white;
    }
    h4{
        color: #d7358c;
    }
</style>

<section>
    <div class="women">
        <div class="container">
            <div class="vertical-space-70"></div>
            <h4 class="sample mb-1">Compare Thermography and Thermalytix</h4>
            <div class="row gx-0">
                <div class="col-lg-12">
                    <p class="main-text mb-5 text-center">
                    This single-site retrospective study aimed to compare the efficacy of AI-based Thermalytix™ with Thermography (Expert Interpretation of Thermal images) for Breast Cancer screening. The study was conducted in collaboration with an expert Thermographer who was also a senior radiologist in Central Diagnostics Research Foundation (CDRF), Bangalore, India.
                    </div>
            </div>
        </div>
        <div class="container">
            <h4 class="mb-1">Objective</h4>
            <div class="row gx-0">
                <div class="col-lg-12">
                    <p class="main-text mb-5" style="text-align: justify;">
                        <i class="fa fa-circle" style="font-size:8px;color: #d7358c;"></i>
                        To evaluate the efficacy of AI-based analysis of thermal images over manual interpretation of thermal images.
                      </p>
                </div>
            </div>
        </div>

      <div class="container">
            <h4 class="mb-1">Method</h4>
            <div class="row gx-0">
                <div class="col-lg-12">
                    <p class="main-text mb-5" style="text-align: justify;">
                        <i class="fa fa-circle" style="font-size:8px;color: #d7358c;"></i>
                        This comparative study was performed on 303 patients who walked into this diagnostic centre either for preventive screening or with a breast complaint. 29 of the 303 enrolled were malignant, as per Sono-mammography. Follow-up or post-surgery subjects were not included in this set.
                       </p>
                  
                </div>
            </div>
        </div>

        <div class="container">
            <h4 class="mb-1">Results</h4>
            <div class="row gx-0">
                <div class="col-lg-12">
                     <p class="main-text mb-5" style="text-align: justify;">
                        <i class="fa fa-circle" style="font-size:8px;color: #d7358c;"></i>
                        The sensitivity of Thermalytix was 96.6% using breast Ultrasound as ground truth. Specificity of manual thermal interpretation was 79.2%, while Thermalytix gave 99.6% specificity with just one false positive in 303 subjects. There was also an increase in PPV by 60% as compared to manual interpretation. To compare with Clinical breast examination, it was observed that 13 patients had palpable benign lumps and all those were correctly identified by Thermalytix. The one false negative subject had breast discharge that would have been found in a physical examination.
                     </p>

                </div>
            </div>
        </div>

        <div class="container">
            <h4 class="mb-1">Conclusion</h4>
            <div class="row gx-0">
                <div class="col-lg-12">
                    <p class="main-text mb-5" style="text-align: justify;">
                        <i class="fa fa-circle" style="font-size:8px;color: #d7358c;"></i>
                        The trial clearly showed that Thermalytix, with its locked machine learning algorithm, considerably improved the specificity of Manual Thermography. The specificity of thermal image analysis increased by 20.4% by including the Thermalytix AI module in the workflow. Thus, Thermalytix is a good alternative breast cancer screening modality over traditional Thermography systems.
                      </p>

                </div>
            </div>
        </div>

        <div class="container">
            <h4 class="mb-1">Reference</h4>
            <div class="row gx-0">
                <div class="col-lg-12">
                    <p class="main-text mb-5" style="text-align: justify;">
                        <i class="fa fa-circle" style="font-size:8px;color: #d7358c;"></i>
                        Prakash R, Rajendra A, Manjunath G, Kakileti ST, Madhu H., 2018. Machine Learning over Thermal Images for Accurate Breast Cancer Screening. International Conference on Advances in Breast Cancer Treatments, Kyoto, Japan.
                    </p>

                </div>
            </div>
        </div>

        <div class="container">
            <h4 class="mb-1">Table 4
            </h4>
            <div class="row gx-0 main-text">
                <div class="col-lg-12">
                    <p class="main-text mb-3" style="text-align: justify;">
                    Performance measures of AI-based Thermalytix™ versus manual thermgraphy   </p>

                    <table id="customers">
                        <tr>
                            <th>Clinical Performance Parameters</th>
                            <th>Thermalytix™, %</th>
                            <th>Manual Thermography, %</th>
                        </tr>
                        <tr>
                            <td>Sensitivity
                            </td>
                            <td>96.55%
                            </td>
                            <td>96.55%
                            </td>
                            </tr>
                        <tr>
                            <td>Specificity</td>
                            <td>99.64%
</td>
                            <td>79.2%
</td>
                        </tr>
                        <tr>
                            <td>PPV
</td>
                            <td>96.55%
</td>
                            <td>32.94%
</td>
                        </tr>
                        <tr>
                            <td>NPV</td>
                            <td>99.64%
</td>
                            <td>99.64%
</td>                    </tr>
                    </table>
<p class="main-text mt-3" style="text-align: center;">
For a full list of publications, <a href="#" class="btn common-btn">Read More</a>
               
</p>
                </div>
            </div>
        </div>

       
    </div>
</section>
@endsection