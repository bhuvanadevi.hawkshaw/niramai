@extends('layouts.app')

@section('content')

<section>
    <div class="what_we_do mb-4">
        <div class="container">
            <div class="vertical-space-40"></div>
            <h4 class="main-title mb-1">&nbsp;&nbsp;&nbsp;&nbsp;For Doctors</h4>
            <div class="row gx-0">
                <div class="col-lg-12">
                    <p class="main-text mb-2" style="text-align: justify;">
                        We, at NIRAMAI, have developed a novel method of screening breast cancer combining Thermography & Artificial Intelligence(AI) to detect breast cancer at a much earlier stage than traditional methods or self-examination. Our solution is a low cost, accurate, automated, portable cancer screening tool that can be operated in any clinic. Our imaging method is radiation free, non-touch, not painful and works for women of all ages. This unique solution can be used as a cancer diagnosis test in hospitals, for regular preventive health checkups, and also for large scale screening in rural and semi-urban areas.
                        <br />
                        Interested in installing Niramai Thermalytix in your clinic?
                        <br/><br/>
                       <p class="text-center"> <a href="{{ url('book_appointment') }}" class="btn common-btn">Get Niramai Screening In Your Clinic</a></p>
                    </p>

                </div>
            </div>
        </div>
    </div>
</section>

<section>
    <div class="what_we_do">
        <div class="container">
            <div class="vertical-space-40"></div>
            <h4 class="main-title mb-1">Clinical Validation of Thermalytix&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</h4>
            <div class="row gx-0">
                <div class="col-lg-12">
                    <p class="main-text" style="text-align: justify;">
                        <i class="fa fa-circle" style="font-size:7px;color:#d7358c"></i>
                        Thermography, a non-invasive procedure, can detect cancer much earlier than the standard methods. The latest thermal cameras are capable of measuring breast surface temperature differences of even 0.05°C and can be used to detect high thermal changes resulting from the highly metabolic cancer cells and those induced by neo-angiogenesis. Though thermography as a screening tool was approved as an adjunct modality in 1984 itself, it was not widely accepted then as manual interpretation of thermograms is highly complex and subjective. However, with recent advancements in high-resolution thermal cameras, and most importantly, the combination of thermography with artificial intelligence (AI) for automated analysis, the results are quantitative and consistent. While a 2D mammogram costs 350,000 USD, a good thermal camera can be procured at 10,000 USD. In resource-constrained settings such as in low- and middle-income countries (LMICs), where the radiologist to population ratio is as low as 1:100,000, such technology-assisted solutions can emerge as an affordable and scalable screening method.
                    </p>
                    <p class="main-text" style="text-align: justify;">
                        <i class="fa fa-circle" style="font-size:7px;color:#d7358c"></i> The use of artificial intelligence (AI) and machine learning (ML) in medical imaging has shown to reduce subjectivity in interpretation, by enabling automation for quantitative and consistent interpretation. AI-based thermal imaging is emerging as a radiation-free, low-cost solution for breast cancer screening, particularly useful for rural areas that may often lack imaging equipment and/or radiologists. Automated techniques also reduce the backlog created in the health system by the COVID-19 pandemic. Such a solution will also reduce the burden on the radiologist, who can now focus on a population that requires additional evaluation and investigation.
                    </p>
                    <p class="main-text" style="text-align: justify;">
                        <i class="fa fa-circle" style="font-size:7px;color:#d7358c"></i> The proposed solution used AI on thermal imaging to create a novel test called Thermalytix ™ that has also been clinical evaluated and compared with standard-of-care tests (2D mammography, ultrasound and biopsy).
                    </p>
                    <p class="main-text" style="text-align: justify;">
                        <i class="fa fa-circle" style="font-size:7px;color:#d7358c"></i> In the following sections, three pivotal clinical studies that were performed from 2017 to 2020 in India are outlined.
                    </p>
<div class="row col-lg-12"  id="clinical_validation">
                    <div class="col-lg-3">
                            <div class="vertical-space-40"></div>
                            <div class="content_card">
                            <a href="{{url('study1')}}" target="_blank">
                                    <div class="card-head text-dark main-text">STUDY 1</div>
                                <div class="text-center">
                                    <img src="{{base_url('public/images/High-Accuracy.png')}}" width="100%" class="health-card-logo">
                                </div>
                                <div class="vertical-space-5"></div>
                            <p class="content_card-text main-text" style="text-align: justify;">
                            Multi-site prospective study using a trial design to show 10% non-inferiority to standard screening modalities in symptomatic women.
                           </p>
                           </a>
                        </div>

                        </div>

                        <div class="col-lg-3">
                            <div class="vertical-space-40"></div>
                            <div class="content_card">
                            <a href="{{url('study2')}}" target="_blank">
                                    <div class="card-head text-dark main-text">STUDY 2</div>
                                <div class="text-center">
                                    <img src="{{base_url('public/images/High-Accuracy.png')}}" width="100%" class="health-card-logo">
                                </div>
                                <div class="vertical-space-5"></div>
                            <p class="content_card-text main-text" style="text-align: justify;">
                            Observational Study To Evaluate The Clinical Efficacy Of Thermalytix™ For Detecting Breast Cancer In Symptomatic And Asymptomatic Women.  </p>
                           </a>
                        </div>

                        </div>

                        <div class="col-lg-3">
                            <div class="vertical-space-40"></div>
                            <div class="content_card">
                            <a href="{{url('study3')}}" target="_blank">
                                    <div class="card-head text-dark main-text">STUDY 3</div>
                                <div class="text-center">
                                    <img src="{{base_url('public/images/High-Accuracy.png')}}" width="100%" class="health-card-logo">
                                </div>
                                <div class="vertical-space-5"></div>
                            <p class="content_card-text main-text" style="text-align: justify;">
                            Prospective Single Centre Study To Evaluate The Clinical Efficacy Of Thermalytix™ Over Mammography In Women With Dense And Non-Dense Breasts.
                             </p>
                           </a>
                        </div>

                        </div>

                        <div class="col-lg-3">
                            <div class="vertical-space-40"></div>
                            <div class="content_card">
                            <a href="{{url('study4')}}" target="_blank">
                                    <div class="card-head text-dark main-text">STUDY 4</div>
                                <div class="text-center">
                                    <img src="{{base_url('public/images/High-Accuracy.png')}}" width="100%" class="health-card-logo">
                                </div>
                                <div class="vertical-space-5"></div>
                            <p class="content_card-text main-text" style="text-align: justify;">
                            A Study Conducted To Compare Thermography And Thermalytix
<br/><br/><br/><br/>
</p>
                           </a>
                        </div>

                        </div>
                     
</div>
                    
                 </div>
            </div>
        </div>
    </div>
</section>

@endsection