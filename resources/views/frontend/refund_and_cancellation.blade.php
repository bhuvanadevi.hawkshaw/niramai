@extends('layouts.app')

@section('content')
<style>
    #customers {
        font-family: Arial, Helvetica, sans-serif;
        border-collapse: collapse;
        width: 100%;
    }

    #customers td,
    #customers th {
        border: 1px solid #ddd;
        text-align: center;
        padding: 8px;
    }

    #customers tr:nth-child(even) {
        background-color: #f2f2f2;
    }

    #customers tr:hover {
        background-color: #ddd;
    }

    #customers th {
        padding-top: 12px;
        padding-bottom: 12px;
        text-align: center;
        background-color: #d7358c;
        color: white;
    }

    h4{
        color: #d7358c;
    }
</style>

<section>
    <div class="women">
        <div class="container">
            <div class="vertical-space-70"></div>
            <h4 class="main-title mb-1">Refund and Cancellation  Policy&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</h4>
               </div>
        <div class="container">
            <div class="row gx-0">
                <div class="col-lg-12">
                    <p class="main-text mb-5" style="text-align: justify;">
                    Once Niramai receives a cancellation for a service (e.g. home screening) that has been done online,  a refund is issued to the original payment method (in case of online  transactions).

                    </p>     </div>
            </div>
        </div>

        <div class="container">
            <h4 class="mb-1  text-dark text-left"style="font-size:18px"><b>Refund Timelines</b></h4>
            <div class="row gx-0">
                <div class="col-lg-12">
      <p class="main-text">The time taken to process a normal refund depends on the payment mode that was used while making the payment.
      </p>
                </div>
            </div>
          <br/><br/>  <table id="customers">
                        <tr>
                            <th>Payment Method</th>
                            <th>Refund Time</th>
                        </tr>
                        <tr>
                            <td>Credit or Debit cards
                            </td>
                            <td>5-10 days
                            </td>
                            </tr>
                        <tr>
                            <td>Netbanking</td>
                            <td>2-10 days
</td>
                           </tr>
                        <tr>
                            <td>Wallet
</td>
                            <td>0-3 days
</td>
                            </tr>
                        <tr>
                            <td>UPI</td>
                            <td>2-7 days
</td>
                                </tr>
                    </table>
        </div>
    </div>
</section>


@endsection