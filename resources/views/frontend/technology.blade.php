@extends('layouts.app')

@section('content')

<style>
@media screen and (max-width: 680px){
    .partner-img{
     
    position: relative !important;
    top: -30px !important;
    }
    .main-title::after {
    content: "";
    display: block;
    background-color: #d7355c;
    width: 70px !important;
    height: 4px;
    position: absolute;
    border-radius: 25px;
    content: "";
    position: absolute;
    margin: 0 12%;
    top: -20px;
}
  }

  .main-title::after {
    content: "";
    display: block;
    background-color: #d7355c;
    width: 70px !important;
    height: 4px;
    position: absolute;
    border-radius: 25px;
    content: "";
    position: absolute;
    margin: 0 35%;
    top: -20px;
}
</style>
@if($technology)

<section>
@foreach($technology as $row)
  <div class="about-us">
          <div class="container">
              <div class="vertical-space-70"></div>
              <h4 class="main-title mb-5">{{$row->technology_name}}</h4>
              <div class="row mb-5">
                  <div class="col-lg-6">
                      <div class="vertical-space-0"></div>
                      <img style="width:65%" class="rounded mx-auto d-block partner-img img-fluid" src="{{url('/')}}/public/images/technology/{{$row->technology_image}}" width="50%">
                  </div>
                  <div class="col-lg-6">
                      <!-- <h5>
                     <b> {{$row->technology_name}}  </b>  </h5> -->

                      <p class="main-text mt-3" style="text-align: justify;">
                      {{$row->technology_description}}  
                      <div class="vertical-space-10"></div> 
                      <a href="{{$row->technology_link}}" class="btn common-btn">Read More</a>
                     </div>
                     
              </div>
            
          </div>
          <div class="vertical-space-80"></div>
      </div>
      @endforeach
  </section>

  @endif

  
@if($technology)
 <!-- <section class="back-color">
        <div class="vertical-space-60"></div>
        <div class="technology">
            <div class="container">
              
                <div class="row">
                   @foreach($technology as $row)
                     <div class="col-lg-3">
                       <div class="card tech-box">
                          <img src="{{url('/')}}/public/images/technology/{{$row->technology_image}}" alt="...">
                          <div class="card-body">
                            <h5 class="card-title">{{$row->technology_name}}</h5>
                            <p class="main-text" style="text-align: justify;">{{$row->technology_description}}</p>
                             {{ substr(strip_tags($row->technology_description),0,150)}}..
                            <a href="#" class="btn common-btn">Read More</a>
                          </div>
                        </div>
                    </div>
                    @endforeach
                 
                    </div>
                </div>    
            
        </div>
      </section> -->
   @endif



 @endsection