@extends('layouts.app')

@section('content')
@if($banner)
<div class="banner">
    <div class="owl-carousel" id="banner-carousel">

        @foreach($banner as $row)
        <div class="item">
            <img src="{{url('/')}}/public/images/banner/{{$row->banner_image}}" alt="" />
            <div class="overlay-img "></div>
            <div class="banner-box" id="banner-box" data-aos="fade-up-right">
                <div class="vertical-space-150"></div>
                <div class="banner-box-detail">
                    <h1 class="banner-box-title">{{$row->banner_name}}</h1>
                    <p class="banner-box-subtext">{{$row->banner_description}}</p>
                    <p class=""><a href="{{$row->banner_link}}" class="btn common-btn">{{$row->banner_link_name}}</a></p>
                </div>
            </div>
        </div>
        @endforeach


    </div>
</div>
@endif
<section>
    <div class="niramai_content">
        <div class="container">
            <div class="vertical-space-80"></div>
            <div class="row ">
                <div class="col-lg-6 order-sm-2">
                    <h2 class="section-title">What is NIRAMAI ?</h2>
                    <p class="section-text">
                        NIRAMAI stands for "Non-Invasive Risk Assessment with Machine Intelligence"
                        Breast cancer is the leading cause of cancer deaths in women today. According to the World Health Organization (WHO), one in every 12 women has a risk of getting a breast abnormality in her lifetime. Early detection is key!
                        We, at NIRAMAI, have developed an AI based novel solution to detect breast cancer at a much earlier stage than traditional methods or self-examination. Niramai Breast Screening is a low cost, accurate, automated, portable cancer screening tool that can be operated in any clinic. Our imaging method is radiation free, non-touch, not painful and works for women of all ages. The core technology of our solution has been developed using our patented machine learning algorithms for reliable and accurate detection of breast cancer. This unique solution can be used as a cancer diagnosis test in hospitals, for regular preventive health checkups, and also for large scale screening in rural and semi-urban areas.
                    </p>
                </div>
                <div class="col-lg-6 order-sm-1">
                    <div class="row justify-content-center">
                        <div class="col-lg-5 col-6">
                            <div class="vertical-space-200"></div>
                            <div class="content_card">
                                <div class="">
                                    <img src="{{base_url('public/images/Non-Invasive.png')}}" width="100%" class="health-card-logo">
                                </div>
                                <div class="card-head">Non-Invasive</div>
                                <p class="content_card-text">
                                No radiation or incision involved</p>
                            </div>
                        </div>
                        <div class="col-lg-5 col-6">
                            <div class="vertical-space-80"></div>
                            <div class="content_card">
                                <div class="">
                                    <img src="{{base_url('public/images/Privacy-Sensitive.png')}}" width="100%" class="health-card-logo">
                                </div>
                                <div class="card-head">Privacy Sensitive</div>
                                <p class="content_card-text">
                                No see, no touch, no pain</p>
                            </div>
                            <div class="vertical-space-30"></div>
                            <div class="content_card">
                                <div class="">
                                    <img src="{{base_url('public/images/High-Accuracy.png')}}" width="100%" class="health-card-logo">
                                </div>
                                <div class="card-head">High Accuracy</div>
                                <p class="content_card-text">
                                Best In Class Sensitivity & Specificity</p>
                            </div>
                        </div>
                    </div>
                </div>


            </div>
        </div>
    </div>
</section>
@if($product)
<section class="mt-5">
    <div class="our-products">
        <div class="container" style="position:relative;bottom:-125px">
            <div class="vertical-space-70"></div>
            <h4 class="main-title">&nbsp;&nbsp;&nbsp;Our Products</h4>
            <!-- <p class="section-text w-75 mx-auto mt-5 text-center"> -->
                <!-- Using our expertise in Thermal Imaging and Machine Learning, we have also developed an automated solution 
                 -->
               <p class="main-text text-center"> to screen for common COVID Symptoms, with our FeverTest product and XRaySetu.</p>
            <!-- </p> -->
            
            <div class="vertical-space-80"></div>
            <div class="row justify-content-center ">
                @foreach($product as $row )
                <div class="col-lg-4">
                    
                    <div class="thumbex">
                            <div class="thumbnail"><a href="{{$row->product_link}}"> 
                            <img style="opacity: 0.7;" src="{{url('/')}}/public/images/product/{{$row->product_image}}" />
                            </a>
               </div>
                    </div>
                    <div style="position: relative;top:-42%;left:7%;" >
               <hr style="content: '';
    display: block;
    background-color: #d7355c;
    width: 70px;
    height: 4px;
    position: absolute;
    border-radius: 25px;
    bottom:2px;
    content: '';
    position: absolute;
    margin-left: 3%;
    top: -30px;
    bottom:-10px;
  ">
       <a style="color:#000;font-size:20px;font-weight:900;line-height:10px !important;" href="{{$row->product_link}}" class="main-text thumbex-text">{{$row->product_name}}</a>
     <br/>
       <p class="main-text"
       style="line-height:1.5em !important;margin-left:10px;width:80%;color:#000;text-align: justify;font-size:12px;font-weight:700;line-height:1.5rem">
       {{$row->product_description}}
       </p> <p><a href="#" style="margin-left:2%" class="btn common-btn mb-5">Read More</a></p>
                    </div>
                
         <br/><br/>      <p class="text-center" style="position: relative;top:-25%">  </p>
       </div>
                @endforeach
                </div>      </div>
        </div>
        <!-- <div class="vertical-space-5"></div> -->
    </div>
</section>
@endif


@if($awards)
<section>
    <div class="awards mt-5">
        <div class="container">
            <div class="vertical-space-40"></div>
            <h4 class="main-title">Awards And Recognition</h4>
            <div class="row" id="awards_list_id">
                @foreach($awards as $row)
                <div class="col-lg-3 mb-5 awards_list" style="display: none;">
                    <div class="award">
                        <div class="award-img">
                            <img src="{{url('/')}}/public/images/awards/{{$row->awards_image}}" width="100%" alt="" />
                        </div>
                        <div class="award-desc">
                            <div class="award-title">{{$row->awards_name}}</div>
                            <p class="award-text">{{$row->description}}</p>
                        </div>
                    </div>
                </div>
                @endforeach
                <p class="text-center"><a id="loadMore" class="btn common-btn">Load more <i class="fas fa-chevron-circle-down "></i></a></p>

                <div class="vertical-space-50"></div>
            </div>
        </div>
</section>
@endif

@if($partners)
<div class="partners">
    <div class="container">
        <div class="vertical-space-70"></div>
        <div class="main-title mb-5">Featured Partners</div>
        <div class="owl-carousel" id="categorey-carousel">
            @foreach($partners as $row)
            <div class="item">
                <p class="text-center"><img src="{{url('/')}}/public/images/partners/{{$row->partners_image}}" class="patners-images"></p>
            </div>
            <!-- <div class="item">
                <p class="text-center"><img src="{{url('/')}}/public/images/partners/{{$row->partners_image}}" class="patners-images"></p>
            </div>
            <div class="item">
                <p class="text-center"><img src="{{url('/')}}/public/images/partners/{{$row->partners_image}}" class="patners-images"></p>
            </div>
            <div class="item">
                <p class="text-center"><img src="{{url('/')}}/public/images/partners/{{$row->partners_image}}" class="patners-images"></p>
            </div>
            <div class="item">
                <p class="text-center"><img src="{{url('/')}}/public/images/partners/{{$row->partners_image}}" class="patners-images"></p>
            </div>
            <div class="item">
                <p class="text-center"><img src="{{url('/')}}/public/images/partners/{{$row->partners_image}}" class="patners-images"></p>
            </div>
            <div class="item">
                <p class="text-center"><img src="{{url('/')}}/public/images/partners/{{$row->partners_image}}" class="patners-images"></p>
            </div> -->
            @endforeach

        </div>
    </div>
</div>
@endif
@if($advisory)
<section>
    <div class="container">
        <div class="vertical-space-100"></div>
        <h4 class="main-title mb-5">&nbsp;Advisory Council</h4>
        <div class="owl-carousel" id="advisory-carousel">
            @foreach($advisory as $row)
            <div class="item">
                <div class="advisory">
                    <div class="row">
                        <div class="col-lg-5">
                            <p class="text-center mb-0"><img src="{{url('/')}}/public/images/advisory/{{$row->advisory_image}}" class="council-pic"></p>
                        </div>
                        <div class="col-lg-7">
                            <div class="council-desc-block">
                                <div class="council-name">{{$row->advisory_name}}</div>
                                <div class="council-post">
                                    {{$row->advisory_designation}}
                                </div>

                                <p class="council-desc mt-2">
                                    {{$row->description}}
                                </p>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
            @endforeach
        </div>
    </div>
</section>
@endif

@endsection

@push('scripts')
<script type="text/javascript">
    $(document).ready(function() {
        var size_li = $("#awards_list_id .awards_list").length;
        x = 4;
        $('#awards_list_id .awards_list:lt(' + x + ')').show();
        $('#loadMore').click(function() {
            $('#loadMore').hide();
            $('#awards_list_id .awards_list').show();
        });

    });

    $("#banner-carousel").owlCarousel({
        loop: true,
        margin: 10,
        nav: true,
        autoplay: true,
        autoplayTimeout: 6000,
        autoplayHoverPause: true,
        responsive: {
            0: {
                items: 1,
            },
            600: {
                items: 1,
            },
            1000: {
                items: 1,
            },
        },
    });
    $("#categorey-carousel").owlCarousel({
        loop: true,
        margin: 50,
        nav: true,
        autoplay: true,
        autoplayTimeout: 3000,
        autoplayHoverPause: true,
        responsive: {
            0: {
                items: 1,
            },
            600: {
                items: 2,
            },
            1000: {
                items: 5,
            },
        },
    });
    $("#advisory-carousel").owlCarousel({
        loop: true,
        margin: 10,
        nav: true,
        autoplay: true,
        autoplayTimeout: 6000,
        autoplayHoverPause: true,
        responsive: {
            0: {
                items: 1,
            },
            600: {
                items: 1,
            },
            1000: {
                items: 1,
            },
        },
    });
    $(".owl-prev").html('<i class="fa fa-chevron-left"></i>');
    $(".owl-next").html('<i class="fa fa-chevron-right"></i>');
</script>
@endpush