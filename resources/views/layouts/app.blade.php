 @php
    $setting = \App\Models\Setting::find(1);
 @endphp
<!DOCTYPE html>
<html lang="en">
    <head>
        <title>{{@$title}}  |  {{ $setting->company_name}}</title>
      <meta name="viewport" content="width=device-width, initial-scale=1" />
        <link rel="shortcut icon" type="image/icon" href="{{asset('public/images/fav-icon.png')}}" />
        <link rel="stylesheet" type="text/css" href="{{asset('public/css/bootstrap.min.css')}}" />
        <link rel="stylesheet" type="text/css" href="{{asset('public/css/aos.css')}}" />
        <link rel="stylesheet" type="text/css" href="{{asset('public/css/all.min.css')}}" />
        <link rel="stylesheet" type="text/css" href="{{asset('public/css/owl.carousel.min.css')}}" />
        <link rel="stylesheet" type="text/css" href="{{asset('public/css/owl.theme.default.min.css')}}" />
        <link rel="stylesheet" type="text/css" href="{{asset('public/css/magnific-popup.css')}}" />
        <link rel="stylesheet" href="{{asset('public/css/animate.css')}}">
         <link rel="stylesheet"  type="text/css" href="{{asset('public/css/camera.css')}}" >
        <link rel="stylesheet" type="text/css" href="{{asset('public/css/style.css?v=1.2')}}" />
        
    </head>
    <body>
</head>
    <body>
        
    <section>
        <div class="main-header">
            <nav class="navbar navbar-expand-lg navbar-light sticky-top">
                <div class="container">
                    <a class="navbar-brand" href="{{url('/home')}}"><img src="{{url('/')}}/public/images/logo.svg" /></a>
                    <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"></span>
                    </button>
                    <div class="collapse navbar-collapse" id="navbarNav">
                        <ul class="navbar-nav ms-auto">
                            <li class="nav-item">
                                <a class="nav-link active hover-link" href="{{base_url('home')}}">Home</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link hover-link" href="{{base_url('whatwedo')}}">What We Do</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link hover-link" href="{{base_url('technology')}}">Technology & Research</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link hover-link" href="{{base_url('partnerships')}}">Partnerships</a>
                            </li>
                            <!-- <li class="nav-item">
                                <a class="nav-link hover-link" href="{{base_url('career')}}">Careers</a>
                            </li> -->
                            <li class="nav-item dropdown">
                                <a class="nav-link hover-link" href="{{base_url('aboutus')}}">About Us</a>
                            </li>
                             <li class="nav-item dropdown">
                                <a class="nav-link hover-link" href="{{base_url('women')}}">Women</a>
                            </li>
                            <li class="nav-item dropdown">
                                <a class="nav-link hover-link" href="{{base_url('doctor')}}">Doctors</a>
                            </li>
                            <li class="nav-item ">
                                <a href="{{base_url('book_appointment')}}" class="btn common-btn">Book Appointment <i class="fas fa-long-arrow-alt-right ms-2"></i></a>
                            </li>
                        </ul>
                    </div>
                </div>
            </nav>
        </div>
    </section>
       
     @yield('content')

  <div class="vertical-space-50"></div>
        <section>
            <div class="footer">
                <div class="container">
                    <div class="vertical-space-30"></div>
                    <div class="row">
                        <div class="col-lg-3">
                            <h2 class="footer-div-title">About Niramai</h2>
                            <p>NIRAMAI Health Analytic is a Bangalore-based deep-tech startup addressing critical healthcare problems through automated solutions.
                            </p>
                        </div>
                        <div class="col-lg-3">
                            <h2 class="footer-div-title">Our Mission</h2>
                            <p>
                              Our mission is to create a Universal Cancer Screening Method that can
                              save lives.
                            </p>
                            <!-- <img src="./img/sarve.jpg" alt=""> -->
                            <p
                              style="
                                background-color: #fff;
                                padding: 1em;
                                display: inline-block;
                                font-weight: 600;
                                margin-top: 10px;
                              "
                            >
                              सर्वे संतु निरामय: |
                            </p>
                        </div>
                        <div class="col-lg-3">
                             <h2 class="footer-div-title">Menu</h2>
                             <ul class="footer-menu">
                                <li><a href="{{base_url('home')}}">Home</a></li>
                                <li><a href="{{base_url('whatwedo')}}">What We Do</a></li>
                                <li><a href="{{base_url('technology')}}">Technology & Research</a>
                                </li>
                                <li><a href="{{base_url('partnerships')}}">Partnerships</a></li>
                                <li><a href="{{base_url('career')}}">Careers</a></li>
                                <li><a href="{{base_url('about_us')}}">About Us</a></li>
                                <li><a href="{{base_url('contact')}}">Contact</a></li>
                            </ul>  
                        </div>
                        <div class="col-lg-3">
                            <div class="footer-div">

                                @if(@$footer == 1)
                                    <h2 class="footer-div-title">Important Links</h2>
                                    <nav class="footer-nav">
                                      <ul class="footer-menu">
                                        <li><a href="{{base_url('career')}}">Careers</a></li>
                                        <li><a href="{{base_url('news')}}">News</a></li>
                                        <li>
                                          <a href="{{base_url('team')}}">Team</a>
                                        </li>
                                        <li><a href="{{base_url('FAQs')}}">FAQs</a></li>
                                        <li><a href="{{base_url('niramai internal')}}">Niramai Internal</a></li>
                                      </ul>
                                    </nav>
                                    @else
                                    <h2 class="footer-div-title">Get In Touch</h2>
                                            <p>📞 {{$setting->company_mobile}}</p>
                                            <p>
                                                🏠 {{$setting->company_address}}
                                            </p>
                                            <p>📧 {{$setting->company_email}}</p>
                                    @endif
                                    <h2 class="footer-div-title" style="margin-top: 15px;">Social Links</h2>
                                    <div class="social-links-footer">
                            <ul>
                                <li>
                                    <a href="{{$setting->facbook}}"><i class="fab fa-facebook-f"></i></a>
                                </li>
                                <li>
                                    <a href="{{$setting->twitter}}"><i class="fab fa-twitter"></i></a>
                                </li>
                                <li>
                                    <a href="{{$setting->whatsapp}}"><i class="fab fa-whatsapp"></i></a>
                                </li>
                                <li>
                                    <a href="{{$setting->instrgram}}"><i class="fab fa-instagram"></i></a>
                                </li>
                                <li>
                                    <a href="{{$setting->youtube}}"><i class="fab fa-youtube"></i></a>
                                </li>
                            </ul>
                        </div>
                         </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section>
            <div class="footer-bottom">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-4">
                            <p> &copy; Copyright 2017 All Rights Reserved By Niramai</p>
                        </div>
                        <div class="col-lg-8 text-end">
                            <p><a href="{{base_url('terms_use_privacy_policy')}}">Terms of Use & Privacy</a> | <a href="{{base_url('gdpr_privacy_notes')}}"> GDRP Privacy Notice</a> | <a href="refund_and_cancellation">Refund  & Cancellation</a> </p>
                        </div>

                    </div>
                </div>
            </div>
        </section>


        <script type="text/javascript" src="{{url('/')}}/public/js/jquery-3.3.1.min.js"></script>
        <script type="text/javascript" src="{{url('/')}}/public/js/bootstrap.js"></script>
        <script type="text/javascript" src="{{url('/')}}/public/js/owl.carousel.min.js"></script>
        <script type="text/javascript" src="{{url('/')}}/public/js/jquery.magnific-popup.min.js"></script>
        <script type="text/javascript" src="{{url('/')}}/public/js/anime.min.js"></script>
        <script src="{{url('/')}}/public/js/aos.js"></script>

        @stack('scripts')
        <script type="text/javascript">
             AOS.init(); 
        </script>
        
    </body>
</html>
