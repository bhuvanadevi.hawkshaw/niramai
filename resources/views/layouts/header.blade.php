@php
    $setting = \App\Models\Setting::find(1);
 @endphp
 <!DOCTYPE html>
<html lang="en">
    <head>
       <title>{{@$title}} | {{$setting->company_name}}</title>
        <meta charset="UTF-8" />
              <meta name="viewport" content="width=device-width, initial-scale=1" />
        <link rel="shortcut icon" type="image/icon" href="{{asset('public/images/fav-icon.png')}}" />
        <link rel="stylesheet" type="text/css" href="{{asset('public/css/bootstrap.min.css')}}" />
        <link rel="stylesheet" type="text/css" href="{{asset('public/css/aos.css')}}" />
        <link rel="stylesheet" type="text/css" href="{{asset('public/css/all.min.css')}}" />
        <link rel="stylesheet" type="text/css" href="{{asset('public/css/owl.carousel.min.css')}}" />
        <link rel="stylesheet" type="text/css" href="{{asset('public/css/magnific-popup.css')}}" />
        <link rel="stylesheet" href="{{asset('public/css/animate.css')}}">
         <link rel="stylesheet"  type="text/css" href="{{asset('public/css/camera.css')}}" >
        <link rel="stylesheet" type="text/css" href="{{asset('public/css/owl.theme.default.min.css')}}" />
       <link rel="stylesheet" type="text/css" href="{{asset('public/css/style.css')}}" />
    </head>
    <body>
        
 <section>
        <div class="main-header">
            <nav class="navbar navbar-expand-lg navbar-light sticky-top">
                <div class="container">
                    <a class="navbar-brand" href="index.html"><img src="{{url('/')}}/public/images/logo.svg" /></a>
                    <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"></span>
                    </button>
                    <div class="collapse navbar-collapse" id="navbarNav">
                        <ul class="navbar-nav ms-auto">
                            <li class="nav-item">
                                <a class="nav-link active hover-link" href="{{base_url('home')}}">Home</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link hover-link" href="{{base_url('whatwedo')}}">What We Do</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link hover-link" href="{{base_url('technology')}}">Technology & Reserch</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link hover-link" href="{{base_url('partnerships')}}">Partnerships</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link hover-link" href="{{base_url('career')}}">Careers</a>
                            </li>
                            <li class="nav-item dropdown">
                                <a class="nav-link hover-link" href="{{base_url('aboutus')}}">About Us</a>
                            </li>
                            <li class="nav-item ">
                                <a href="booking.html" class="btn common-btn">Book Appoinment <i class="fas fa-long-arrow-alt-right ms-2"></i></a>
                            </li>
                        </ul>
                    </div>
                </div>
            </nav>
        </div>
    </section>

      @yield('content')
    <div class="vertical-space-60"></div>
        <section>
            <div class="footer">
                <div class="container">
                    <div class="vertical-space-30"></div>
                    <div class="row gx-2">
                        <div class="col-lg-3">
                            <h2 class="footer-div-title">Comapny Info</h2>
                            <ul class="footer-menu">
                                <li><a href="{{base_url('aboutus')}}">About Us</a></li>
                                <li><a href="{{base_url('career')}}">Career</a></li>
                                <li><a href="">We Are Hiring</a></li>
                                <li><a href="">Blog</a></li>
                                <li>
                                    <a href=""><b>Legal</b></a>
                                </li>
                            </ul>
                        </div>
                        <div class="col-lg-3">
                            <h2 class="footer-div-title">Features</h2>
                            <ul class="footer-menu">
                                <li><a href="">Business Marketing</a></li>
                                <li><a href="">User Analytic</a></li>
                                <li><a href="">Live Chat</a></li>
                                <li><a href="">Unlimited Support</a></li>
                            </ul>
                        </div>
                        <div class="col-lg-3">
                            <h2 class="footer-div-title">Resources</h2>
                            <ul class="footer-menu">
                                <li><a href="{{base_url('aboutus')}}">IOS & Andriod</a></li>
                                <li><a href="">Watch A Demor</a></li>
                                <li><a href="">Customers</a></li>
                                <li><a href="">API</a></li>
                            </ul>
                        </div>
                        <div class="col-lg-3">
                            <h2 class="footer-div-title">Get In Touch</h2>
                            <p>📞 {{$setting->company_mobile}}</p>
                            <p>
                                🏠 {{$setting->company_address}}
                            </p>
                            <p>📧 {{$setting->company_email}}</p>
                        </div>
                    </div>
                </div>
            </div>
        </section>


        <script type="text/javascript" src="{{url('/')}}/public/js/jquery-3.3.1.min.js"></script>
        <script type="text/javascript" src="{{url('/')}}/public/js/bootstrap.js"></script>
        <script type="text/javascript" src="{{url('/')}}/public/js/owl.carousel.min.js"></script>
        <script type="text/javascript" src="{{url('/')}}/public/js/jquery.magnific-popup.min.js"></script>
        <script type="text/javascript" src="{{url('/')}}/public/js/anime.min.js"></script>
        <script src="{{url('/')}}/public/js/aos.js"></script>
       
        </body>
        </html>
