-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: May 23, 2022 at 12:34 PM
-- Server version: 10.4.22-MariaDB
-- PHP Version: 7.4.27

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `niramai`
--

-- --------------------------------------------------------

--
-- Table structure for table `about_us`
--

CREATE TABLE `about_us` (
  `id` int(11) NOT NULL,
  `title` varchar(256) DEFAULT NULL,
  `content` text DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT 0,
  `created_at` datetime NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `about_us`
--

INSERT INTO `about_us` (`id`, `title`, `content`, `status`, `created_at`, `updated_at`) VALUES
(1, 'About Us', '<p><span style=\"font-family: &quot;Roboto Slab&quot;, serif; font-size: 14px; text-align: justify; background-color: rgb(247, 248, 250);\">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum ac sodales sapien. Sed pellentesque, quam in ornare tincidunt, magna augue placerat nunc, ut facilisis nibh ipsum non ipsum. Cras ac eros non neque viverra consequat sed at est. Fusce efficitur, lacus nec dignissim tincidunt, diam sapien rhoncus neque, at tristique sapien nibh sed neque. Proin in neque in purus luctus facilisis. Donec viverra ligula quis lorem viverra consequat. Aliquam condimentum id enim volutpat rutrum. Donec semper iaculis convallis. Praesent quis elit eget ligula facilisis mattis. Praesent sed euismod dui. Suspendisse imperdiet vel quam nec venenatis. Suspendisse dictum blandit quam, vitae auctor enim gravida et. Sed id dictum nibh. Proin egestas massa sit amet tincidunt aliquet.</span><br></p>', 1, '0000-00-00 00:00:00', '2022-01-22 06:11:15');

-- --------------------------------------------------------

--
-- Table structure for table `advisory`
--

CREATE TABLE `advisory` (
  `id` int(10) NOT NULL,
  `advisory_name` varchar(256) NOT NULL,
  `advisory_image` varchar(256) NOT NULL,
  `advisory_designation` varchar(256) DEFAULT NULL,
  `description` text DEFAULT NULL,
  `advisory_status` int(11) NOT NULL DEFAULT 1,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `advisory`
--

INSERT INTO `advisory` (`id`, `advisory_name`, `advisory_image`, `advisory_designation`, `description`, `advisory_status`, `created_at`, `updated_at`) VALUES
(6, 'Dr. Kiran Mazumdar-Shaw', '1651463154.jpg', 'Chairperson & Managing Director,Biocon Ltd', 'I congratulate Team Niramai for developing an innovative thermal analytics based Breast Cancer Solution for early stage breast cancer detection. This Non-Invasive Risk Assessment with Machine Intelligence, cancer screening solution is relevant for women of all ages and poses minimal health hazard since it is contact and radiation free. I believe Niramai will enable easy adoption for early cancer screening and will play a key role in improving outcomes in our ongoing battle against breast cancer. I am delighted to support this initiative.', 1, '2022-05-02 03:45:54', '2022-05-01 22:15:54'),
(9, 'Dr. H V Ram Prakash', '1643817828.jpg', 'Senior Radiologist and Expert Thermographer', 'Thermography can detect breast cancer in ways that are non-invasive, non-ionizing and non-traumatizing. In addition to detecting tumor growth earlier, this innovation can bring breast cancer screening to the doorsteps of women all over the world. I am happy to partner with Niramai on this journey.', 1, '2022-02-03 13:46:48', '2022-02-03 08:16:48'),
(10, 'Ms. Anna Maria Kinberg Batra', '1649153069.jpg', 'Prominent Leader of Sweden', 'Niramai has great opportunities to improve women’s lives and health around the world, using technology and AI.', 1, '2022-04-05 04:34:29', '2022-04-05 04:34:29');

-- --------------------------------------------------------

--
-- Table structure for table `awards`
--

CREATE TABLE `awards` (
  `id` int(10) NOT NULL,
  `awards_image` varchar(256) DEFAULT NULL,
  `awards_name` varchar(256) NOT NULL,
  `description` text DEFAULT NULL,
  `awards_status` int(11) NOT NULL DEFAULT 1,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `awards`
--

INSERT INTO `awards` (`id`, `awards_image`, `awards_name`, `description`, `awards_status`, `created_at`, `updated_at`) VALUES
(2, '1643811840.png', 'Amazon AI Conclave', 'Winner of Amazon AI Conclave 2017 Award in Health Care', 0, '2022-03-26 09:56:16', '2022-03-26 04:26:16'),
(3, '1644555297.png', 'Google Launchpad', ' Niramai is among 4 startups from India that got selected for Google Launchpad Accelerator Program 2018', 0, '2022-03-26 09:54:50', '2022-03-26 04:24:50'),
(4, '1644555409.png', 'Incub@te', 'Selected for Tata Incub@te program', 0, '2022-03-26 09:54:51', '2022-03-26 04:24:51'),
(5, '1644555485.png', 'Incub@te', 'Selected for Tata Incub@te program', 0, '2022-03-26 09:54:52', '2022-03-26 04:24:52'),
(6, '1644555527.png', 'Incub@te', 'Selected for Tata Incub@te program', 0, '2022-03-26 09:54:53', '2022-03-26 04:24:53'),
(7, '1644555567.png', 'Incub@te', 'Selected for Tata Incub@te program', 0, '2022-03-26 09:54:54', '2022-03-26 04:24:54'),
(8, '1644555606.png', 'Incub@te', 'Selected for Tata Incub@te program', 0, '2022-03-26 09:54:54', '2022-03-26 04:24:54'),
(9, '1644555662.png', 'Incub@te', 'Selected for Tata Incub@te program', 0, '2022-03-26 09:54:55', '2022-03-26 04:24:55'),
(10, '1644555720.png', 'Incub@te', 'Selected for Tata Incub@te program', 0, '2022-03-26 09:54:57', '2022-03-26 04:24:57'),
(11, '1644555744.png', 'Incub@te', 'Selected for Tata Incub@te program', 0, '2022-03-26 09:54:58', '2022-03-26 04:24:58'),
(12, '1644555770.png', 'Incub@te', 'Selected for Tata Incub@te program', 0, '2022-03-26 09:55:01', '2022-03-26 04:25:01'),
(13, '1644555834.png', 'Incub@te', 'Selected for Tata Incub@te program', 0, '2022-03-26 09:55:03', '2022-03-26 04:25:03'),
(15, '1648288786.svg', 'In Top 100', 'Only Indian startup in the  TOP 100 AI startups in the world.', 1, '2022-03-26 09:59:46', '2022-03-26 04:29:46'),
(16, '1648288658.jpg', 'European Clearance', 'Niramai received CE marking clearance for European market', 1, '2022-03-26 04:27:38', '2022-03-26 04:27:38'),
(17, '1648288943.svg', 'Winner Hack Osaka', 'Niramai secured Gold Prize in International Startup Contest', 1, '2022-03-26 04:32:23', '2022-03-26 04:32:23'),
(18, '1648289092.jpg', 'Pride Of India', 'Selected for Marico Innovation Foundation Scale-Up program and tagged as Pride of India.', 1, '2022-03-26 04:34:52', '2022-03-26 04:34:52'),
(19, '1648290263.svg', 'Selected for nVIDIA Inception', 'Niramai is a part of nVIDIA Inception Startup program', 1, '2022-03-26 04:54:23', '2022-03-26 04:54:23'),
(20, '1648290472.png', 'Accenture AI Award', 'Winners of Accenture Applied Intelligence AI Award 2019', 1, '2022-03-26 04:57:52', '2022-03-26 04:57:52'),
(21, '1648291264.png', 'National Startup Award', 'Winner of National Startup Award 2020 in Healthcare Diagnostics', 1, '2022-03-26 10:41:04', '2022-03-26 05:11:04'),
(22, '1648290692.png', 'AEGIS GRAHAM Bell Award 2018', 'Niramai was awarded Aegis Graham Bell Award 2018 under Innovative Mobile App for Consumer', 1, '2022-03-26 05:01:32', '2022-03-26 05:01:32'),
(23, '1648291079.png', 'Amazon AI Conclave', 'Winner of Amazon AI Conclave 2017 Award in Health Care', 1, '2022-03-26 10:37:59', '2022-03-26 05:07:59'),
(24, '1648291581.jpg', 'Google Launchpad', 'Niramai is among 4 startups from India that got selected for Google Launchpad Accelerator program 2018', 1, '2022-03-26 05:16:21', '2022-03-26 05:16:21'),
(25, '1648291759.png', 'AEGIS GRAHAM BELL AWARD 2017', 'Niramai was awarded Aegis Graham Bell Award 2017 under Data Science', 1, '2022-03-26 05:19:19', '2022-03-26 05:19:19'),
(26, '1648291820.png', 'Call4Winner', 'Niramai won the international contest Open-F@b promoted by BNB Paribas Cardif', 1, '2022-03-26 05:20:20', '2022-03-26 05:20:20'),
(27, '1648292016.png', 'SmartCEO', 'Niramai in SMartCEO top 50 startups.', 1, '2022-03-26 10:53:36', '2022-03-26 05:23:36'),
(28, NULL, 'Tech30', 'Niramai in Tech30 - Top 30 innovations in 2017.', 1, '2022-03-26 11:33:05', '2022-03-26 05:41:03'),
(29, NULL, 'Philips Healthworks', 'Niramai in Philips Healthworks program in India', 1, '2022-03-26 11:32:59', '2022-03-26 05:44:13'),
(30, NULL, 'Elevate 100', 'Selected among 100 top technology startups across Karnataka', 1, '2022-03-26 05:48:26', '2022-03-26 05:48:26'),
(31, '1648293615.png', 'Startupindia', 'Recognised by startup India', 1, '2022-03-26 05:50:15', '2022-03-26 05:50:15'),
(32, '1648293722.svg', 'Tracxn', 'Startup of the week @ Tracxn', 1, '2022-03-26 05:52:02', '2022-03-26 05:52:02'),
(33, '1648293897.png', 'Axilor', 'Best startup @ Axilor Summer ’17', 1, '2022-03-26 05:54:57', '2022-03-26 05:54:57'),
(34, '1648294049.png', 'Berlin Startup Night', 'Selected and participated in Startup Night @ Berlin ’17', 1, '2022-03-26 05:57:29', '2022-03-26 05:57:29'),
(35, '1648294107.jpg', 'Nasscom', 'Selected in nasscom 10k startups', 1, '2022-03-26 05:58:27', '2022-03-26 05:58:27'),
(36, '1648294604.png', 'Zone Startup', 'Selected for Zone Startup', 1, '2022-03-26 11:36:44', '2022-03-26 06:06:44'),
(37, '1648294237.png', 'TATA ELXSI', 'Selected for Tata Incubate Program', 1, '2022-03-26 06:00:37', '2022-03-26 06:00:37');

-- --------------------------------------------------------

--
-- Table structure for table `banner`
--

CREATE TABLE `banner` (
  `id` int(11) NOT NULL,
  `banner_name` varchar(250) NOT NULL,
  `banner_image` varchar(250) NOT NULL,
  `banner_description` varchar(250) DEFAULT NULL,
  `banner_link` varchar(256) DEFAULT NULL,
  `banner_link_name` varchar(256) NOT NULL,
  `banner_status` int(11) DEFAULT 1,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf16;

--
-- Dumping data for table `banner`
--

INSERT INTO `banner` (`id`, `banner_name`, `banner_image`, `banner_description`, `banner_link`, `banner_link_name`, `banner_status`, `created_at`, `updated_at`) VALUES
(26, 'Thermalytix', '1647083593.png', 'A Novel Breast Cancer Screening Solution', 'technology', 'Thermalytix', 1, '2022-03-24 12:41:41', '2022-03-24 07:11:41'),
(27, 'Privacy Aware Screening', '1647084754.png', 'No pain, no radiation, no touch, no see method of screening breast health', 'https://www.niramai.com/book-appointment/', 'Book Appointment', 1, '2022-03-12 11:37:15', '2022-03-12 06:07:15'),
(28, 'Clinically proven and Regulatory certified', '1647084889.png', 'With International clinical publications', 'https://hashref.org/n/tech-research.html', 'Publications', 1, '2022-03-24 18:10:13', '2022-03-24 12:40:13'),
(29, 'Join our mission', '1647085025.png', 'Together we can save many lives', 'book_appointment', 'Partner with us', 1, '2022-03-24 12:49:39', '2022-03-24 07:19:39'),
(30, 'FeverTest', '1647085186.png', 'Passive Screening for Covid Symptoms', 'book_appointment', 'Buy Now', 1, '2022-03-24 12:49:48', '2022-03-24 07:19:48');

-- --------------------------------------------------------

--
-- Table structure for table `book_appointment`
--

CREATE TABLE `book_appointment` (
  `id` int(11) NOT NULL,
  `book_appointment_image` varchar(256) NOT NULL,
  `book_appointment_name` varchar(256) NOT NULL,
  `book_appointment_description` text NOT NULL,
  `book_appointment_status` int(10) NOT NULL DEFAULT 1,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `book_appointment`
--

INSERT INTO `book_appointment` (`id`, `book_appointment_image`, `book_appointment_name`, `book_appointment_description`, `book_appointment_status`, `created_at`, `updated_at`) VALUES
(1, '1644658826.png', 'Home Screening', 'It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout.', 1, '2022-02-12 10:40:39', '2022-02-12 05:10:39'),
(2, '1644662308.jfif', 'Hospital Screening', 'It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout.', 1, '2022-02-12 05:08:28', '2022-02-12 05:08:28'),
(3, '1644662423.jfif', 'Online Risk Test', 'It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout.', 1, '2022-02-12 05:10:23', '2022-02-12 05:10:23'),
(4, '1644662552.jfif', 'Organize Screening Camp', 'It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout.', 1, '2022-02-12 05:12:32', '2022-02-12 05:12:32');

-- --------------------------------------------------------

--
-- Table structure for table `career`
--

CREATE TABLE `career` (
  `id` int(10) NOT NULL,
  `career_image` varchar(256) DEFAULT NULL,
  `career_name` varchar(256) DEFAULT NULL,
  `career_job_type` varchar(256) NOT NULL,
  `career_location` varchar(256) NOT NULL,
  `career_description` text NOT NULL,
  `career_content` text NOT NULL,
  `career_category` varchar(256) NOT NULL,
  `career_status` int(11) DEFAULT 1,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `career`
--

INSERT INTO `career` (`id`, `career_image`, `career_name`, `career_job_type`, `career_location`, `career_description`, `career_content`, `career_category`, `career_status`, `created_at`, `updated_at`) VALUES
(1, '1644564566.svg', 'Technicians – Public Health', 'Full Time', 'Punjab, Trivendrum', 'We are hiring Image technicians for Screening operations – Female candidates ONLY.', 'https://www.niramai.com/career/technicians-public-health/', 'Technicians', 1, '2022-02-11 18:30:00', '2022-03-22 02:17:45'),
(2, '1644571628.svg', 'Machine Learning Engineer', 'Full Time', 'Banglore', 'We are inviting applications from Machine Learning (ML) engineers to work on an exciting health tech projects at NIRAMAI. If you have good problem solving skills, a passion for machine learning research and creating social impact, this is the right place for you.', '<p><font color=\"#505050\" face=\"Poppins, sans-serif\"><span style=\"font-size: 14px; letter-spacing: 1px;\">https://www.niramai.com/career/machine-learning-engineer/</span></font><br></p>', 'Engineer', 1, '2022-02-11 03:57:08', '2022-03-22 02:19:22'),
(3, '1647935463.svg', 'Research Associate', 'Full Time', 'Bangalore', 'We are inviting applications from Research Associates to work on an exciting health tech projects at NIRAMAI. If you have good problem solving skills, a passion for machine learning research and creating social impact, this is the right place for you.', '<p>https://www.niramai.com/career/research-associate/<br></p>', 'Research', 1, '2022-03-22 02:21:03', '2022-03-22 02:21:03'),
(4, '1647935510.svg', 'Data Scientist', 'Full Time', 'Bangalore', 'We are inviting applications from Data Scientists to work on an exciting health tech projects at NIRAMAI. If you have good problem solving skills, a passion for machine learning research and creating social impact, this is the right place for you.', '<p>https://www.niramai.com/career/data-scientist/<br></p>', 'Data Scientist', 1, '2022-03-22 02:21:50', '2022-03-22 02:21:50'),
(5, '1647935561.svg', 'Business Development Manager – International Business', 'Full Time', 'Bangalore', 'NIRAMAI Health Analytix is looking for a dynamic, entrepreneurial Leader to spearhead development & growth of its International Business. The incumbent will lead the International business development strategy, cultivate relationships & manage KOLs within the medical, regulatory & business ecosystem across US, Europe, Africa and Asia.', '<p>https://www.niramai.com/career/business-development-manager-international-business/<br></p>', 'Business Development Manager – International Business', 1, '2022-03-22 02:22:41', '2022-03-22 02:22:41'),
(6, '1647935616.svg', 'Frontend Developer', 'Full Time', 'Bangalore', 'Niramai is looking for 2+ years experienced Front end Angular Developers responsible for developing the UI for web apps. Your primary focus will be to create interactive and involved user interfaces for mobile and cloud based medical/technical web applications, with a focus on performance. Your main duties will include creating modules and components and coupling them together into a functional app. You will work in a team with the back-end developer, and communicate with the […]', '<p>https://www.niramai.com/career/frontend-developer/<br></p>', 'Frontend Developer', 1, '2022-03-22 02:23:36', '2022-03-22 02:23:36');

-- --------------------------------------------------------

--
-- Table structure for table `category`
--

CREATE TABLE `category` (
  `id` int(11) NOT NULL,
  `category_name` varchar(256) NOT NULL,
  `category_image` varchar(256) NOT NULL,
  `category_slug` varchar(256) NOT NULL,
  `category_status` int(11) NOT NULL DEFAULT 1,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `partners`
--

CREATE TABLE `partners` (
  `id` int(10) NOT NULL,
  `partners_image` varchar(256) NOT NULL,
  `partners_status` int(11) NOT NULL DEFAULT 1,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `partners`
--

INSERT INTO `partners` (`id`, `partners_image`, `partners_status`, `created_at`, `updated_at`) VALUES
(3, '1647239626.png', 1, '2022-03-14 06:33:46', '2022-03-14 01:03:46'),
(4, '1647239637.jpg', 1, '2022-03-14 06:33:57', '2022-03-14 01:03:57'),
(5, '1647239647.png', 1, '2022-03-14 06:34:07', '2022-03-14 01:04:07'),
(6, '1647421397.jpeg', 1, '2022-03-16 09:03:17', '2022-03-16 03:33:17'),
(7, '1647239665.jpg', 1, '2022-03-14 06:34:25', '2022-03-14 01:04:25');

-- --------------------------------------------------------

--
-- Table structure for table `partnerships`
--

CREATE TABLE `partnerships` (
  `id` int(10) NOT NULL,
  `partnerships_image` varchar(256) NOT NULL,
  `partnerships_name` varchar(256) NOT NULL,
  `partnerships_description` text NOT NULL,
  `partnerships_status` int(10) DEFAULT 1,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `partnerships`
--

INSERT INTO `partnerships` (`id`, `partnerships_image`, `partnerships_name`, `partnerships_description`, `partnerships_status`, `created_at`, `updated_at`) VALUES
(1, '1649405229.png', 'For Hospitals', 'We Have A Strong Existing Network Of Hospital-Partners. We Are Keen To Partner With Additional Hospitals Pan-India To Offer Our Clinically Proven, AI-Based Screening Solution For Early Detection Of Breast Cancer, For Screenings Both Inside The Hospital And At Their Client Locations. Please Get In Touch With Us For More Information', 1, '2022-04-08 08:07:09', '2022-04-08 02:37:09'),
(2, '1649405248.png', 'For Doctors', 'We invite expressions of interest from potential channel partners who wish to offer our novel screening solution to their valued healthcare clients. Please get in touch with us for more information', 1, '2022-04-08 08:07:28', '2022-04-08 02:37:28'),
(3, '1649405268.png', 'For Distributors', 'We invite expressions of interest from potential channel partners who wish to offer our novel screening solution to their valued healthcare clients. Please get in touch with us for more information', 1, '2022-04-08 08:07:48', '2022-04-08 02:37:48'),
(4, '1649405281.png', 'For Screening Partners', 'We welcome interest from NGOs and potential community-screening partners for tie-ups for providing extensive screenings to large communities across the country.Please get in touch with us for more information', 1, '2022-04-08 08:08:01', '2022-04-08 02:38:01'),
(5, '1649405290.png', 'For Sponsors', 'Screening the community for early detection of Breast Cancer is an extremely noble activity. We wholeheartedly welcome organisations and individuals interested in sponsoring such a noble initiative and truly appreciate their support. Please get in touch with us for more information', 1, '2022-04-08 08:08:10', '2022-04-08 02:38:10');

-- --------------------------------------------------------

--
-- Table structure for table `product`
--

CREATE TABLE `product` (
  `id` int(10) NOT NULL,
  `product_image` varchar(256) DEFAULT NULL,
  `product_name` varchar(256) DEFAULT NULL,
  `product_status` varchar(256) NOT NULL DEFAULT '1',
  `product_description` varchar(500) DEFAULT NULL,
  `product_link` varchar(200) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `updated_at` timestamp NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `product`
--

INSERT INTO `product` (`id`, `product_image`, `product_name`, `product_status`, `product_description`, `product_link`, `created_at`, `updated_at`) VALUES
(5, '1644557641.png', 'Breast Health', '1', 'A no see, no touch, no pain, radiation free breast cancer screening.', 'http://localhost/nirmai/whatwedo#breasthealthtest', '2022-03-14 06:03:49', '2022-03-14 00:33:48'),
(6, '1647094444.jpeg', 'Fever Test', '1', 'AI Based temperature screening', 'http://localhost/nirmai/whatwedo#fevertest', '2022-03-24 13:04:08', '2022-03-24 07:34:08'),
(7, '1647094453.jpg', 'Risk Assessment', '1', 'A free online risk assessment tool to check breast cancer risk', 'http://localhost/nirmai/whatwedo#onlineriskassessment', '2022-03-24 13:04:18', '2022-03-24 07:34:18');

-- --------------------------------------------------------

--
-- Table structure for table `setting`
--

CREATE TABLE `setting` (
  `id` int(11) NOT NULL,
  `company_name` varchar(255) CHARACTER SET utf8 NOT NULL,
  `image_name` varchar(255) NOT NULL,
  `image_name2` varchar(255) DEFAULT NULL,
  `company_fav` varchar(255) DEFAULT NULL,
  `company_address` text CHARACTER SET utf8 NOT NULL,
  `company_mobile` varchar(256) NOT NULL,
  `company_email` varchar(255) DEFAULT NULL,
  `currency` varchar(25) CHARACTER SET utf8 DEFAULT NULL,
  `facbook` varchar(100) CHARACTER SET utf8 DEFAULT NULL,
  `twitter` varchar(100) CHARACTER SET utf8 DEFAULT NULL,
  `google_plus` varchar(100) CHARACTER SET utf8 DEFAULT NULL,
  `instrgram` varchar(100) CHARACTER SET utf8 DEFAULT NULL,
  `youtube` varchar(500) DEFAULT NULL,
  `meta_title` varchar(100) CHARACTER SET utf8 DEFAULT NULL,
  `meta_description` text CHARACTER SET utf8 DEFAULT NULL,
  `meta_keyword` text CHARACTER SET utf8 DEFAULT NULL,
  `header_script` text CHARACTER SET utf8 DEFAULT NULL,
  `footer_script` text CHARACTER SET utf8 DEFAULT NULL,
  `map_location` text DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `setting`
--

INSERT INTO `setting` (`id`, `company_name`, `image_name`, `image_name2`, `company_fav`, `company_address`, `company_mobile`, `company_email`, `currency`, `facbook`, `twitter`, `google_plus`, `instrgram`, `youtube`, `meta_title`, `meta_description`, `meta_keyword`, `header_script`, `footer_script`, `map_location`, `created_at`, `updated_at`) VALUES
(1, 'Niramai Health Analytix Pvt Ltd', 'company-logo-1643870698.svg', 'company-logo2-1643870698.svg', 'fav-icon-1643870698.svg', 'Ground Floor, Innova Pearl, No. 17, 5th Block, Industry Layout, Bangalore, 560095', '+91 86609 22978', 'Contact@Niramai.Com', 'Rs', 'https://www.facebook.com/niramaihealthanalytix', 'https://twitter.com/niramaianalytix', 'https://www.whatsapp.com/', 'https://www.instagram.com/niramaihealthanalytix/', 'https://www.youtube.com/c/NiramaiAnalytix', 'Meta Title', 'test -1', 'test', '', '', '<iframe id=\"map_o\" width=\"100%\" height=\"300px\" src=\"https://maps.google.com/maps?q=9%2F1877%2C%20kailash%20nagar%2C%20street%20number%202%2C%20new%20delhi%20-%20110031%2C%20india&amp;t=&amp;z=13&amp;ie=UTF8&amp;iwloc=&amp;output=embed\" frameborder=\"0\" scrolling=\"no\" marginheight=\"0\" marginwidth=\"0\"></iframe>', NULL, '2022-04-12 23:48:38');

-- --------------------------------------------------------

--
-- Table structure for table `subcategory`
--

CREATE TABLE `subcategory` (
  `id` int(10) NOT NULL,
  `category_id` int(11) DEFAULT NULL,
  `subcategory_name` varchar(256) NOT NULL,
  `subcategory_image` varchar(256) NOT NULL,
  `subcategory_slug` varchar(256) DEFAULT NULL,
  `subcategory_status` int(10) NOT NULL DEFAULT 1,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `subcategory`
--

INSERT INTO `subcategory` (`id`, `category_id`, `subcategory_name`, `subcategory_image`, `subcategory_slug`, `subcategory_status`, `created_at`, `updated_at`) VALUES
(1, 3, 'BLACK BLOCKS', '1642834001.png', 'black-blocks', 1, '2022-01-22 06:46:41', '2022-01-22 01:16:41'),
(2, 3, 'RED BLOCKS', '1642849957.png', 'red-blocks', 1, '2022-01-22 11:12:37', '2022-01-22 05:42:37'),
(5, 4, 'STEEL ROOF', '1642849979.png', 'steel-roof', 1, '2022-01-22 17:23:23', '2022-01-22 11:53:23');

-- --------------------------------------------------------

--
-- Table structure for table `technology`
--

CREATE TABLE `technology` (
  `id` int(10) NOT NULL,
  `technology_image` varchar(256) NOT NULL,
  `technology_name` varchar(256) NOT NULL,
  `technology_description` text NOT NULL,
  `technology_link` varchar(256) NOT NULL,
  `technology_status` varchar(256) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `updated_at` timestamp NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `technology`
--

INSERT INTO `technology` (`id`, `technology_image`, `technology_name`, `technology_description`, `technology_link`, `technology_status`, `created_at`, `updated_at`) VALUES
(1, '1644575437.png', 'Thermalytix', 'The core of  NIRAMAI solution is Thermalytix, a computer aided diagnostic engine that is powered by Artificial Intelligence. The solution uses a high resolution thermal sensing device and a cloud hosted analytics solution for analyzing the thermal images. Our SaaS solution has been developed using big data analytics, artificial intelligence and machine learning for reliable, early and accurate breast cancer screening.', '#', '1', '2022-03-31 08:15:02', '2022-03-31 02:45:02'),
(2, '1644578601.png', 'Clinical Studies', 'Our cancer screening tool has been tested on more than 43000 women in 120+ hospitals/diagnostic centers as well as 2000+ screening camps. Results of multiple clinical studies comparing Thermalytix with current standard of care have been published in peer reviewed conferences/journals.', 'doctor#clinical_validation', '1', '2022-03-22 07:12:13', '2022-03-22 01:42:13'),
(3, '1644578671.png', 'Patents And Publications', 'Niramai has been granted more than 24 Indian & International patents.', 'patents', '1', '2022-04-06 07:15:18', '2022-04-06 01:45:18'),
(4, '1644578728.png', 'Regulatory Clearances', 'Thermalytix is also regulatory cleared in India and has obtained a CE Mark. Our Quality Management Processes are certified to be compliant with ISO 13485. We follow good clinical practices, comply with information security and privacy policies.', '#', '1', '2022-03-14 11:48:27', '2022-03-14 06:18:27');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `mobile_no` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `dob` date DEFAULT NULL,
  `city` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `gender` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`, `mobile_no`, `dob`, `city`, `gender`) VALUES
(1, 'Niramai', 'contact@niramai.com', NULL, '$2y$10$QZslzZV6S.J6EOAfC9DmXup6U7XB99mEi/2MMXGDC.1ydaQEE6tQ2', 'A3inmLBOqXxOMzdaWOpO92wigsi1n6Xgi33ermx6XgrmVz6pYcYt3gDCvDnr', '2020-10-13 21:18:55', '2022-04-05 04:03:19', '918660922978', '2021-01-27', 'Dharmapuri', '1');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `about_us`
--
ALTER TABLE `about_us`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `advisory`
--
ALTER TABLE `advisory`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `awards`
--
ALTER TABLE `awards`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `banner`
--
ALTER TABLE `banner`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `book_appointment`
--
ALTER TABLE `book_appointment`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `career`
--
ALTER TABLE `career`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `category`
--
ALTER TABLE `category`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `partners`
--
ALTER TABLE `partners`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `partnerships`
--
ALTER TABLE `partnerships`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `product`
--
ALTER TABLE `product`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `setting`
--
ALTER TABLE `setting`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `subcategory`
--
ALTER TABLE `subcategory`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `technology`
--
ALTER TABLE `technology`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `about_us`
--
ALTER TABLE `about_us`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `advisory`
--
ALTER TABLE `advisory`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `awards`
--
ALTER TABLE `awards`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=38;

--
-- AUTO_INCREMENT for table `banner`
--
ALTER TABLE `banner`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;

--
-- AUTO_INCREMENT for table `book_appointment`
--
ALTER TABLE `book_appointment`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `career`
--
ALTER TABLE `career`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `category`
--
ALTER TABLE `category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `partners`
--
ALTER TABLE `partners`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `partnerships`
--
ALTER TABLE `partnerships`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `product`
--
ALTER TABLE `product`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `subcategory`
--
ALTER TABLE `subcategory`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `technology`
--
ALTER TABLE `technology`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
