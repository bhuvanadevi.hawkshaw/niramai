<?php
  
function changeDateFormate($date,$date_format){
    return \Carbon\Carbon::createFromFormat('Y-m-d', $date)->format($date_format);    
}

function base_url($path="")
{
    return url("/")."/".$path;
}


function productImagePath($image_name)
{
    return public_path('images/products/'.$image_name);
}

function pr($data)
{
	echo "<pre>"; print_r($data); echo "</pre>";
}


function time_elapsed_string($datetime, $full = false) {
    $now = new DateTime;
    $ago = new DateTime($datetime);
    $diff = $now->diff($ago);

    $diff->w = floor($diff->d / 7);
    $diff->d -= $diff->w * 7;

    $string = array(
        'y' => 'year',
        'm' => 'month',
        'w' => 'week',
        'd' => 'day',
        'h' => 'hour',
        'i' => 'minute',
        's' => 'second',
    );
    foreach ($string as $k => &$v) {
        if ($diff->$k) {
            $v = $diff->$k . ' ' . $v . ($diff->$k > 1 ? 's' : '');
        } else {
            unset($string[$k]);
        }
    }

    if (!$full) $string = array_slice($string, 0, 1);
    return $string ? implode(', ', $string) . ' ago' : 'just now';
}

function get_price($amount, $symbol = false) {
	return "₹".$amount;
}

function get_main_category($id){
    if($id ==1 ) $text = "Repairs and Servicing";
    if($id ==2 ) $text = "Business";
    if($id ==3 ) $text = "Buy";
    return $text;
}

function get_main_category_slug($id){
    if($id ==1 ) $text = "services";
    if($id ==2 ) $text = "business";
    if($id ==3 ) $text = "buy";
    return $text;
}

function get_days($arra_day,$array=0){
    $days = array (
        1 => 'Sun',
        2 => 'Mon', 
        3 => 'Tue',
        4 => 'Wed',
        5 => 'Thu',
        6 => 'Fri',
        7 => 'Sat'
    );
     $new_days = array();
   // pr($arra_day);die;
    if($arra_day){  
       
        foreach ($arra_day as $key => $value) {
           $new_days[] = @$days[$value];
        }  
    }
    if($array ==1 ) return $new_days;
    else return implode(",",$new_days);
}

function get_all_childrens($parent_id, $grand_child= false,$level_2,$new_no="") {

    // echo "parent-".$parent_id."<br>";

    $html="";
    $level3 = App\Models\Level_3::where('parent_id',$parent_id)->where('level_2',$level_2)->get();;
        if($level3->count() > 0){
        echo '<ul class="id--'.$parent_id.'">';

         $class = "level3";
        
        foreach($level3 as $row) {

              $theme_data = $row->theme_data  ;
     
            if($theme_data == 2) $class = "white";
            if($theme_data == 3) $class = "gray";
            echo '<li><a href="#" class="'.$class.'" >'.$row->name.'</a>';
            if($row->description){
                echo '<ul><li><a href="#" class="map-desc">'.$row->description.'</a>';
                get_all_childrens($row->id, ($grand_child + 1),$level_2,$new_no++);
                   echo  '</li></ul></li>';
            }
            else{
                get_all_childrens($row->id, ($grand_child + 1),$level_2,$new_no++);
            }

            
           
        }
          echo  '</ul>';
      }  
}

function get_all_childrens2($parent_id, $grand_child= false,$level_2,$new_no="") {

    // echo "parent-".$parent_id."<br>";

    $html="";
    $level3 = App\Models\Level_3::where('parent_id',$parent_id)->where('level_2',$level_2)->get();;
        if($level3->count() > 0){
        echo '<ul class="id--'.$parent_id.'">';
         foreach($level3 as $row) {
           echo '<li><a href="#" class="level3">'.$row->name.'</a><ul><li><a href="#" class="map-desc">'.$row->description.'</a>';
              get_all_childrens($row->id, ($grand_child + 1),$level_2,$new_no++);
              echo  '</li></ul></li>';
            }
          echo  '</ul>';
      }  
}


