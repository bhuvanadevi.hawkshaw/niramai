<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Banner;
use App\Models\Product;
use App\Models\Advisory;
use App\Models\Awards;
use App\Models\About_us;
use App\Models\Partners;
use App\Models\Career;
use App\Models\Technology;
use App\Models\Partnerships;
use App\Models\Book_Appointment;
use App\Models\Setting;
use LDAP\Result;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $title =  "Home";
        $banner = Banner::where('banner_status',1)->get();
        $product = Product::where('product_status',1)->get();
        $awards = Awards::where('awards_status',1)->get();
        $partners = Partners::where('partners_status',1)->get();
        $advisory = Advisory::where('advisory_status',1)->get();
        $about_us = About_us::find(1);
        $footer = 1;
        return view('frontend.home',compact('title','banner','product','awards','partners','advisory','footer'));
    }

    public function career()
    {
        $career = Career::where('career_status',1)->get();
        $title =  "Career";
        return view('frontend.career',compact('title','career'));
    }

     public function women()
    {
        $title =  "For Women";
        return view('frontend.women',compact('title'));
    }


   public function doctor()
    {
        $title =  "For Doctors";
        return view('frontend.doctor',compact('title'));
    }

    public function study1()
    {
        $title =  "Study 1";
        return view('frontend.study1',compact('title'));
    }

    
    public function study2()
    {
        $title =  "Study 2";
        return view('frontend.study2',compact('title'));
    }

    
    public function study3()
    {
        $title =  "Study 3";
        return view('frontend.study3',compact('title'));
    }

    
    public function study4()
    {
        $title =  "Study 4";
        return view('frontend.study4',compact('title'));
    }

    public function whatwedo(){
        $title =  "What We Do";
        return view('frontend.whatwedo',compact('title'));
    }

   public function Partnerships()
    {
        $partnerships = Partnerships::where('partnerships_status',1)->get();
        $title =  "Partnerships";
        return view('frontend.partnerships',compact('title','partnerships'));
    }
     public function aboutus(){
        $title =  "About Us";
        return view('frontend.aboutus',compact('title'));
    }
    public function technology()
    {
        $technology = Technology::where('technology_status',1)->get();
        $title =  "Technology";
        return view('frontend.technology',compact('title','technology'));
    }

  public function Book_Appointment()
    {
        $book_appointment = Book_Appointment::where('book_appointment_status',1)->get();
        $title =  "Book_Appoinment";
        return view('frontend.book_appointment',compact('title','book_appointment'));
    }
    public function fevertest(){
        $title =  "Fever Test";
        return view('frontend.fevertest',compact('title'));
    }
    
    public function patents(){
        $title =  "Patents";
        return view('frontend.patents',compact('title'));
    }
    public function contact(){
        $title =  "Contact";
        $result = Setting::first();

        return view('frontend.contact', compact('title','result'));
    
    //    return view('frontend.contact',compact('title'));
    }

    public function terms_use_privacy_policy(){
        $title =  "Terms Of Use Privacy Policy";
        return view('frontend.terms_use_privacy_policy',compact('title'));
    }

    public function gdpr_privacy_notes(){
        $title =  "GDPR Privacy Notes";
        return view('frontend.gdpr_privacy_notes',compact('title'));
    }

    public function refund_and_cancellation(){
        $title =  "Refund & Cancellation";
        return view('frontend.refund_and_cancellation',compact('title'));
    }

}