<?php
namespace App\Http\Controllers\Admin;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\Partnerships;
use File;
use Session;
use Hash;
class PartnershipsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $results = Partnerships::all();
        $title = "Partnerships";
        return view('admin.partnerships.index', compact('title','results'));
    }

    public function create()
    {
        $title = "Add Partnerships";
        return view('admin.partnerships.create', compact('title'));
    }

    public function store(Request $request)
    {       
        
        $this->validate($request, [
            'partnerships_name'        => 'required'
        ]);
        $data = $request->all();
        // dd($request->all());
        if ($request->hasFile('partnerships_image')) {
                $image = $request->file('partnerships_image');
                $partnerships_image = time().'.'.$image->getClientOriginalExtension();
                $destinationPath = public_path('images/partnerships');
                $image->move($destinationPath, $partnerships_image);
                $data['partnerships_image'] = $partnerships_image;
        };
   
        $partnerships = new Partnerships;           
        //value pass above this line in controller
        $partnerships->create($data);
  
    
        // 
        Session::flash('message', 'Successfully Saved.');
        return redirect('admin/partnerships');
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        $result = Partnerships::find($id);
         $title = "Edit Partnerships";
        return view('admin/partnerships.edit', compact('title','result', 'id'));
    }

    public function update(Request $request, $id)
    {

        
        $this->validate($request, [
            'partnerships_name'        => 'required',

        ]);

        $data = $request->all();
        $partnerships_image = "";
        if ($request->hasFile('partnerships_image')) {
                $image = $request->file('partnerships_image');
                $partnerships_image = time().'.'.$image->getClientOriginalExtension();
                $destinationPath = public_path('images/partnerships');
                // dd($destinationPath);
                $image->move($destinationPath, $partnerships_image);
                $data['partnerships_image'] = $partnerships_image;
        }

           $partnerships = Partnerships::find($id);          
        $partnerships->update($data);
     
        Session::flash('message', 'Successfully Saved.');
        return redirect('admin/partnerships');
    }

    public function destroy($id)
    {
        $res=Partnerships::find($id)->delete();
        Session::flash('message', 'Successfully Deleted.');
        return redirect('admin/partnerships');
    }

    
    public function status($id,$status)
    {   
        $partnerships = Partnerships::find($id);
        $partnerships->partnerships_status = $status;
        $partnerships->save();

    }

}