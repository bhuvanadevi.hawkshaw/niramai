<?php
namespace App\Http\Controllers\Admin;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\Career;
use File;
use Session;
use Hash;
class CareerController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $results = Career::all();
        $title = "Career";
        return view('admin.career.index', compact('title','results'));
    }

    public function create()
    {
        $title = "Add Career";
        return view('admin.career.create', compact('title'));
    }

    public function store(Request $request)
    {       
        
        $this->validate($request, [
            'career_name'        => 'required'
        ]);
        $data = $request->all();
        // dd($request->all());
        if ($request->hasFile('career_image')) {
                $image = $request->file('career_image');
                $career_image = time().'.'.$image->getClientOriginalExtension();
                $destinationPath = public_path('images/career');
                $image->move($destinationPath, $career_image);
                $data['career_image'] = $career_image;
        };
   
        $career = new Career;           
        //value pass above this line in controller
        $career->create($data);
  
    
        // 
        Session::flash('message', 'Successfully Saved.');
        return redirect('admin/career');
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        $result = Career::find($id);
         $title = "Edit Career";
        return view('admin/career.edit', compact('title','result', 'id'));
    }

    public function update(Request $request, $id)
    {

        
        $this->validate($request, [
            'career_name'        => 'required',

        ]);

        $data = $request->all();
        $career_image = "";
        if ($request->hasFile('career_image')) {
                $image = $request->file('career_image');
                $Career_image = time().'.'.$image->getClientOriginalExtension();
                $destinationPath = public_path('images/career');
                // dd($destinationPath);
                $image->move($destinationPath, $career_image);
                $data['career_image'] = $career_image;
        }

          $career = Career::find($id);          
        $career->update($data);
     
        Session::flash('message', 'Successfully Saved.');
        return redirect('admin/career');
    }

    public function destroy($id)
    {
        $res=Career::find($id)->delete();
        Session::flash('message', 'Successfully Deleted.');
        return redirect('admin/career');
    }

    
    public function status($id,$status)
    {   
        $career = Career::find($id);
        $career->career_status = $status;
        $career->save();

    }

}