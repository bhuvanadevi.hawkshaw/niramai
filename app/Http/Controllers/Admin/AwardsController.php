<?php
namespace App\Http\Controllers\Admin;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\Awards;
use File;
use Session;
use Hash;
use Str;
class AwardsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $title = "Awards";
        $results = Awards::all();
        return view('admin.awards.index', compact('title','results'));
    }

    public function create()
    {
        $title = "Add Awards";
        //$subcategory = Subcategory::all();
        return view('admin.awards.create', compact('title'));
    }

    public function store(Request $request)
    {       
         $this->validate($request, [
            'awards_name'        => 'required',
        ]);
          //$data = $request->all();
        // dd($request->all());

        $data = array(
                          'awards_image'           => $request->awards_image,
                          'awards_name'           => $request->awards_name,
                          'description'  => $request->description
                        
                        
                    );
        

        if ($request->hasFile('awards_image')) {
                $image = $request->file('awards_image');
                $awards_image = time().'.'.$image->getClientOriginalExtension();
                $destinationPath = public_path('images/awards');
                $image->move($destinationPath, $awards_image);
                $data['awards_image'] = $awards_image;
        };
       
        $Awards = new Awards;
        $Awards->create($data);
        // 
        Session::flash('message', 'Successfully Saved.');
        return redirect('admin/awards');
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
       
        $title = "Edit Awards";
        
        $result = Awards::find($id);
        return view('admin/awards.edit', compact('title','result', 'id'));
    }

    public function update(Request $request, $id)
    { 

        $this->validate($request, [
            'awards_name'        => 'required',
        ]);
          //$data = $request->all();
        // dd($request->all());

        $data = array(
                        'awards_image'           => $request->awards_image,
                          'awards_name'           => $request->awards_name,
                          'description'  => $request->description
                        
                        
                    );
        
            //   $data = $request->all();
        $awards_image = "";
        if ($request->hasFile('awards_image')) {
                $image = $request->file('awards_image');
                $awards_image = time().'.'.$image->getClientOriginalExtension();
                $destinationPath = public_path('images/awards');
                // dd($destinationPath);
                $image->move($destinationPath, $awards_image);
                $data['awards_image'] = $awards_image;

       
        }

        $Awards = Awards::find($id);          
        $Awards->update($data);
        Session::flash('message', 'successfully Saved.');
        return redirect('admin/awards');
    }

    public function destroy($id)
    {
        $res=Awards::find($id)->delete();
        Session::flash('message', 'Successfully Deleted.');
        return redirect('admin/awards');
    }
    public function status($id,$status)
    {   
        $awards = Awards::find($id);
        $awards->awards_status = $status;
        $awards->save();

    }




}