<?php
namespace App\Http\Controllers\Admin;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\Advisory;
use File;
use Session;
use Hash;
use Str;
class AdvisoryController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $title = "Advisory";
        $results = Advisory::all();
        return view('admin.advisory.index', compact('title','results'));
    }

    public function create()
    {
        $title = "Add Advisory";
        //$subcategory = Subcategory::all();
        return view('admin.advisory.create', compact('title'));
    }

    public function store(Request $request)
    {       
        
        $this->validate($request, [
           
        ]);
          //$data = $request->all();
        // dd($request->all());

        $data = array(
                        'advisory_name'           => $request->advisory_name,
                         'advisory_designation'   => $request->advisory_designation,
                         'description'  => $request->description
                        
                        
                    );
        

        if ($request->hasFile('advisory_image')) {
                $image = $request->file('advisory_image');
                $advisory_image = time().'.'.$image->getClientOriginalExtension();
                $destinationPath = public_path('images/advisory');
                $image->move($destinationPath, $advisory_image);
                $data['advisory_image'] = $advisory_image;
        };
       
        $advisory = new Advisory;
        $advisory->create($data);
        // 
        Session::flash('message', 'Successfully Saved.');
        return redirect('admin/advisory');
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
       
        $title = "Edit Advisory";
        
        $result = Advisory::find($id);
        return view('admin/advisory.edit', compact('title','result', 'id'));
    }

    public function update(Request $request, $id)
    { 

        $this->validate($request, [
          
        ]);

         $data = array(
                       'advisory_name'           => $request->advisory_name,
                        'advisory_designation'   => $request->advisory_designation,
                         'description'  => $request->description
                        );
            //   $data = $request->all();
        $advisory_image = "";
        if ($request->hasFile('advisory_image')) {
                $image = $request->file('advisory_image');
                $advisory_image = time().'.'.$image->getClientOriginalExtension();
                $destinationPath = public_path('images/advisory');
                // dd($destinationPath);
                $image->move($destinationPath, $advisory_image);
                $data['advisory_image'] = $advisory_image;
        }

        $advisory = Advisory::find($id);          
        $advisory->update($data);
        Session::flash('message', 'successfully Saved.');
        return redirect('admin/advisory');
    }

    public function destroy($id)
    {
        $res=Advisory::find($id)->delete();
        Session::flash('message', 'Successfully Deleted.');
        return redirect('admin/advisory');
    }
    public function status($id,$status)
    {   
        $advisory = Advisory::find($id);
        $advisory->advisory_status = $status;
        $advisory->save();

    }




}