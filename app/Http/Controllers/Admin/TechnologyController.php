<?php
namespace App\Http\Controllers\Admin;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\Technology;
use File;
use Session;
use Hash;
class TechnologyController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $results = Technology::all();
        $title = "technology";
        return view('admin.technology.index', compact('title','results'));
    }

    public function create()
    {
        $title = "Add Technology";
        return view('admin.technology.create', compact('title'));
    }

    public function store(Request $request)
    {       
        
        $this->validate($request, [
            'technology_name'        => 'required'
        ]);
        $data = $request->all();
        // dd($request->all());
        if ($request->hasFile('technology_image')) {
                $image = $request->file('technology_image');
                $technology_image = time().'.'.$image->getClientOriginalExtension();
                $destinationPath = public_path('images/technology');
                $image->move($destinationPath, $technology_image);
                $data['technology_image'] = $technology_image;
        };

        $technology = new Technology;           
        //value pass above this line in controller
        $technology->create($data);
        // 
        Session::flash('message', 'Successfully Saved.');
        return redirect('admin/technology');
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        $result = technology::find($id);
         $title = "Edit Technology";
        return view('admin/technology.edit', compact('title','result', 'id'));
    }

    public function update(Request $request, $id)
    {

        
        $this->validate($request, [
            'technology_name'        => 'required',

        ]);

        $data = $request->all();
        $technology_image = "";
        if ($request->hasFile('technology_image')) {
                $image = $request->file('technology_image');
                $technology_image = time().'.'.$image->getClientOriginalExtension();
                $destinationPath = public_path('images/technology');
                // dd($destinationPath);
                $image->move($destinationPath, $technology_image);
                $data['technology_image'] = $technology_image;
        }
        $technology = Technology::find($id);          
        $technology->update($data);
        Session::flash('message', 'Successfully Saved.');
        return redirect('admin/technology');
    }

    public function destroy($id)
    {
        $res=Technology::find($id)->delete();
        Session::flash('message', 'Successfully Deleted.');
        return redirect('admin/technology');
    }

    
    public function status($id,$status)
    {   
        $technology = Technology::find($id);
        $technology->technology_status = $status;
        $technology->save();

    }

}