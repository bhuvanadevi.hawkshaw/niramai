<?php
namespace App\Http\Controllers\Admin;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\Book_Appointment;
use File;
use Session;
use Hash;
class Book_AppointmentController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $results = Book_Appointment::all();
        $title = "Book_Appointment";
        return view('admin.book_appointment.index', compact('title','results'));
    }

    public function create()
    {
        $title = "Add Book_Appointment";
        return view('admin.book_appointment.create', compact('title'));
    }

    public function store(Request $request)
    {       
        
        $this->validate($request, [
            'book_appointment_name'        => 'required'
        ]);
        $data = $request->all();
        // dd($request->all());
        if ($request->hasFile('book_appointment_image')) {
                $image = $request->file('book_appointment_image');
                $book_appointment_image = time().'.'.$image->getClientOriginalExtension();
                $destinationPath = public_path('images/book_appointment');
                $image->move($destinationPath, $book_appointment_image);
                $data['book_appointment_image'] = $book_appointment_image;
        };
   
        $book_appointment = new Book_Appointment;           
        //value pass above this line in controller
        $book_appointment->create($data);
  
    
        // 
        Session::flash('message', 'Successfully Saved.');
        return redirect('admin/book_appointment');
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        $result = Book_Appointment::find($id);
         $title = "Edit Book_Appointment";
        return view('admin/book_appointment.edit', compact('title','result', 'id'));
    }

    public function update(Request $request, $id)
    {

        
        $this->validate($request, [
            'book_appointment_name'        => 'required',

        ]);

        $data = $request->all();
        $book_appointment_image = "";
        if ($request->hasFile('book_appointment_image')) {
                $image = $request->file('book_appointment_image');
                $book_appointment_image = time().'.'.$image->getClientOriginalExtension();
                $destinationPath = public_path('images/book_appointment');
                // dd($destinationPath);
                $image->move($destinationPath, $book_appointment_image);
                $data['book_appointment_image'] = $book_appointment_image;
        }

           $book_appointment = Book_Appointment::find($id);          
        $book_appointment->update($data);
     
        Session::flash('message', 'Successfully Saved.');
        return redirect('admin/book_appointment');
    }

    public function destroy($id)
    {
        $res=Book_Appointment::find($id)->delete();
        Session::flash('message', 'Successfully Deleted.');
        return redirect('admin/book_appointment');
    }

    
    public function status($id,$status)
    {   
        $book_appointment = Book_Appointment::find($id);
        $book_appointment->book_appointment_status = $status;
        $book_appointment->save();

    }

}