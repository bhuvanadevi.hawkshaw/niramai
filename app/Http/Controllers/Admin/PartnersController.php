<?php
namespace App\Http\Controllers\Admin;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\Partners;
use File;
use Session;
use Hash;
use Str;
class PartnersController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $title = "Partners";
        $results = Partners::all();
        return view('admin.partners.index', compact('title','results'));
    }

    public function create()
    {
        $title = "Add Partners";
        //$subcategory = Subcategory::all();
        return view('admin.partners.create', compact('title'));
    }

    public function store(Request $request)
    {       
        
        
        $this->validate($request, [
            'partners_image'        => 'required',
        ]);
          //$data = $request->all();
        // dd($request->all());

        $data = array(
                        'partners_image'           => $request->partners_image,
                        
                        
                    );
        

        
        if ($request->hasFile('partners_image')) {
                $image = $request->file('partners_image');
                $partners_image = time().'.'.$image->getClientOriginalExtension();
                $destinationPath = public_path('images/partners');
                $image->move($destinationPath, $partners_image);
                $data['partners_image'] = $partners_image;
        };
       
       
        $partners = new Partners;
        $partners->create($data);
        // 
        Session::flash('message', 'Successfully Saved.');
        return redirect('admin/partners');
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
       
        $title = "Edit Partners";
        
        $result = Partners::find($id);
        return view('admin/partners.edit', compact('title','result', 'id'));
    }

    public function update(Request $request, $id)
    { 

        $this->validate($request, [
            'partners_image'        => 'required',
        ]);
          //$data = $request->all();
        // dd($request->all());

        $data = array(
                        'partners_image'           => $request->partners_image,
                        
                        
                    );
            //   $data = $request->all();
        $partners_image = "";
        if ($request->hasFile('partners_image')) {
                $image = $request->file('partners_image');
                $partners_image = time().'.'.$image->getClientOriginalExtension();
                $destinationPath = public_path('images/partners');
                // dd($destinationPath);
                $image->move($destinationPath, $partners_image);
                $data['partners_image'] = $partners_image;
        }

        $partners = Partners::find($id);          
        $partners->update($data);
        Session::flash('message', 'successfully Saved.');
        return redirect('admin/partners');
    }

    public function destroy($id)
    {
        $res=Partners::find($id)->delete();
        Session::flash('message', 'Successfully Deleted.');
        return redirect('admin/partners');
    }
    public function status($id,$status)
    {   
        $partners = Partners::find($id);
        $partners->partners_status = $status;
        $partners->save();

    }




}