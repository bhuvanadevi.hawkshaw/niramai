<?php
namespace App\Http\Controllers\Admin;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\Product;
use File;
use Session;
use Hash;
use Str;
class ProductController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $title = "Product";
        $results = Product::all();
        return view('admin.product.index', compact('title','results'));
    }

    public function create()
    {
        $title = "Add Product";
        //$subcategory = Subcategory::all();
        return view('admin.product.create', compact('title'));
    }

    public function store(Request $request)
    {       
           
        $this->validate($request, [
            'product_name'        => 'required'
        ]);
        $data = $request->all();
        // dd($request->all());
        if ($request->hasFile('product_image')) {
                $image = $request->file('product_image');
                $product_image = time().'.'.$image->getClientOriginalExtension();
                $destinationPath = public_path('images/product');
                $image->move($destinationPath, $product_image);
                $data['product_image'] = $product_image;
        };
      
       
        $product = new Product;
        $product->create($data);
        // 
        Session::flash('message', 'Successfully Saved.');
        return redirect('admin/product');
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
       
        $title = "Edit Product";
        
        $result = Product::find($id);
        return view('admin/product.edit', compact('title','result', 'id'));
    }

    public function update(Request $request, $id)
    { 
        $this->validate($request, [
            'product_name'        => 'required',

        ]);

        $data = $request->all();
        $product_image = "";
        if ($request->hasFile('product_image')) {
                $image = $request->file('product_image');
                $product_image = time().'.'.$image->getClientOriginalExtension();
                $destinationPath = public_path('images/product');
                // dd($destinationPath);
                $image->move($destinationPath, $product_image);
                $data['product_image'] = $product_image;
        }

        $product = Product::find($id);          
        $product->update($data);
        Session::flash('message', 'successfully Saved.');
        return redirect('admin/product');
    }

    public function destroy($id)
    {
        $res=Product::find($id)->delete();
        Session::flash('message', 'Successfully Deleted.');
        return redirect('admin/product');
    }
    public function status($id,$status)
    {   
        $product = Product::find($id);
        $product->product_status = $status;
        $product->save();

    }




}