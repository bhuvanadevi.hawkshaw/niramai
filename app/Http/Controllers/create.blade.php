@extends('layouts.admin')

@section('content')

 <div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
                    <div class="kt-portlet kt-portlet--mobile">
                        <div class="kt-portlet__head kt-portlet__head--lg">
                            <div class="kt-portlet__head-label">
                                <span class="kt-portlet__head-icon">
                                    <i class="kt-font-brand flaticon2-line-chart"></i>
                                </span>
                                <h3 class="kt-portlet__head-title">
                                    {{ $title }}
                                </h3>
                            </div>
                            <div class="kt-portlet__head-toolbar">
                                <div class="kt-portlet__head-wrapper">
                                    <div class="kt-portlet__head-actions">
                                         
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="kt-portlet__body">

                           
                                @if(count($errors) > 0)
                                    <div class="alert alert-danger">
                                        <ul>

                                            @foreach($errors->all() as $error)
                                                <li>{{ $error}}</li>
                                            @endforeach 

                                        </ul>
                                    </div>
                                @endif
                            
                        <form method="post" id="career_form" action="{{ route('career.index') }}" enctype="multipart/form-data" > 
                            {{ csrf_field() }}
                            <div class="m-portlet__body">
                                <div class="m-form__section m-form__section--first">
                                  <div class="form-group m-form__group row">
                                        <label class="col-lg-2 col-form-label">
                                           Career Image
                                        </label>
                                        <div class="col-lg-6">
                                              <input type="file" class="form-control" name="career_image" placeholder="" required accept="image/*">
                                              
                                        </div>
                                        
                                    </div>

                                    <div class="form-group m-form__group row">
                                        <label class="col-lg-2 col-form-label">
                                           Career Title
                                        </label>
                                        <div class="col-lg-6">
                                              <input type="text" class="form-control" name="career_name" placeholder="" value="{{ old('career_name') }}" required>
                                        </div>
                                        
                                    </div>
                                         <div class="form-group m-form__group row">
                                        <label class="col-lg-2 col-form-label">
                                           Career Job Type
                                        </label>
                                        <div class="col-lg-6">
                                              <input type="text" class="form-control" name="career_job_type" placeholder="" value="{{ old('career_job_type') }}" required>
                                        </div>
                                        
                                    </div>


                                       <div class="form-group m-form__group row">
                                        <label class="col-lg-2 col-form-label">
                                           Career Location
                                        </label>
                                        <div class="col-lg-6">
                                              <input type="text" class="form-control" name="career_location" placeholder="" value="{{ old('career_location') }}" required>
                                        </div>
                                        
                                    </div>

                                       <div class="form-group m-form__group row">
                                        <label class="col-lg-2 col-form-label">
                                           Career Post Date 
                                        </label>
                                        <div class="col-lg-6">
                                              <input type="text" class="form-control" name="career_post_date" placeholder="" value="{{ old('career_post_date') }}" required>
                                        </div>
                                        
                                    </div>
                                    
                                    <div class="form-group m-form__group row">
                                        <label class="col-lg-2 col-form-label">
                                           Career Description
                                        </label>
                                        <div class="col-lg-6">
                                            <textarea class="form-control career description" name="career description" required></textarea>
                                        </div>
                                        
                                    </div>
                                     <div class="form-group m-form__group row">
                                        <label class="col-lg-2 col-form-label">
                                          Career Content
                                        </label>
                                        <div class="col-lg-6">
                                            <textarea class="form-control career_content" name="career_content" required></textarea>
                                        </div>
                                        
                                    </div>
                              
                                    
                                    
                                       <div class="form-group m-form__group row">
                                        <label class="col-lg-2 col-form-label">
                                           Career Category
                                        </label>
                                        <div class="col-lg-6">
                                              <input type="text" class="form-control" name="career_category" placeholder="" value="{{ old('career_category') }}" required>
                                        </div>
                                        
                                    </div>
                                </div>
                        </div>
                        <div class="m-portlet__foot m-portlet__foot--fit">
                            <div class="m-form__actions m-form__actions">
                                <div class="row">
                                    <div class="col-lg-2"></div>
                                    <div class="col-lg-6">
                                        
                                        <button class="btn btn-success"><span>Submit</span></button>
                                        <a href="{{ url('admin/career') }}" class="btn btn-danger"><span>Cancel</span></a>
                                    
                                    </div>
                                
                                </div>
                            </div>
                        </div>
                    </form>        
                
                           
                        </div>
                    </div>
                </div>
@endsection

@push('scripts')
<script type="text/javascript">
    $("#career_form").validate();
    $(".career_content").summernote();
</script>
@endpush