-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Mar 09, 2022 at 09:52 AM
-- Server version: 10.4.21-MariaDB
-- PHP Version: 7.4.24

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `niramai`
--

-- --------------------------------------------------------

--
-- Table structure for table `about_us`
--

CREATE TABLE `about_us` (
  `id` int(11) NOT NULL,
  `title` varchar(256) DEFAULT NULL,
  `content` text DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT 0,
  `created_at` datetime NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `about_us`
--

INSERT INTO `about_us` (`id`, `title`, `content`, `status`, `created_at`, `updated_at`) VALUES
(1, 'About Us', '<p><span style=\"font-family: &quot;Roboto Slab&quot;, serif; font-size: 14px; text-align: justify; background-color: rgb(247, 248, 250);\">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum ac sodales sapien. Sed pellentesque, quam in ornare tincidunt, magna augue placerat nunc, ut facilisis nibh ipsum non ipsum. Cras ac eros non neque viverra consequat sed at est. Fusce efficitur, lacus nec dignissim tincidunt, diam sapien rhoncus neque, at tristique sapien nibh sed neque. Proin in neque in purus luctus facilisis. Donec viverra ligula quis lorem viverra consequat. Aliquam condimentum id enim volutpat rutrum. Donec semper iaculis convallis. Praesent quis elit eget ligula facilisis mattis. Praesent sed euismod dui. Suspendisse imperdiet vel quam nec venenatis. Suspendisse dictum blandit quam, vitae auctor enim gravida et. Sed id dictum nibh. Proin egestas massa sit amet tincidunt aliquet.</span><br></p>', 1, '0000-00-00 00:00:00', '2022-01-22 06:11:15');

-- --------------------------------------------------------

--
-- Table structure for table `advisory`
--

CREATE TABLE `advisory` (
  `id` int(10) NOT NULL,
  `advisory_name` varchar(256) NOT NULL,
  `advisory_image` varchar(256) NOT NULL,
  `advisory_designation` varchar(256) DEFAULT NULL,
  `description` text DEFAULT NULL,
  `advisory_status` int(11) NOT NULL DEFAULT 1,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `advisory`
--

INSERT INTO `advisory` (`id`, `advisory_name`, `advisory_image`, `advisory_designation`, `description`, `advisory_status`, `created_at`, `updated_at`) VALUES
(6, 'Dr. Kiran Mazumdar-Shaw', '1643555794.jpg', 'Chairperson & Managing Director,Biocon Ltd', 'I congratulate Team Niramai for developing an innovative thermal analytics based Breast Cancer Solution for early stage breast cancer detection. This Non-Invasive Risk Assessment with Machine Intelligence, cancer screening solution is relevant for women of all ages and poses minimal health hazard since it is contact and radiation free. I believe Niramai will enable easy adoption for early cancer screening and will play a key role in improving outcomes in our ongoing battle against breast cancer. I am delighted to support this initiative.', 1, '2022-02-02 16:00:51', '2022-02-02 10:30:51'),
(9, 'Dr. H V Ram Prakash', '1643817828.jpg', 'Senior Radiologist and Expert Thermographer', 'Thermography can detect breast cancer in ways that are non-invasive, non-ionizing and non-traumatizing. In addition to detecting tumor growth earlier, this innovation can bring breast cancer screening to the doorsteps of women all over the world. I am happy to partner with Niramai on this journey.', 1, '2022-02-03 13:46:48', '2022-02-03 08:16:48');

-- --------------------------------------------------------

--
-- Table structure for table `awards`
--

CREATE TABLE `awards` (
  `id` int(10) NOT NULL,
  `awards_image` varchar(256) DEFAULT NULL,
  `awards_name` varchar(256) NOT NULL,
  `description` text DEFAULT NULL,
  `awards_status` int(11) NOT NULL DEFAULT 1,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `awards`
--

INSERT INTO `awards` (`id`, `awards_image`, `awards_name`, `description`, `awards_status`, `created_at`, `updated_at`) VALUES
(2, '1643811840.png', 'Amazon AI Conclave', 'Winner of Amazon AI Conclave 2017 Award in Health Care', 1, '2022-02-02 14:24:00', '2022-02-02 08:54:00'),
(3, '1644555297.png', 'Google Launchpad', ' Niramai is among 4 startups from India that got selected for Google Launchpad Accelerator Program 2018', 1, '2022-02-11 04:54:57', '2022-02-10 23:24:57'),
(4, '1644555409.png', 'Incub@te', 'Selected for Tata Incub@te program', 1, '2022-02-11 04:56:49', '2022-02-10 23:26:49'),
(5, '1644555485.png', 'Incub@te', 'Selected for Tata Incub@te program', 1, '2022-02-11 04:58:05', '2022-02-10 23:28:05'),
(6, '1644555527.png', 'Incub@te', 'Selected for Tata Incub@te program', 1, '2022-02-11 04:58:47', '2022-02-10 23:28:47'),
(7, '1644555567.png', 'Incub@te', 'Selected for Tata Incub@te program', 1, '2022-02-11 04:59:27', '2022-02-10 23:29:27'),
(8, '1644555606.png', 'Incub@te', 'Selected for Tata Incub@te program', 1, '2022-02-11 05:00:06', '2022-02-10 23:30:06'),
(9, '1644555662.png', 'Incub@te', 'Selected for Tata Incub@te program', 1, '2022-02-11 05:01:02', '2022-02-10 23:31:02'),
(10, '1644555720.png', 'Incub@te', 'Selected for Tata Incub@te program', 1, '2022-02-11 05:02:00', '2022-02-10 23:32:00'),
(11, '1644555744.png', 'Incub@te', 'Selected for Tata Incub@te program', 1, '2022-02-11 05:02:24', '2022-02-10 23:32:24'),
(12, '1644555770.png', 'Incub@te', 'Selected for Tata Incub@te program', 1, '2022-02-11 05:02:50', '2022-02-10 23:32:50'),
(13, '1644555834.png', 'Incub@te', 'Selected for Tata Incub@te program', 1, '2022-02-11 05:03:54', '2022-02-10 23:33:54');

-- --------------------------------------------------------

--
-- Table structure for table `banner`
--

CREATE TABLE `banner` (
  `id` int(11) NOT NULL,
  `banner_name` varchar(250) NOT NULL,
  `banner_image` varchar(250) NOT NULL,
  `banner_description` varchar(250) DEFAULT NULL,
  `banner_link` varchar(256) DEFAULT NULL,
  `banner_link_name` varchar(256) NOT NULL,
  `banner_status` int(11) DEFAULT 1,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf16;

--
-- Dumping data for table `banner`
--

INSERT INTO `banner` (`id`, `banner_name`, `banner_image`, `banner_description`, `banner_link`, `banner_link_name`, `banner_status`, `created_at`, `updated_at`) VALUES
(22, 'Privacy Aware screening', '1644552407.png', 'No pain, no radiation, no touch, no see method of screening breast health', 'https://hashref.org/n/what-we-do.html', 'Your Breast Health', 1, '2022-02-11 05:36:56', '2022-02-11 00:06:56'),
(23, 'Clinically Proven And Regulatory Certified', '1643905249.png', 'with International clinical publications', 'https://hashref.org/n/tech-research.html', 'Publications', 1, '2022-02-11 05:37:44', '2022-02-11 00:07:44'),
(24, 'Join Our Mission', '1643905226.png', 'Together we can save many lives', 'https://hashref.org/n/partnerships.html', 'Partner with us', 1, '2022-02-11 05:38:14', '2022-02-11 00:08:14'),
(25, '“FeverTest”', '1644553377.png', 'Passive Screening for Covid Symptoms', 'https://hashref.org/n/what-we-do.html', 'Buy Now', 1, '2022-02-11 05:38:48', '2022-02-11 00:08:48');

-- --------------------------------------------------------

--
-- Table structure for table `book_appointment`
--

CREATE TABLE `book_appointment` (
  `id` int(11) NOT NULL,
  `book_appointment_image` varchar(256) NOT NULL,
  `book_appointment_name` varchar(256) NOT NULL,
  `book_appointment_description` text NOT NULL,
  `book_appointment_status` int(10) NOT NULL DEFAULT 1,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `book_appointment`
--

INSERT INTO `book_appointment` (`id`, `book_appointment_image`, `book_appointment_name`, `book_appointment_description`, `book_appointment_status`, `created_at`, `updated_at`) VALUES
(1, '1644658826.png', 'Home Screening', 'It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout.', 1, '2022-02-12 10:40:39', '2022-02-12 05:10:39'),
(2, '1644662308.jfif', 'Hospital Screening', 'It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout.', 1, '2022-02-12 05:08:28', '2022-02-12 05:08:28'),
(3, '1644662423.jfif', 'Online Risk Test', 'It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout.', 1, '2022-02-12 05:10:23', '2022-02-12 05:10:23'),
(4, '1644662552.jfif', 'Organize Screening Camp', 'It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout.', 1, '2022-02-12 05:12:32', '2022-02-12 05:12:32');

-- --------------------------------------------------------

--
-- Table structure for table `career`
--

CREATE TABLE `career` (
  `id` int(10) NOT NULL,
  `career_image` varchar(256) DEFAULT NULL,
  `career_name` varchar(256) DEFAULT NULL,
  `career_job_type` varchar(256) NOT NULL,
  `career_location` varchar(256) NOT NULL,
  `career_description` text NOT NULL,
  `career_content` text NOT NULL,
  `career_category` varchar(256) NOT NULL,
  `career_status` int(11) DEFAULT 1,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `career`
--

INSERT INTO `career` (`id`, `career_image`, `career_name`, `career_job_type`, `career_location`, `career_description`, `career_content`, `career_category`, `career_status`, `created_at`, `updated_at`) VALUES
(1, '1644564566.svg', 'Lorem Ipsum Comes From Section', 'Full Time', 'Banglore', 'Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of \"de Finibus Bonorum et Malorum\"\r\n(The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory of ethics, very popular during the Renaissance.', '<p><span style=\"color: rgb(80, 80, 80); font-family: Poppins, sans-serif; font-size: 14px; letter-spacing: 1px;\">Lorem ipsum dolor sit amet consectetur adipisicing elit. Accusamus obcaecati sunt tempora aliquid omnis facere ipsam sed iusto quod a? Aperiam itaque id aspernatur architecto earum quod sint ullam doloremque omnis, soluta non asperiores veniam nihil sunt dolore fuga iste molestiae, voluptatem optio nam est expedita voluptate? Consequuntur omnis corrupti libero unde laborum iusto atque eligendi aliquam facilis? Sed ipsum at a velit impedit laborum rerum fugit quidem, doloremque consequatur tempora pariatur accusantium ducimus nihil dolor asperiores nam, natus voluptatum eum! Perspiciatis consequuntur dolorem dolor nulla! Explicabo corrupti obcaecati eius dignissimos dolor dicta quos quo dolorem, neque fugiat. Adipisci facilis temporibus beatae voluptates odio debitis. Praesentium excepturi optio pariatur tempore voluptatem quis aliquam, earum cumque eius vitae perspiciatis reiciendis fugit aperiam rerum qui? Amet corrupti aliquam eaque similique odit, nemo illum unde pariatur ipsa debitis labore consectetur quod saepe dolores ea ab autem nostrum perspiciatis placeat! Soluta fugiat ipsa sit voluptates alias, illo quae quisquam sequi eveniet rem explicabo, et hic assumenda ducimus adipisci, ipsum dolorum voluptatem natus? Corrupti doloremque eligendi repudiandae nihil atque sed aut repellendus numquam optio magni debitis facilis culpa dolorem nobis saepe, voluptas nisi officiis ea reiciendis eum quibusdam error. Doloribus assumenda voluptas omnis autem sit necessitatibus nam molestiae nisi reiciendis labore voluptatem natus hic impedit dolorum recusandae, alias ullam, eum facilis error magnam, corporis perferendis harum? Quo reprehenderit quos vel consequatur, blanditiis iure dicta unde sit inventore illum voluptatum maiores temporibus modi impedit odio excepturi! Ut, ipsam asperiores quaerat consectetur veniam dignissimos optio consequuntur omnis. Debitis optio unde similique corrupti alias deleniti. Similique, aliquid placeat. Modi nisi beatae natus iusto corrupti magni in, repudiandae sapiente, optio perspiciatis accusantium! Explicabo sunt molestias ipsam, illum quidem reprehenderit, eum quasi iure exercitationem dolorum consectetur? Suscipit modi dicta magnam, hic architecto optio, quos repellat ratione natus, eveniet consectetur praesentium?</span><br></p>', 'ashghds', 1, '2022-02-11 18:30:00', '2022-02-11 18:30:00'),
(2, '1644571628.svg', 'Lorem Ipsum Comes From Section', 'Full Time', 'Banglore', 'Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of \"de Finibus Bonorum et Malorum\"\r\n(The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory of ethics, very popular during the Renaissance.', '<p><span style=\"color: rgb(80, 80, 80); font-family: Poppins, sans-serif; font-size: 14px; letter-spacing: 1px;\">Lorem ipsum dolor sit amet consectetur adipisicing elit. Accusamus obcaecati sunt tempora aliquid omnis facere ipsam sed iusto quod a? Aperiam itaque id aspernatur architecto earum quod sint ullam doloremque omnis, soluta non asperiores veniam nihil sunt dolore fuga iste molestiae, voluptatem optio nam est expedita voluptate? Consequuntur omnis corrupti libero unde laborum iusto atque eligendi aliquam facilis? Sed ipsum at a velit impedit laborum rerum fugit quidem, doloremque consequatur tempora pariatur accusantium ducimus nihil dolor asperiores nam, natus voluptatum eum! Perspiciatis consequuntur dolorem dolor nulla! Explicabo corrupti obcaecati eius dignissimos dolor dicta quos quo dolorem, neque fugiat. Adipisci facilis temporibus beatae voluptates odio debitis. Praesentium excepturi optio pariatur tempore voluptatem quis aliquam, earum cumque eius vitae perspiciatis reiciendis fugit aperiam rerum qui? Amet corrupti aliquam eaque similique odit, nemo illum unde pariatur ipsa debitis labore consectetur quod saepe dolores ea ab autem nostrum perspiciatis placeat! Soluta fugiat ipsa sit voluptates alias, illo quae quisquam sequi eveniet rem explicabo, et hic assumenda ducimus adipisci, ipsum dolorum voluptatem natus? Corrupti doloremque eligendi repudiandae nihil atque sed aut repellendus numquam optio magni debitis facilis culpa dolorem nobis saepe, voluptas nisi officiis ea reiciendis eum quibusdam error. Doloribus assumenda voluptas omnis autem sit necessitatibus nam molestiae nisi reiciendis labore voluptatem natus hic impedit dolorum recusandae, alias ullam, eum facilis error magnam, corporis perferendis harum? Quo reprehenderit quos vel consequatur, blanditiis iure dicta unde sit inventore illum voluptatum maiores temporibus modi impedit odio excepturi! Ut, ipsam asperiores quaerat consectetur veniam dignissimos optio consequuntur omnis. Debitis optio unde similique corrupti alias deleniti. Similique, aliquid placeat. Modi nisi beatae natus iusto corrupti magni in, repudiandae sapiente, optio perspiciatis accusantium! Explicabo sunt molestias ipsam, illum quidem reprehenderit, eum quasi iure exercitationem dolorum consectetur? Suscipit modi dicta magnam, hic architecto optio, quos repellat ratione natus, eveniet consectetur praesentium?</span><br></p>', 'fsft', 1, '2022-02-11 03:57:08', '2022-02-11 03:57:08');

-- --------------------------------------------------------

--
-- Table structure for table `category`
--

CREATE TABLE `category` (
  `id` int(11) NOT NULL,
  `category_name` varchar(256) NOT NULL,
  `category_image` varchar(256) NOT NULL,
  `category_slug` varchar(256) NOT NULL,
  `category_status` int(11) NOT NULL DEFAULT 1,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `partners`
--

CREATE TABLE `partners` (
  `id` int(10) NOT NULL,
  `partners_image` varchar(256) NOT NULL,
  `partners_status` int(11) NOT NULL DEFAULT 1,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `partners`
--

INSERT INTO `partners` (`id`, `partners_image`, `partners_status`, `created_at`, `updated_at`) VALUES
(3, '1643547887.svg', 1, '2022-01-30 07:34:47', '2022-01-30 07:34:47'),
(4, '1643547912.svg', 1, '2022-01-30 07:35:12', '2022-01-30 07:35:12'),
(5, '1643547943.svg', 1, '2022-01-30 07:35:43', '2022-01-30 07:35:43'),
(6, '1643547964.svg', 1, '2022-01-30 07:36:04', '2022-01-30 07:36:04'),
(7, '1643547989.svg', 1, '2022-01-30 07:36:29', '2022-01-30 07:36:29');

-- --------------------------------------------------------

--
-- Table structure for table `partnerships`
--

CREATE TABLE `partnerships` (
  `id` int(10) NOT NULL,
  `partnerships_image` varchar(256) NOT NULL,
  `partnerships_name` varchar(256) NOT NULL,
  `partnerships_description` text NOT NULL,
  `partnerships_status` int(10) DEFAULT 1,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `partnerships`
--

INSERT INTO `partnerships` (`id`, `partnerships_image`, `partnerships_name`, `partnerships_description`, `partnerships_status`, `created_at`, `updated_at`) VALUES
(1, '1644654387.jfif', 'For Hospitals', '.We Have A Strong Existing Network Of Hospital-Partners. We Are Keen To Partner With Additional Hospitals Pan-India To Offer Our Clinically Proven, AI-Based Screening Solution For Early Detection Of Breast Cancer, For Screenings Both Inside The Hospital And At Their Client Locations. Please Get In Touch With Us For More Information', 1, '2022-02-12 08:45:06', '2022-02-12 03:15:06'),
(2, '1644656458.jfif', 'For Doctors', 'We invite expressions of interest from potential channel partners who wish to offer our novel screening solution to their valued healthcare clients. Please get in touch with us for more information', 1, '2022-02-12 03:30:58', '2022-02-12 03:30:58');

-- --------------------------------------------------------

--
-- Table structure for table `product`
--

CREATE TABLE `product` (
  `id` int(10) NOT NULL,
  `product_image` varchar(256) DEFAULT NULL,
  `product_name` varchar(256) DEFAULT NULL,
  `product_status` varchar(256) NOT NULL DEFAULT '1',
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `updated_at` timestamp NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `product`
--

INSERT INTO `product` (`id`, `product_image`, `product_name`, `product_status`, `created_at`, `updated_at`) VALUES
(5, '1644557641.png', 'Breast Health', '1', '2022-02-11 05:34:01', '2022-02-11 00:04:01'),
(6, '1645508847.jpeg', 'Fever Test', '1', '2022-02-22 05:47:27', '2022-02-22 00:17:27'),
(7, '1645508958.jpeg', 'Risk Assessment', '1', '2022-02-22 05:49:18', '2022-02-22 00:19:18');

-- --------------------------------------------------------

--
-- Table structure for table `setting`
--

CREATE TABLE `setting` (
  `id` int(11) NOT NULL,
  `company_name` varchar(255) CHARACTER SET utf8 NOT NULL,
  `image_name` varchar(255) NOT NULL,
  `image_name2` varchar(255) DEFAULT NULL,
  `company_fav` varchar(255) DEFAULT NULL,
  `company_address` text CHARACTER SET utf8 NOT NULL,
  `company_mobile` varchar(256) NOT NULL,
  `company_email` varchar(255) DEFAULT NULL,
  `currency` varchar(25) CHARACTER SET utf8 DEFAULT NULL,
  `facbook` varchar(100) CHARACTER SET utf8 DEFAULT NULL,
  `twitter` varchar(100) CHARACTER SET utf8 DEFAULT NULL,
  `google_plus` varchar(100) CHARACTER SET utf8 DEFAULT NULL,
  `instrgram` varchar(100) CHARACTER SET utf8 DEFAULT NULL,
  `meta_title` varchar(100) CHARACTER SET utf8 DEFAULT NULL,
  `meta_description` text CHARACTER SET utf8 DEFAULT NULL,
  `meta_keyword` text CHARACTER SET utf8 DEFAULT NULL,
  `header_script` text CHARACTER SET utf8 DEFAULT NULL,
  `footer_script` text CHARACTER SET utf8 DEFAULT NULL,
  `map_location` text DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `setting`
--

INSERT INTO `setting` (`id`, `company_name`, `image_name`, `image_name2`, `company_fav`, `company_address`, `company_mobile`, `company_email`, `currency`, `facbook`, `twitter`, `google_plus`, `instrgram`, `meta_title`, `meta_description`, `meta_keyword`, `header_script`, `footer_script`, `map_location`, `created_at`, `updated_at`) VALUES
(1, 'Niramai', 'company-logo-1643870698.svg', 'company-logo2-1643870698.svg', 'fav-icon-1643870698.svg', 'Ground Floor, Innova Pearl, No. 17, 5th Block, Industry Layout, Bangalore, 56009', '+91 866092297', 'Contact@Niramai.Co', 'Rs', 'https://www.facebook.com/', 'https://twitter.com', 'https://www.whatsapp.com/', 'https://instagram.com/', 'Meta Title', 'test -1', 'test', '', '', '<iframe id=\"map_o\" width=\"100%\" height=\"300px\" src=\"https://maps.google.com/maps?q=9%2F1877%2C%20kailash%20nagar%2C%20street%20number%202%2C%20new%20delhi%20-%20110031%2C%20india&amp;t=&amp;z=13&amp;ie=UTF8&amp;iwloc=&amp;output=embed\" frameborder=\"0\" scrolling=\"no\" marginheight=\"0\" marginwidth=\"0\"></iframe>', NULL, '2022-02-12 01:25:30');

-- --------------------------------------------------------

--
-- Table structure for table `subcategory`
--

CREATE TABLE `subcategory` (
  `id` int(10) NOT NULL,
  `category_id` int(11) DEFAULT NULL,
  `subcategory_name` varchar(256) NOT NULL,
  `subcategory_image` varchar(256) NOT NULL,
  `subcategory_slug` varchar(256) DEFAULT NULL,
  `subcategory_status` int(10) NOT NULL DEFAULT 1,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `subcategory`
--

INSERT INTO `subcategory` (`id`, `category_id`, `subcategory_name`, `subcategory_image`, `subcategory_slug`, `subcategory_status`, `created_at`, `updated_at`) VALUES
(1, 3, 'BLACK BLOCKS', '1642834001.png', 'black-blocks', 1, '2022-01-22 06:46:41', '2022-01-22 01:16:41'),
(2, 3, 'RED BLOCKS', '1642849957.png', 'red-blocks', 1, '2022-01-22 11:12:37', '2022-01-22 05:42:37'),
(5, 4, 'STEEL ROOF', '1642849979.png', 'steel-roof', 1, '2022-01-22 17:23:23', '2022-01-22 11:53:23');

-- --------------------------------------------------------

--
-- Table structure for table `technology`
--

CREATE TABLE `technology` (
  `id` int(10) NOT NULL,
  `technology_image` varchar(256) NOT NULL,
  `technology_name` varchar(256) NOT NULL,
  `technology_description` text NOT NULL,
  `technology_link` varchar(256) NOT NULL,
  `technology_status` varchar(256) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `updated_at` timestamp NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `technology`
--

INSERT INTO `technology` (`id`, `technology_image`, `technology_name`, `technology_description`, `technology_link`, `technology_status`, `created_at`, `updated_at`) VALUES
(1, '1644575437.png', 'Thermalytix', 'The core of NIRAMAI solution is Thermalytix, a computer aided diagnostic engine that is powered by Artificial Intelligence.', '#', '1', '2022-02-11 10:30:44', '2022-02-11 05:00:44'),
(2, '1644578601.png', 'Clinical Studies', 'Our cancer screening tool has been tested on more than 43000 women in 120+ hospitals/diagnostic centers as well as 2000+ screening camps.', '#', '1', '2022-02-11 05:53:21', '2022-02-11 05:53:21'),
(3, '1644578671.png', 'Patents And Publications', 'Niramai has been granted more than 24 Indian & International patents. Regulatory Clearances', '#', '1', '2022-02-11 05:54:31', '2022-02-11 05:54:31'),
(4, '1644578728.png', 'Regulatory Clearances', 'It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout', '#', '1', '2022-02-11 05:55:28', '2022-02-11 05:55:28');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `mobile_no` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `dob` date DEFAULT NULL,
  `city` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `gender` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`, `mobile_no`, `dob`, `city`, `gender`) VALUES
(1, 'Niramai', 'admin@gmail.com', NULL, '$2y$10$QZslzZV6S.J6EOAfC9DmXup6U7XB99mEi/2MMXGDC.1ydaQEE6tQ2', 'A3inmLBOqXxOMzdaWOpO92wigsi1n6Xgi33ermx6XgrmVz6pYcYt3gDCvDnr', '2020-10-13 21:18:55', '2022-01-27 05:55:18', '9066418026', '2021-01-27', 'Dharmapuri', '1');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `about_us`
--
ALTER TABLE `about_us`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `advisory`
--
ALTER TABLE `advisory`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `awards`
--
ALTER TABLE `awards`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `banner`
--
ALTER TABLE `banner`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `book_appointment`
--
ALTER TABLE `book_appointment`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `career`
--
ALTER TABLE `career`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `category`
--
ALTER TABLE `category`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `partners`
--
ALTER TABLE `partners`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `partnerships`
--
ALTER TABLE `partnerships`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `product`
--
ALTER TABLE `product`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `setting`
--
ALTER TABLE `setting`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `subcategory`
--
ALTER TABLE `subcategory`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `technology`
--
ALTER TABLE `technology`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `about_us`
--
ALTER TABLE `about_us`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `advisory`
--
ALTER TABLE `advisory`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `awards`
--
ALTER TABLE `awards`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `banner`
--
ALTER TABLE `banner`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;

--
-- AUTO_INCREMENT for table `book_appointment`
--
ALTER TABLE `book_appointment`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `career`
--
ALTER TABLE `career`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `category`
--
ALTER TABLE `category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `partners`
--
ALTER TABLE `partners`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `partnerships`
--
ALTER TABLE `partnerships`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `product`
--
ALTER TABLE `product`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `subcategory`
--
ALTER TABLE `subcategory`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `technology`
--
ALTER TABLE `technology`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
